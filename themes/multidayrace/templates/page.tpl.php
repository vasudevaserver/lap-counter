<?php 
/**
 * @file
 * Get Skeleton theme implementation to display a single Drupal page.
 */
?>
  <div id="lap-counter-container" class="container">
    <div<?php print $attributes; ?>>
    <?php if (!$messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div></div> <!-- /.section, /#messages -->
    <?php endif; ?>      
          
    <div id="content" class="column<?php print $content_classes ?>">
      <?php if ($title == '123'): ?>
        <h1 class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>

     
      <?php if (isset($page['content'])) : ?>
        <?php print render($page['content']); ?>
      <?php endif; ?>
      
    </div>
    </div>
  </div><!-- container -->
