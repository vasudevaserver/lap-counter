<?php 
/**
 * @file
 * Get Skeleton theme implementation to display a single Drupal page.
 */
//menu_save($menu);

  $menu_check = race_manager_check_menus();

  $mobile = race_manager_detect_mobile();
  $refresh_list = race_manager_refresh_rate_list();
  if (in_array($display_id, $refresh_list)) {
    $refresh_rate = race_manager_refresh_rate();
    $args = Array();
    $args['#tag'] = 'meta';
    $args['#attributes'] = Array(
      'http-equiv' => 'Refresh',
      'content' => $refresh_rate
    );
    drupal_add_html_head($args, 'refresh_rate');
}
$module_location = race_manager_module_location();
$cssfile = $module_location . '/css/race_manager.css';
 drupal_add_css($cssfile);
$display_list = race_manager_js_enabled_list();
if (in_array($display_id, array_keys($display_list))) {
  $file = $display_list[$display_id];
  $file = $module_location . '/js/' . $file;
  drupal_add_js($file);
}

$display_list = race_manager_data_entry_list();
if (in_array($display_id, array_keys($display_list))) {
  $file = $display_list[$display_id];
  $file = $module_location . '/css/' . $file;
  drupal_add_css($file);
}

$c39 = chr(39);
$onkeyup = ' onkeyup="KeyHandler(event, ' .$c39 . $display_id . $c39 . ');"';
$onclick = ' onclick="SetFocus();"';
?>
  <div class="container"
       <?php print($onkeyup); print($onclick)?>>
    <div id="header">
      
      <?php if (strpos($display_id, 'raceadmin')===FALSE): ?>
        <?php if ($display_id != 'dataentryonly'): ?>
          <div id="race-manager-menu">
            <?php $menu_links = menu_navigation_links('menu-race-manager');
                  $menu_html = theme('links__menu-race-manager',
                          array('links' => $menu_links));
                   print $menu_html; ?>
            <?php if (intval(!$user->uid)): ?>
              <div class="login" ><a href="user">Login</a></div>
            <?php endif; ?>
          <?php endif; ?>
      <?php endif; ?>
      <?php if (strpos($display_id, 'raceadmin')!==FALSE): ?>
        <div id="race-manager-menu-admin">
            <?php $menu_links = menu_navigation_links('menu-race-manager-admin');
                  $menu_html = theme('links__menu-race-manager-admin', array('links' => $menu_links));
            print $menu_html; ?>
        </div>
      <?php endif; ?>
    </div> <!-- /header -->
    </div> <!-- /header -->
    <div id="content" class="column 
      <?php if (strpos($display_id, 'dataentrychute') !==FALSE ): ?>
        <?php print ' data-entry-chute'; ?>
      <?php endif; ?>
      ">
      <div id="content-header">
      <?php if (strpos($display_id, 'raceadminregistration')!==FALSE): ?>
        <div id="race-manager-menu-admin-registration">
            <?php $menu_links = menu_navigation_links('menu-race-registration');
                  $menu_html = theme('links__menu-race-registration', array('links' => $menu_links));
            print $menu_html; ?>
        </div>
      <?php endif; ?>
          <?php 
           if (($display_id != '1dataentrychute') and (!$mobile)) {
             $output = '<div id="race-manager-title">';
             $output .= $page_title;
             $output .= '</div>';
             print $output;
             }
          ?>
      </div>
      <?php print ( $page_content ); ?>
      
    </div>
      
      
  </div><!-- container -->
