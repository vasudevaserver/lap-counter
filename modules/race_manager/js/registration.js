function SetNameClick() {
  SetNameSelection();
}

function SetNameChange() {
  SetNameSelection();
}

function SetNameBlur() {
  SetNameSelection();
}

function SetNameSelection() {
  var selection = document.getElementById('edit-select-name');
  var id_ob = document.getElementById('current-id');
  var data = selection.value;
  var list = data.split('(');
  if (list.length <= 1) {
    id_ob.value = '';
    return;
  }
  var id = list[1].split(')');
  id = id[0];
  if (!id) {
    id_ob.value = '';
    return;
  }
  id_ob.value = id;
}
