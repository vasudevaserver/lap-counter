function BibFn(arg) {
  var bib = document.getElementById("bib_entry");
  var bvalue = bib.value.trim();
  var type = document.getElementById("chute_type");
  type = type.value;
  var id = document.getElementById("current_id");
  if (id) {
    id = id.value;
  }
  if (type == 'entry') { 
    if (arg == 1) { // Regular entry of bib upon arrival at finish line
      // The `record` button triggers this value
      ProcessBibEntry(bvalue, 0, 'dataentrychute');
    }
    if (arg == 2) { // Pre-enter bib prior to arrival at finish line
      ProcessBibEntry(bvalue, 0, 'dataentrychutepre-enter');
    }
    if (arg == 3) { // Previous finisher: assign bib to previously recorded time
      ProcessBibEntry(bvalue, id, 'dataentrychutepreviousfinisher');
    }
    if (arg == 4) { // Previous finisher: assign bib to previously recorded time
      ProcessBibEntry(bvalue, id, 'dataentrychutesametime');
    }
  }
  if (type == 'exit') {
    if (arg == 1) { // Regular entry of bib upon exit of chute
      ProcessBibEntry(bvalue, id, 'dataentrychuteexit');
    }
    if (arg == 2) { // No bib number provided - accept as bandit
      ProcessBibEntry(-1, id, 'dataentrychutebandit');
    }
    if (arg == 3) { // No bib number provided - accept as bandit
      ProcessBibEntry(bvalue, id, 'dataentrychutealtstart');
    }
    if (arg == 4) { // process (accept) all bibs up to next missing bib
      ProcessBibEntry(bvalue, id, 'dataentrychuteacceptall');
    }
    if (arg == 9) { // Delete entry 
      ProcessBibEntry(bvalue, id, 'dataentrychutedelete');
    }
  }
  bib.value = '';
  bib.focus();
  return false;
}

function SetBibNum(data) {
  var bib = document.getElementById("bib_entry");
  bib.value += data;
  bib.focus();
  return false;
}

// Keystroke detection and handling
function KeyHandler(event, displayid) {
  var bib = document.getElementById("bib_entry");
  var type = document.getElementById("chute_type");
  type = type.value;
  bib.focus();
  var itemlog = document.getElementById("chute-items");
  var bvalue = '0';
  bvalue = bib.value.trim();
  blength = bvalue.length;
  if (blength == 0) {
    bvalue = '0';
  }
  
  var keypress = 0;
  if (event) {
    keypress = event.keyCode? event.keyCode : event.charCode
  }
//  alert(keypress);
  var enter = document.getElementById("enter");
  if (enter) {
    enter = enter.value;
  }
  else {
    enter = 0;
  }
  var cancel = document.getElementById("cancel");
  if (cancel) {
    cancel = cancel.value;
  }
  else {
    cancel = 0;
  }
  var deleterecord = document.getElementById("deleterecord");
  if (deleterecord) {
    deleterecord = deleterecord.value;
  }
  else {
    deleterecord = 0;
  }
  if (type == 'entry') {
    var preenter = document.getElementById("pre-enter");
    if (preenter) {
      preenter = preenter.value;
    }
    else {
      preenter = 0;
    }
    var prevfinisher = document.getElementById("prev-finisher");
    if (prevfinisher) {
      prevfinisher = prevfinisher.value;
    }
    else {
      prevfinisher = 0;
    }
    var sametime = document.getElementById("same-time");
    if (sametime) {
      sametime = sametime.value;
    }
    else {
      sametime = 0;
    }
  }
  if (type == 'exit') {
    var bandit = document.getElementById("bandit");
    if (bandit) {
      bandit = bandit.value;
    }
    else {
      bandit = 0;
    }
    var acceptall = document.getElementById("acceptall");
    if (acceptall) {
      acceptall = acceptall.value;
    }
    else {
      acceptall = 0;
    }
    var altstart = document.getElementById("altstart");
    if (altstart) {
      altstart = altstart.value;
    }
    else {
      altstart = 0;
    }
  }
  var send = false;
  var fn = displayid;
  if (keypress == 13 || keypress == 9) {
    BibFn(1);
//    window.location.reload(false); 
  }
//  extract the last value in the bib entry field
  var lvalue = 0;
  lvalue = bvalue.charCodeAt(bvalue.length - 1);
  if (lvalue == cancel) { // Clear field
    // allow a "-" sign in the first location to enable delete record function
    if (blength == 1){
      return;
    } 
    bib.value = '';
    return false;
  }
  if (type == 'entry') {
    if (lvalue == preenter) {
      BibFn(2);
    }
    if (lvalue == prevfinisher) {
      BibFn(3);
    }
    if (lvalue == sametime) {
      BibFn(4);
    }
  }
  if (type == 'exit') {
    if (lvalue == bandit) {
      BibFn(2);
    }
    if (lvalue == altstart) {
      BibFn(3);
    }
    if (lvalue == acceptall) {
      BibFn(4);
    }
  }
  if (lvalue == enter) {
    BibFn(1);
    return false;
  }
  if (lvalue == deleterecord) {
    BibFn(9);
    return false;
  }
  return false;
}

function ProcessBibEntry(bibnumber, id, fn) {
  var itemlog = document.getElementById("chute-items");
  var bib = document.getElementById("bib_entry");
  bib.value = '';
  bib.focus();
  var xmlhttp;
  var response = '';
  var basepath = document.getElementById("base-path");
  if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
  else
    {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      response = xmlhttp.responseText;
      itemlog.innerHTML=response;
    }
  }
  basepath = basepath.value
//    alert (lvalue + 'dq 1' + bvalue);
  var url = basepath + 'bib-entry.php';
//    alert(url);
  var args = 'bibnumber=' + bibnumber;
  args += '&fn=' + fn;
  args += '&recid=' + id;
  xmlhttp.open("POST",url,true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return false;
}

// onClick outside keypad
function SetFocus() {
  var bib = document.getElementById("bib_entry");
  if (bib) bib.focus();
}
  

function markAllRows( container_id, chkbox ) {
    var rows = document.getElementById(container_id).getElementsByTagName('tr');
    var checkbox;

    for ( var i = 0; i < rows.length; i++ ) {

        checkbox = rows[i].getElementsByTagName( 'input' )[0];

        if ( checkbox && checkbox.type == 'checkbox' ) {
            if ( checkbox.disabled == false ) {
              if (checkbox.checked){
                checkbox.checked = false;
              }
              else {
                checkbox.checked = true;
              }
            }
        }
    }

    return true;
}