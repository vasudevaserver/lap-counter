function SetBibNumber(bibnumber) {
  var response = '';
  var textfield = document.getElementById("bib-number");
  var itemlog = document.getElementById("entry-items");
  textfield.value = '';
  textfield.focus();
  var userid = document.getElementById("userid");
  var uid = userid.value;
  
  var xmlhttp;
  if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
  else
    {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      response = UpdateLog(itemlog, xmlhttp.responseText);
      itemlog.innerHTML=response;
    }
  }
  var basepath = document.getElementById("base-path");
  basepath = basepath.value
  var url = basepath + 'bib-entry.php';
  var args = 'bibnumber=' + bibnumber;
  args += '&userid=' + uid;
  xmlhttp.open("POST",url,true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  return;
}

function UpdateLog(itemlog, text) {
  var data = itemlog.innerHTML;
  data = data.split("\n");
  var len = data.length;
  var result = text + '\n';
  for (var i=0;i<len-1;i++)
  {
    result += data[i];
    result += '\n';
  }
  return result;
}


function SetRegistrationSelection() {
  var selection = document.getElementById('edit-name');
  var data = selection.value;
  
  alert(data);
}
