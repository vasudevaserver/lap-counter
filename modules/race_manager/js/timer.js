var starttime;
// var timer = Array(10,1);
var timerstarted = false;

function RaceStarted(racetime, index, ctr) {
  timerstarted = true;
  acttime = new Date().getTime();
  starttime = acttime - (racetime * 1000);
  obname = 'start' + index + 'c-' + ctr;
  startcontainer =document.getElementById(obname);
  startcontainer.style.display = 'none';
  obname = 'starttime' + index + '-' + ctr;
  starttimer = document.getElementById(obname);
  starttimer.value = starttime;
  showTimers();
}

function StartTimers() {
  var acttime;
  var start = false;
  var starter;
  var index = 0;
  var ctr = 1;
  if (!timerstarted) {
    starttime = new Date().getTime();
    acttime = starttime;
    timerstarted = true;
  }
  else {
    acttime = new Date().getTime();
  }
//  alert(acttime);
  var obname = '';
  t=setTimeout(function(){showTimers()},5);
  while (index < 10) {
    for (ctr=1;ctr<3;ctr++) {
      obname = 'start' + ctr + '-' + index;
      starter = document.getElementById(obname);
      if (!starter) {
        continue;
      }
      start = false;
      if (starter.value != 'started') {
        obname = 'choose' + ctr + '-' + index;
        selected = document.getElementById(obname);   
        if (selected) {
          if (selected.checked) {
            start = true;
          }
        }
        if (start) {
          starter.value = 'started';
//          timer[index,ctr-1] = acttime;
          obname = 'starttime' + ctr + '-' + index;
          starttimer = document.getElementById(obname);
          starttimer.value = acttime;
          obname = 'start' + ctr + 'c-' + index;
          startcontainer =document.getElementById(obname);   
          startcontainer.style.display = 'none';
        }
      }
    }
    index++;
  }
}

function ResetTimer(index, ctr) {
   starter = document.getElementById('start' + ctr + '-' + index);
   starttimer = document.getElementById('starttime' + ctr + '-' + index);
   starttimer.value = '';
   starter.value = 'start';
   selected = document.getElementById('choose' + ctr + '-' + index);
   if (selected) {
     selected.checked = false;
   }
   startcontainer = document.getElementById('start' + ctr + 'c-' + index);
   startcontainer.style.display = 'inline-block';
   timedisplay = document.getElementById('timer' + ctr + '-' + index);
   if (timedisplay) {
     timedisplay.innerHTML = '';
     timedisplay.style.display = 'none';
   }
   return false;
}

function showTimers()  {
  if (!timerstarted) {
    return false;
  }
  var acttime = new Date().getTime(); 
  t=setTimeout(function(){showTimers()},1000);
  var starter;
  var disp = '';
  var index = 0;
  var ctr = 1;
  while (index < 10) {
    for (ctr=1;ctr<3;ctr++) {
      obname = 'start' + ctr + '-' + index;
      starter = document.getElementById(obname);
      if (!starter) {
        continue;
      }
      if (starter.value == 'started') {
        starttimer = document.getElementById('starttime' + ctr + '-' + index);
        if (starttimer.value){
          disp = formatTime((acttime - starttimer.value) / 1000);
          timedisplay = document.getElementById('timer' + ctr + '-' + index);
          if (timedisplay) {
            timedisplay.innerHTML = disp;
            timedisplay.style.display = 'inline-block';
          }
        }
      }
    }
    index++;
  }
}

function SendTimer(index, ctr, id) {
  var disp = '';
  var acttime = new Date().getTime();
  var starttimer = document.getElementById('starttime' + ctr + '-' + index);
  var val = Math.floor(starttimer.value);
  if (!val){
    return false;
  }
  var diff = Math.floor((acttime - starttimer.value) / 1000);
  var xmlhttp;
  var response = '';
  if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
  else
    {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  var basepath = document.getElementById("base-path");
  basepath = basepath.value
  var url = basepath + 'bib-entry.php';
  var args = 'bibnumber=' + diff;
  args += '&fn=sendstarttime';
  args += '&recid=' + id + '-' + ctr;
  xmlhttp.open("POST",url,true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send(args);
  // window.location.reload(false); 
  return false;
}

function formatTime(time)  {
  var hours = Math.floor(time/3600);
//  var minutes = Math.floor(time/60);
  var minutes = Math.floor((time - (hours * 3600))/60);
  var seconds = time - (hours * 3600) - (minutes * 60);
  var seconds = Math.floor(seconds);
  var disp = '';
  if (seconds < 10) {
    disp += '0';
  }
  disp += seconds.toString();
  disp = minutes.toString() + ':' + disp;
  if (minutes < 10) {
    disp =  '0' + disp;
  }
  if (hours) {
    disp = hours.toString() + ':' + disp;
    if (hours < 10) {
      disp =  '0' + disp;
    }
  }
  return disp;
}


function showTimerArray() {
  var acttime = new Date().getTime(); 
  var disp = '';
  var index = 0;
  var ctr = 0;
  while (index < 10) {
    for (ctr=1;ctr<3;ctr++) {
      starttimer = document.getElementById('starttime' + ctr + '-' + index);
      if (starttimer.value){
        disp += index + ' || ' + ctr + ' \n';
        disp += formatTime((acttime - starttimer.value) / 1000);
        disp += ' - \n';
      }
    }
    index++;
  }
  var debug = document.getElementById('debug');
  debug.value = disp;
}
