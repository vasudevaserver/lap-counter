<?php
/**
 * Configuration settings which need to be loaded only during installation.
 *
 */

// The custom menus defined by this module

function race_manager_config_install_menus() {
  $menus = Array();
  
  $custom_menu = Array();
  $custom_menu['menu_name'] = 'menu-race-manager';
  $custom_menu['title'] = 'Race Manager';
  $custom_menu['description'] = 'The menu for the public interface.';
  $menus['menu-race-manager'] = $custom_menu;
  $custom_menu['menu_name'] = 'menu-race-manager-admin';
  $custom_menu['title'] = 'Race Manager Admin';
  $custom_menu['description'] = 'The administration interface menu.';
  $menus['menu-race-manager-admin'] = $custom_menu;
  $custom_menu['menu_name'] = 'menu-race-registration';
  $custom_menu['title'] = 'Race Registration';
  $custom_menu['description'] = 'The race registration sub-menu.';
  $menus['menu-race-registration'] = $custom_menu;

  $items = Array();
  
  $item = Array();
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Mgr';
  $item['link_path'] = 'race-admin/home';
  $item['options']['attributes']['title'] = 'Home';
  $item['hidden'] = 0;
  $item['weight'] = -50;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Final';
  $item['link_path'] = 'final';
  $item['options']['attributes']['title'] = 'Final Results';
  $item['hidden'] = 0;
  $item['weight'] = -49;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Scoreboard';
  $item['link_path'] = 'scoreboard';
  $item['options']['attributes']['title'] = 'Scoreboard';
  $item['hidden'] = 0;
  $item['weight'] = -48;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Log';
  $item['link_path'] = 'log';
  $item['options']['attributes']['title'] = 'Log';
  $item['hidden'] = 0;
  $item['weight'] = -47;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Runners';
  $item['link_path'] = 'entrants';
  $item['options']['attributes']['title'] = 'Registered Participants';
  $item['hidden'] = 0;
  $item['weight'] = -46;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Awards';
  $item['link_path'] = 'awards';
  $item['options']['attributes']['title'] = 'Category Results';
  $item['hidden'] = 0;
  $item['weight'] = -45;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager';
  $item['plid'] = 0;
  $item['link_title'] = 'Data';
  $item['link_path'] = 'entry-js';
  $item['options']['attributes']['title'] = 'Data entry forms';
  $item['hidden'] = 0;
  $item['weight'] = -44;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Home';
  $item['link_path'] = 'final';
  $item['options']['attributes']['title'] = 'Final Results';
  $item['hidden'] = 0;
  $item['weight'] = -50;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Registration';
  $item['link_path'] = 'race-admin/registration';
  $item['options']['attributes']['title'] = 'Registration';
  $item['hidden'] = 0;
  $item['weight'] = -49;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Events';
  $item['link_path'] = 'race-admin/event';
  $item['options']['attributes']['title'] = 'Events Management';
  $item['hidden'] = 0;
  $item['weight'] = -48;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Entrants';
  $item['link_path'] = 'race-admin/entrant';
  $item['options']['attributes']['title'] = 'Entrants Management';
  $item['hidden'] = 0;
  $item['weight'] = -47;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Start';
  $item['link_path'] = 'race-admin/timer';
  $item['options']['attributes']['title'] = 'Race start timer';
  $item['hidden'] = 0;
  $item['weight'] = -46;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-manager-admin';
  $item['plid'] = 0;
  $item['link_title'] = 'Settings';
  $item['link_path'] = 'race-admin/system';
  $item['options']['attributes']['title'] = 'System Options';
  $item['hidden'] = 0;
  $item['weight'] = -40;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-registration';
  $item['plid'] = 0;
  $item['link_title'] = 'Full';
  $item['link_path'] = 'race-admin/registration';
  $item['options']['attributes']['title'] = 'Participant registration';
  $item['hidden'] = 0;
  $item['weight'] = -50;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-registration';
  $item['plid'] = 0;
  $item['link_title'] = 'Quick';
  $item['link_path'] = 'race-admin/registration/short';
  $item['options']['attributes']['title'] = 'Quick race registration';
  $item['hidden'] = 0;
  $item['weight'] = -39;
  $items[] = $item;
  
  $item['menu_name'] = 'menu-race-registration';
  $item['plid'] = 0;
  $item['link_title'] = 'Manage';
  $item['link_path'] = 'race-admin/registration/manage';
  $item['options']['attributes']['title'] = 'Registration Management';
  $item['hidden'] = 0;
  $item['weight'] = -29;
  $items[] = $item;
  
  return Array('menus' => $menus, 'items'=>$items);
}
