<?php
/*
Plugin Name: Race Manager
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.06
Author: Medur C Wilson
Author URL: http://medur.ca
*/

/*  
	Copyright 2007  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// The current request's query string as an array.
function race_manager_get_query_string_array() {
  $qs = $_SERVER['QUERY_STRING'];
  $qs_array = Array();
  parse_str($qs, $qs_array);
  return $qs_array;
}

// A list of all events as an array for use in a select list.
function race_manager_event_select_list($display='name') {
  $res = db_select('race_event', 'r')
          ->fields('r', array('RACE_ID', $display))
          ->orderBy('RACE_ID')
          ->execute();
  $return = Array(0 => '- Select Event -');
  while (1) {
    $record = $res->fetchAssoc();
    if (!$record) {
      break;
    }
    $return[$record['RACE_ID']] = $record[$display];
  }
  return $return;
}


// A list of all current entrants as an array for use in a select list.
function race_manager_entrant_select_list() {
  $entrants = race_manager_entrants_current();
  $list = Array();
  foreach ($entrants as $entrant) {
    $entrant_id = $entrant->ENTRANT_ID;
    $name = $entrant->bib . ' ' . $entrant->full_name;
    $list[$entrant_id] = $name;
  }
  return $list;
}

/* A list of all events as an array for use in a select list.
 * This needs a dynamic update to 
 * restrict the list to editions of current event
 */

function race_manager_edition_select_list(
        $options = Array()) {
  $sql = 'SELECT race_edition.EDITION_ID , race_edition.race_edition , ';
  $sql .= 'race_event.RACE_ID, race_event.short_name, ';
  $sql .= 'race_edition.description ';
  $sql .= 'FROM race_edition ';
  $sql .= 'INNER JOIN race_event ON race_edition.RACE_ID = race_event.RACE_ID ';
  $wheres = array();
  if (array_key_exists('current', $options)) {
    if ($options['current']) {
      $wheres[] = '(race_edition.current = 1)';
    }
  }
  $t = '';
  if (array_key_exists('race_id', $options)) {
    $wheres[] = '(race_event.RACE_ID = ' . $options['race_id'] . ')';
  }
  if ($wheres) {
    $t = implode(' AND ', $wheres);
    $t = 'WHERE (' . $t . ') ';
  }
  $sql .= $t;
  if (array_key_exists('sort', $options)) {
    $sql .= 'ORDER BY ' . $options['sort'];
  }
  $res = db_query($sql);
  
  $return = Array();
  if (array_key_exists('info-row', $options)) {
    $return = Array(0=>'- Select Event Edition -');
  }
  foreach ($res as $record) {
    $value = $record->short_name . ' ' . $record->description;
    $return[$record->EDITION_ID] = $value;
  }
  return $return;
}

// Prepare the navigation bar data for the entrant page.
function race_manager_get_navigation($nav = Array()) {
  $entrant_id = NULL;
  if (array_key_exists('entrant-id', $nav)) {
    $entrant_id = intval($nav['entrant-id']);
  }
  $return = Array('entrant-id'=>$entrant_id);
  $target = NULL;
  if (array_key_exists('target', $nav)) {
    $target = $nav['target'];
  }
  $return['target'] = $target;
  $session_nav = $_SESSION['nav'];
  $session_nav = Array();
  $id = $nav['id'];
  $return['id'] = $id;
  $nav_size = $nav['size'];
  if (array_key_exists('nav-size', $_REQUEST)) {
    $nav_size = intval($_REQUEST['nav-size']);
  }
  elseif (array_key_exists('size', $session_nav)) {
//    $nav_size = intval($session_nav['size']);
  }
  $return['size'] = $nav_size;
  $nav_start = 0;
  $nav_total = $nav['total'];
  if (array_key_exists('nav-total', $_REQUEST)) {
    $nav_total = intval($_REQUEST['nav-total']);
  }
  elseif (array_key_exists('total', $session_nav)) {
//    $nav_total = intval($session_nav['total']);
  }
  $return['total'] = $nav_total;
  if ($id == 'entrant') {
    $nav_start = max(Array($nav_total - $nav_size, 0));
  }
  if (array_key_exists('nav-start', $_REQUEST)) {
    $nav_start = intval($_REQUEST['nav-start']);
  }
  elseif (array_key_exists('start', $session_nav)) {
//    $nav_start = intval($session_nav['start']);
  }
  if (array_key_exists('start', $session_nav)) {
    $nav_start = intval($session_nav['start']);
  }
  if (array_key_exists('submit-first', $_REQUEST)) {
    $nav_start = 0;
  }
  if (array_key_exists('submit-prev', $_REQUEST)) {
    $nav_start = max(Array($nav_start - $nav_size, 0));
  }
  if (array_key_exists('submit-next', $_REQUEST)) {
    $min = min(Array($nav_start + $nav_size, $nav_total - $nav_size));
    $nav_start = max(Array($min, 0));
  }
  if (array_key_exists('submit-last', $_REQUEST)) {
    $nav_start = max(Array($nav_total - $nav_size, 0));
  }
  $return['start'] = $nav_start;
  $_SESSION['nav'] = $return;
  return $return;
}


function race_manager_get_request_value($key, $default='') {
  global $_REQUEST;
  $rk = array_keys($_REQUEST);
  $return = $default;
  if (in_array($key, $rk) ) {
    if ($_REQUEST[$key]) {
      $return = $_REQUEST[$key];
    }
  }
  return $return;
}


// Convert a string HH:MM:SS to seconds
function race_manager_strtime_to_seconds($timeval, $ttype = 0) {
  $timevals = explode(':', $timeval);
  $seconds = 0;
  switch (count($timevals)) {
    case 1: // seconds only
      {
        foreach ($timevals as $time) {
          $seconds = intval(trim($time));
          break;
        }
        break;
      }
    case 2: // MM:SS
      {
        $time = trim($timevals[1]);
        $seconds = intval($time);
        $time = trim($timevals[0]);
        $mult = 60;
        $seconds += intval($time) * $mult;
        break;
      }
    case 3: // HH:MM:SS
      {
        $time = trim($timevals[2]); // SS
        $seconds = intval($time);
        $time = trim($timevals[1]); // MM
        $mult = 60;
        $seconds += intval($time) * $mult;
        $time = trim($timevals[0]); // HH
        $mult *= 60;
        $seconds += intval($time) * $mult;
        break;
      }
    default:
      break;
  }
  return $seconds;
}

// Format a php timestamp time as HH:MM:SS
function race_manager_format_timestamp($time, $ftype = 0) {
  $ftime = '';
  switch ($ftype) {
    case 0:
      $hours = intval($time / 3600);
      if ($hours) {
        $ftime = strval($hours) . ':';
      }
      else {
        $ftime = '0:';
      }
      $ltime = localtime($time);
      if ($ltime[1]) {
        $ftime .= str_pad($ltime[1], 2, "0", STR_PAD_LEFT) . ':';
      }
      else {
        $ftime .= '00:';
      }
      if ($ltime[0]) {
        $ftime .= str_pad($ltime[0], 2, "0", STR_PAD_LEFT);
      }
      else {
        $ftime .= '00';
      }
    break;
    case 1:
      $ltime = localtime($time);
      if ($ltime[2]) {
        $ftime = str_pad($ltime[2], 2, "0", STR_PAD_LEFT) . ":";
      }
      $ftime .= str_pad($ltime[1], 2, "0", STR_PAD_LEFT) . ":";
      $ftime .= str_pad($ltime[0], 2, "0", STR_PAD_LEFT);
    break;
  }
  return $ftime;
}

// Calculate the overall place of the next finisher
function race_manager_current_place() {
  $start_time = race_manager_current_event_start_time();
  $sstart_time = strftime("%Y-%m-%d %X", $start_time);
  $sql = 'SELECT count(*) as place FROM race_dataentry ';
  $sql .= 'WHERE ((deleted = 0) AND (';
  $sql .= 'chute_status >= 0) AND (';
  $sql .= 'lap_time >= :stt))';
  $sql .= 'GROUP BY deleted';
  $result = db_query($sql, array(':stt' => $sstart_time));
  $place = 1;
  $record = FALSE;
  foreach ($result as $record) {
    break;
  }
  if ($record) {
    $place = $record->place + 1;
  }
  return $place;
}

// Retrieve a regular php timestamp and a millisecond value
function race_manager_time_with_milliseconds($time = 0) {
  $mtime = $time;
  $return = Array();
  if (!$time) {
    $mtime = microtime(TRUE) + RACE_MANAGER_TZ_CORRECTION;
  }
  $itime = intval($mtime);
  $msec = ($mtime - $itime) * 1000;
  $time = strftime("%Y-%m-%d %X", $itime);
  $return['time'] = $time;
  $return['msec'] = $msec;
  return $return;
}


// Get the offset value for the race edition
function race_manager_get_offset($edition_id) {
  $return = race_manager_option_get(8);
  $return = $return[1];
  $sql = 'SELECT offset FROM race_edition WHERE edition_id = ';
  $res = db_select('race_edition', 'e')
          ->fields('e')
          ->condition('edition_id', $edition_id)
          ->execute();
  $record = $res->fetchAssoc();
  if ($record) {
    $return = intval($record['offset']);
  }
  return $return;
}

// Get the applicable category regimes as a list of regime ids
function race_manager_get_category_regimes($options) {
  $entrant = FALSE;
  $event_id = FALSE;
  if (isset($options['event_id'])) {
    $event_id = $options['event_id'];
  }
  elseif (isset($options['edition_id'])) {
    $edition = db_select('race_edition', 'e')
            ->fields('e')
            ->condition('EDITION_ID', $options['edition_id'])
            ->execute();
    $edition = $edition->fetchAssoc();
    $event_id = $edition['race_id'];
  }
  elseif (isset($options['entrant_id'])) {
    $opt = Array('entrant_id' => $options['entrant_id']);
    $entrant = race_manager_entrant_info($opt);
    $event_id = $entrant->race_id;
  }
  if ($event_id) {
    $event_ids = Array($event_id);
  }
  else {
    $events = race_manager_current_events();
    $event_ids = Array();
    foreach ($events as $edition) {
      $event_ids[] = $edition->race_id;
    }
  }
  $events = '';
  if ($event_ids) {
    if (count($event_ids) == 1) {
      $event_ids = '= ' . $event_ids[0];
    }
    else {
      $event_ids = implode(',', $event_ids);
      $event_ids = ' IN ('.$event_ids . ')';
    }
  }
  $sql = 'SELECT regime_id FROM race_category_event ';
  $sql .= ' WHERE event_id ' . $event_ids;
  $sql .= ' GROUP BY regime_id';
//  $regimes = db_query($sql, Array('eids' => $event_ids));
  $regimes = db_query($sql);
  $return = Array();
  foreach ($regimes as $regime) {
    $return[$regime->regime_id] = 1;
  }
  if ($return) {
    $return = array_keys($return);
  }
  return $return;
}

// Shortcut function to return the system operating mode.
function race_manager_get_mode() {
  return race_manager_option_get(9);
}

// Shortcut function to return the current timezone offset.
function race_manager_get_timezone_offset() {
  $return = race_manager_option_get(28, 0);
  return intval($return[1]);
}

// Sets an option value in the options table
function race_manager_option_set($option_id, $args = Array()) {
  $result = db_select('race_options', 'ro')
          ->fields('ro')
          ->condition('option_id', $option_id, '=')
          ->fetchAssoc()
          ->execute();
  if ($result) {
    $values = Array();
    if (isset($args['option_name'])) {
      $values['option_name'] = $args['option_name'];
    }
    if (isset($args['option_value'])) {
      $values['option_value'] = $args['option_value'];
    }
    if (count($values)) {
      $result = db_update('race_options')
          ->fields($values)
          ->condition('option_id', $option_id, '=')
          ->execute();
    }
  }
  else {
    array_merge($args, array('option_id' => $option_id));
    $result = db_insert('race_options')
          ->fields($args)
          ->execute();
  }
  return race_manager_option_get($option_id);
}

// Get an option value from the options table
function race_manager_option_get($option_id, $default = '') {
  $res = db_select('race_options', 'r')
          ->fields('r')
          ->condition('option_id', $option_id, '=')
          ->execute()
          ->fetchAssoc();
  if (!$res) {
    $return = Array(1 => (int)$default);
  }
  elseif (count($res)) {
    $return = Array(1 => $res['option_value']);
  }
  else {
    $return = Array(1 => $default);
  }
  return $return;
}

/**
 * Return a list of all current editions
 */
function race_manager_current_events($args = Array()) {
  return race_manager_events(Array(
      'current'=>1,
      'entrant_count' => 1,
      )
          );
}

/**
 * Return a list of all events
 */
function race_manager_events($options = Array()) {

  $ecount = FALSE;
  if (array_key_exists('entrant_count', $options)) {
    if ($options['entrant_count']) {
      $ecount = TRUE;
    }
  }
  $acount = FALSE;
  if (array_key_exists('archive_count', $options)) {
    if ($options['archive_count']) {
      $acount = TRUE;
    }
  }
  
  $sql = 'SELECT race_event.short_name, race_event.name, ';
  $sql .= 'race_event.lap_distance, race_event.reject_limit, ';
  $sql .= 'race_edition.start_time, race_edition.offset, ';
  $sql .= 'race_edition.finish_time, race_edition.EDITION_ID, ';
  $sql .= 'race_edition.offset_start, ';
  $sql .= 'race_edition.race_id, race_edition.race_edition, ';
  $sql .= 'race_edition.description, race_edition.current, ';
  $sql .= 'race_edition.bib_range_start, race_edition.status';
  if ($ecount) {
    $sql .= ', pcount.ecount';
  }
  if ($acount) {
    $sql .= ', arcount.acount';
  }
  $sql .= ' FROM race_event LEFT JOIN race_edition ';
  $sql .= ' ON race_event.RACE_ID = race_edition.race_id ';
  if ($acount) {
    $ssql = 'SELECT COUNT(race_archive.ARCHIVE_ID) as acount, ';
    $ssql .= 'MAX(race_entrant.edition_id) as edition_id ';
    $ssql .= 'FROM race_entrant INNER JOIN race_archive ';
    $ssql .= 'ON race_archive.entrant_id = race_entrant.entrant_id ';
    $ssql .= 'GROUP BY race_entrant.edition_id ';
    $sql .= 'LEFT JOIN (' . $ssql . ') AS arcount ON ';
    $sql .= ' race_edition.EDITION_ID = arcount.edition_id ';
  }
  if ($ecount) {
    $sssql   = 'SELECT MAX(race_laps.entrant_id) as eid, ';
    $sssql  .= 'COUNT(race_laps.LAP_ID) as laps ';
    $sssql  .= 'FROM race_laps where deleted = 0 GROUP BY entrant_id';
    $ssql    = 'SELECT COUNT(e_laps.eid) as ecount, edition_id ';
    $ssql   .= 'FROM race_entrant INNER JOIN ( ' . $sssql . ' ) as e_laps ';
    $ssql   .= 'ON race_entrant.entrant_id = e_laps.eid ';
    $ssql   .= 'GROUP BY race_entrant.edition_id ';
    $sql    .= 'LEFT JOIN (' . $ssql . ') AS pcount ON ';
    $sql    .= ' race_edition.EDITION_ID = pcount.edition_id ';
  }
  $wheres = array();
  if (array_key_exists('current', $options)) {
    if ($options['current']) {
      $wheres[] = '(race_edition.current = 1)';
    }
  }
  if (array_key_exists('race_id', $options)) {
    if ($options['race_id']) {
      $wheres[] = '(race_event.RACE_ID = ' . $options['race_id'] . ')';
    }
  }
  if (array_key_exists('edition_id', $options)) {
    if ($options['edition_id']) {
      $wheres[] = '(race_edition.EDITION_ID = ' . $options['edition_id'] . ')';
    }
  }
  $t = '';
  if ($wheres) {
    $t = implode(' AND ', $wheres);
    $t = 'WHERE (' . $t . ') ';
  }
  $sql .= $t;
  $sort = '';
  if (array_key_exists('sort', $options)) {
    if ($options['sort']) {
      $sort = ' ORDER BY ' . $options['sort'];
    }
  }
  if (!$sort) {
    $sort = ' ORDER BY race_edition.start_time DESC';
  }
  $sql .= $sort;
  $events = db_query($sql);
  $return = Array();
  foreach ($events as $event) {
    $return[$event->EDITION_ID] = $event;
  }
  return $return;
}

/**
 * Return the clock start time
 * There may me multiple events being staged at staggered start times.
 * This function returns the earliest start time of all current events
 */

function race_manager_current_event_start_time($edition_id = 0) {
  $options = Array('current' => 1);
  if ($edition_id) {
    $options = Array('edition_id' => $edition_id);
  }
  $current_events = race_manager_events($options);
  $time = -1;
  foreach ($current_events as $edition) {
    $ttime = strtotime($edition->start_time);
    if ($time == -1) {
      $time = $ttime;
    }
    elseif ($ttime < $time) {
      $time = $ttime;
    }
  }
  return $time;
}


  /**
 * Return a list of counter stations
 */
function race_manager_counter_stations() {
  $sql = 'SELECT * FROM race_counter_stations';
  $stations = db_query($sql);
  $return = Array();
  foreach ($stations as $station) {
    $return[$station->ID] = $station;
  }
  return $return;
}

 /*
 * Generate a new (unused) race id
 */
function race_manager_race_id_suggestion() {
  $res = db_select('race_event', 'r')
          ->fields('r')
          ->orderBy('RACE_ID')
          ->execute();
  $ids = Array();
  foreach ($res as $record) {
    $ids[] = $record->RACE_ID;
  }
  for ($i = 1; $i < 1000; $i++) {
    if (!in_array($i, $ids)) {
      return $i;
    }
  }
  return 0;
}

function race_manager_get_next_bib_number($edition_id = 0) {
  $bib_start = 0;
  if ($edition_id) {
    $options = Array('edition_id' => $edition_id);
    $options['sort'] = 'EDITION_ID DESC';
    $res = race_manager_events($options);
    foreach ($res as $id => $edition) {
      break;
    }
    if (!$edition) {
      return $bib_start;
    }
    $bib_start = $edition->bib_range_start;
  }
  $options = Array();
  $options['index'] = 'bib';
  $options['sort'] = 'bib DESC';
  $entrants = race_manager_entrants_current($options);
  foreach ($entrants as $bib => $entrant) {
    break;
  }
  if (!$bib) {
    return $bib_start;
  }
  $bib += 1;
  return $bib;
}

/*
 * This function takes 2 strings and calculates an age
 */
function race_manager_calculate_age($dob, $as_of_date) {
  if (is_string($dob)) {
    $date = strtotime($dob);
  }
  if (is_string($as_of_date)) {
    $as_of_date = strtotime($as_of_date);
  }
  $year1 = intval(strftime('%Y', $date));
  $days1 = intval(strftime('%j', $date));

  $year2 = intval(strftime('%Y', $as_of_date));
  $days2 = intval(strftime('%j', $as_of_date));
  $age = $year2 - $year1;
  if ($days1 > $days2) {
    // Assume the time of birth is a billionth of a second after midnight
    // 
    $age -= 1;
  }
  return $age;
}

/*
 * Create a name code from the full name
 */
function race_manager_calculate_name_code($full_name) {
  $name = explode(' ', $full_name); 
  $name_code = '';
  foreach ($name as $part) {
    if (strlen($part)) { // allow for double spaces in name
      $name_code .= ucfirst($part);
    }
  }
  return $name_code;
}

// Detect the nature of the user agent
// Use to change the display for mobile apps
function race_manager_detect_mobile(){
  $user_agent = $_SERVER['HTTP_USER_AGENT'];
  $return = FALSE;
  $test = explode('mobile', strtolower($user_agent));
  if (count($test) > 1) {
    $return = TRUE;
  }
  return $return;
}

function race_manager_calculate_place($edition_id) {
  # Count the number of finishers
  return 0;
  $sql = 'SELECT COUNT(LAP_ID)as place FROM race_laps ';
  $sql .= ' INNER JOIN race_entrant ';
  $sql .= ' ON race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= ' WHERE edition_id = :eid';
  $res = db_query($sql, Array(':eid'=>$edition_id));
  $return = 0;
  $record = $res->fetchAssoc();
  if ($record) {
    $return = $record['place'];
  }
  return $return;
}

function race_manager_calculate_split($stime, $ftime) {
  #find the split time between the 2 times and return as number of seconds
  $tftime = strtotime($ftime);
  $tstime = strtotime($stime);
  $diff = $tftime - $tstime;
  return $diff;
}

function race_manager_get_date_default_value($date) {
  if (is_int($date)) {
    $date = date('Y-m-d', $date);
  }
  $return = array();
  $val = strftime('%Y',  strtotime($date));
  $return['year'] = intval($val);
  $val = strftime('%m',  strtotime($date));
  $return['month'] = intval($val);
  $val = strftime('%d',  strtotime($date));
  $return['day'] = intval($val);
  return $return;
}

function race_manager_get_table_info($table) {
  $sql = 'SHOW FIELDS FROM ' . $table;
  $res = db_query($sql);
  $return = Array();
  foreach ($res as $record) {
    $return[$record->Field] = $record;
  }
  return $return;
}

/**
 * Check to see if there are custom menus installed.
 * If not then install them
 */

function race_manager_check_menus() {
  $menu = menu_load('menu-race-manager');
  if (!$menu) {
    race_manager_admin_menu_setup();
  }
}



