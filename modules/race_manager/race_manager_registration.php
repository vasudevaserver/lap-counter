<?php
/*
Name: race_manager_registration
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2014  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Delete all entrant table entries for the selected edition
function race_manager_registration_clear($edition_id) {
  if (!$edition_id) {
    return;
  }
  $sql = 'SELECT race_entrant.ENTRANT_ID, LAP_ID FROM race_entrant INNER JOIN race_laps ';
  $sql .= 'ON race_entrant.ENTRANT_ID = race_laps.ENTRANT_ID ';
  $sql .= 'WHERE edition_id = :eid';
  $res = db_query($sql, Array(':eid'=>$edition_id));
  $found = FALSE;
  foreach ($res AS $record) {
    $found = TRUE;
    break;
  }
  if ($found) {
    // Do not delete entrants for race with results recorded
    return;
  }
  $sql = 'DELETE FROM race_entrant WHERE edition_id = :eid';
  $res = db_query($sql, Array(':eid'=>$edition_id));
}


function race_manager_registration_rollover_submit($args=Array()) {
  // Use to roll over an edition registration to a newer edition.
  $source_id = 0;
  if (array_key_exists('source', $args)) {
    $source_id = $args['source'];
  }
  $target_id = 0;
  if (array_key_exists('target', $args)) {
    $target_id = $args['target'];
  }
  $res = db_select('race_entrant', 'r')
          ->fields('r')
          ->condition('edition_id', $source_id)
          ->execute();
  foreach($res as $record) {
    $bib = $record->bib;
    $data = Array();
    $data['partcpt_id'] = $record->partcpt_id;
    $data['bib'] = $bib;
    $data['name_code'] = $record->name_code;
    $data['edition_id'] = $target_id;
    $data['full_name'] = $record->full_name;
    $data['alt_start'] = $record->alt_start;
    $sql = 'SELECT * FROM race_entrant ';
    $sql .= 'WHERE ((edition_id = ' . $target_id . ') AND (';
    $sql .= 'bib = ' . $bib . '))';
    $check = db_query($sql);
    $count = 0;
    foreach ($check as $record) {
      $pid = $record->partcpt_id; 
      $count+=1;
      break;
    }
    if ($count > 0) {
      $entrant_id = $record->ENTRANT_ID;
      if ($entrant_id) {
        $update = db_update('race_entrant')
                ->condition('ENTRANT_ID', $entrant_id)
                ->fields($data)
                ->execute();
      }
    }
    else {
      $entrant_id = db_insert('race_entrant')
              ->fields($data)
              ->execute();
    }
  }
}


function race_manager_registration_import_submit($entrydata=Array()) {
  // Import a list of names and bib numbers for bulk registration.
  $edition_id = $entrydata['edition-id'];
  if (count($entrydata) == 0) {
    return;
  }
  if (!$edition_id) {
    return;
  }
  $sq = chr(39); // single quote character
  if (!array_key_exists('data', $entrydata)) {
    return;
  }
  $data = $entrydata['data'];
  $datalength = strlen($data);
  $tmp = '';
  $ctr = 0;
  // assemble a list
  $namelist = Array();
  $line_items = explode(chr(13), $data);
  foreach ($line_items as $line_item) {
    $entrant_data = Array();
    // tab-delimited data is required
    $dataset = explode(chr(9), $line_item);
    $t = trim(implode('', $dataset));
    if (strlen($t) > 0) {
      // Capture data line by line and load into array
      /* the partcpt_id is optional - really useful if provided
       *   but may not be available in the case of new registrants etc
       */
      $partcpt_id = 0;
      $entrant_data['partcpt_id'] = $partcpt_id;
      $bib = intval(trim($dataset[0]));
      $entrant_data['bib'] = $bib;
      $lname = $dataset[1];
      $lname = trim($lname, ' ');
      $entrant_data['last_name'] = $lname;
      $entrant_data['first_name'] = $lname;
      $entrant_data['full_name'] = $lname;
      $entrant_data['gender'] = trim($dataset[2]);
      $entrant_data['edition_id'] = $edition_id;
      $name_code = ucfirst($lname);
      $entrant_data['name_code'] = $name_code;
      // Capture data line by line and load into array
      /* The entrants need to be assigned a unique bib number.
       *    At some point this module might provide a bib assignment 
       *    utility but for now it must be done fully in advance.
       */
      if (!array_key_exists('bib', $entrant_data)) {
         continue;
      }
      $bib = intval($entrant_data['bib']);
      if ($bib <=0) {
        // BIB <= 0 NOT ALLOWED !!!
        continue;
      }
      $namecode = $entrant_data['name_code'];
      $fullname = $entrant_data[ 'full_name'];
      $firstname = $entrant_data[ 'first_name'];
      $lastname = $entrant_data[ 'last_name'];
      $gender = $entrant_data[ 'gender'];
      $pedition_id = $entrant_data['edition_id'];
      if (!$pedition_id) {
        $pedition_id = $edition_id;
      }
      $pid = intval($entrant_data['partcpt_id']);
      if (!$pid) {
      /* Try to find the partcpt_id from the name_code
       * The name_code may have changed (!!!).
       * Check both the participant table and the entrant table
       */
      
        $sql  = 'SELECT p.*, e.name_code, e.partcpt_id ';
        $sql .= 'FROM race_partcpt as p ';
        $sql .= ' LEFT JOIN race_entrant as e ';
        $sql .= ' ON p.ID = e.partcpt_id ';
        $sql .= ' WHERE ((p.name_code = ' . $sq . strval($namecode) . $sq . ') ';
        $sql .= ' OR (e.name_code = ' . $sq . strval($namecode) . $sq . ')) ';
        $res  = db_query($sql);
        $record = $res->fetchAssoc();
        if ($record) {
          $pid = $record['ID'];
        }
        else {
          // If it is not found then create a new participant
          $insert_data = Array();
          $insert_data['full_name'] = $fullname;
          $insert_data['first_name'] = $firstname;
          $insert_data['last_name'] = $lastname;
          $insert_data['name_code'] = $namecode;
          $insert_data['gender'] = $gender;
          $pid = race_manager_create_participant($insert_data);
        }
        // This needs to be set up at some point.
        $age = 0;
        
        /* Next check the entrant table to see if this participant 
         * is already registered:
         */
        $res = db_select('race_entrant', 'e')
            ->fields('e')
            ->condition('partcpt_id', $pid, '=')
            ->condition('edition_id', $pedition_id, '=')
            ->execute();
        $record = $res->fetchAssoc();
        // Prepare the line item data for insert or update in the entrants table  
        $insert_data = Array();
        $insert_data['partcpt_id'] = $pid;
        $insert_data['name_code'] = $namecode;
        $insert_data['edition_id'] = $pedition_id;
        $insert_data['bib'] = $bib;
        $insert_data['full_name'] = $fullname;
        $insert_data['current_age'] = $age;
        $insert_data['city'] = $city;
        $insert_data['state'] = $state;
        $insert_data['country'] = $country;
        // Here is the test
        if (!$record) {
          /* This participant is not registered in the db for this edition.
           * so register now:
           */
          $entrant_id = db_insert('race_entrant')
                ->fields($insert_data)
                ->execute();
        }
        else {
          // Do an update query on the existing record
          $entrant_id = $record['ENTRANT_ID'];
          $res = db_update('race_entrant')
                ->fields($insert_data)
                ->condition('ENTRANT_ID', $entrant_id, '=')
                ->execute();
        }
        $namelist[$pid] = $namecode;
      }
    }
  }
}


function _race_manager_registration_autocomplete($string) {
  $matches = array();
  if (strlen($string) < 2) {
    return drupal_json_output($matches);
  }
  
  $result = db_select('race_partcpt', 'r')
    ->fields('r', array('ID','full_name'))
    ->condition('full_name', '%' . db_like($string) . '%', 'LIKE')
    ->execute();
  // save the query to matches
  foreach ($result as $record) {
    $matches[$record->full_name . ' (' . $record->ID . ')'] = check_plain(
            $record->full_name);
  }
  // Return the result to the form in json
  return drupal_json_output($matches);
}

function race_manager_registration_submit($args, $reg_type = 'long') {
//  drupal_set_message('message');
  $form_state = $args['form_state'];
  $input = $form_state['input'];
  $partcpt_id = intval($input['current-id']);
  if (!$partcpt_id) {
    return 0;
  }
  $edition_id = intval($input['select_edition_id']);
  if (!$edition_id) {
//    return 0;
  }
  $bib = intval($input['bib']);
  $offset = intval($form_state['values']['alt_start']);
  $entrant_id = race_manager_register_participant(
          $edition_id,
          $partcpt_id,
          $bib,
          $input,
          $offset,
          $reg_type
          );
  return $entrant_id;
}

/* Function: race_manager_register_participant
 * Enter this participant in the selected eventj
 * 
 * create a record in the entrant table and obtain the entrant id
 * calculate the new entrant's membershio on the category tables
 * and return the entrant_id
 */

/* Return Registration data for a participant
 * Note: this participant may not be registered in an event, in which case 
 * return an empty recordset
 */

function race_manager_registration_info($partcpt_id) {
  $sql = 'SELECT race_entrant.*, race_edition.* FROM race_entrant ';
  $sql .= 'INNER JOIN race_edition ON ';
  $sql .= 'race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((race_edition.current = 1) AND (';
  $sql .= 'race_entrant.partcpt_id = ' . $partcpt_id . '))';
  $res = db_query($sql);
  $entrant = FALSE;
  foreach ($res as $entrant) {
    break;
  }
  return $entrant;
}

function race_manager_register_participant(
        $edition_id,
        $partcpt_id,
        $bib,
        $input,
        $offset_start,
        $reg_type = 'long'
        ) {

  // Check the partcpt id:
  $data = Array();
  $pid = $partcpt_id;
  $fname = trim($input['first_name']);
  if ($reg_type == 'short') {
    $lname = '';
  }
  else {
    $lname = trim($input['last_name']);
  }
  $full_name = trim($fname . ' ' . $lname);
  $name_code = race_manager_calculate_name_code($full_name);
  $dob = '1969-12-31';
  if (isset($input['dob'])) {
    $dob = Array(strval($input['dob']['year']));
    $dob[] = strval($input['dob']['month']);
    $dob[] = strval($input['dob']['day']);
    $dob = implode('-', $dob);
  }
  $data['gender'] = trim($input['gender']);
  $data['first_name'] = $fname;
  $data['name_code'] = $name_code;
  $data['dob'] = $dob;
  if ($reg_type == 'long') {
    $data['last_name'] = $lname;
    $data['full_name'] = $full_name;
    $data['add1'] = $input['add1'];
    $data['add2'] = $input['add2'];
    $data['city'] = $input['city'];
    $data['state'] = $input['state'];
    $data['country'] = $input['country'];
    $data['postalcode'] = $input['postalcode'];
  }
  if (!$pid) {
    $pid = race_manager_create_participant($data);
  }
  else {
    $data['id'] = $pid;
    race_manager_update_participant_info($data);
  }
  if (!$edition_id) {
//    Do not register this participant in an event at this time
//    Just save and exit;
    return 0;
  }
    // Check the entrant table
  $sql = 'SELECT race_entrant.* FROM ';
  $sql .= 'race_entrant INNER JOIN race_edition ';
  $sql .= 'ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((race_edition.current = 1) AND (';
  $sql .= 'race_entrant.bib = '.$bib .'))';
  
  $res = db_query($sql);
  $record = $res->fetchAssoc();
    // Get a list of the fields in the target table
  $fields = race_manager_get_table_info('race_entrant');
    // Prepare the line item data for insert or update in the entrants table
  $insert_data = Array();
  $insert_data['partcpt_id'] = $pid;
  $insert_data['edition_id'] = $edition_id;
  $insert_data['bib'] = $bib;
  $insert_data['alt_start'] = ($offset_start) ? 1 : 0;
  $insert_data['name_code'] = $name_code;
  $insert_data['edition_id'] = $edition_id;
  $insert_data['full_name'] = $full_name;
  if ($reg_type == 'long') {
    $start_time = race_manager_current_event_start_time($edition_id);
    $age = race_manager_calculate_age($dob, $start_time);
    $insert_data['current_age'] = $age;
    $insert_data['add1'] = $input['add1'];
    $insert_data['add2'] = $input['add2'];
    $insert_data['city'] = $input['city'];
    $insert_data['state'] = $input['state'];
    $insert_data['country'] = $input['country'];
    $insert_data['postalcode'] = $input['postalcode'];
    $insert_data['club'] = $input['club'];
    $insert_data['current_bio'] = $input['bio'];
  }
    // Here is the test
  if (!$record) {
      // This participant is not registered in the db for this edition.
    $entrant_id = db_insert('race_entrant')
            ->fields($insert_data)
            ->execute();
    $message = $full_name . ' (' . $bib . ') has been registered.';
    drupal_set_message($message);
  }
  else {
        // Do an update query on the existing record
    $entrant_id = $record['ENTRANT_ID'];
    $res = db_update('race_entrant')
            ->fields($insert_data)
            ->condition('ENTRANT_ID', $entrant_id, '=')
            ->execute();
    $message = $full_name . ' (' . $bib . ') has been updated.';
    drupal_set_message($message);
  }
  race_manager_entrant_assign_categories($entrant_id);
  return $entrant_id;
}
