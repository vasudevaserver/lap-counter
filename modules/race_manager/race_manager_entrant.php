<?php
/*
Name: Lap Counter
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2014  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Iterate though all currently registered participants and update
 * the age on race day value
 */ 


 /**
 * Return a list of counter stations as a select menu
 */
function race_manager_entrant_station_selection($options=Array()) {
    $races = race_manager_current_events();
    $stations = race_manager_counter_stations();
    $output = '<h6 id="race-manager-station-select-menu">';
    $output = 'Show Runners for: Race: ';
    $race_array = Array();
    $qstring = $_SERVER['QUERY_STRING'];
    $base_url = $_SERVER['REDIRECT_URL'];
    $qstr = Array();
    parse_str($qstring, $qstr);
    unset($qstr['station-id']);
    foreach ($races as $edition_id => $race) {
      $qstr['edition-id'] = $edition_id;
      $qstring = http_build_query($qstr);
      $href = $base_url . '?' . $qstring;
      $output .= ' <a href="' . $href .'">' . $race->short_name . '</a>';
    }
    unset($qstr['edition-id']);
    $output .= ' Station: ';
    foreach ($stations as $station) {
      $sid = $station->ID;
      $qstr['station-id'] = $sid;
      $qstring = http_build_query($qstr);
      $href = $base_url . '?' . $qstring;
      $output .= ' <a href="' . $href .'">' . $sid . '</a>';
    }
    $output .= '</h6>';
    return $output;
}
  
 /**
 * Return a log of laps filtered by counter station
 */
function race_manager_entrant_station_log_table($options=Array()) {
  // $postresults = race_manager_handle_post_submit();
  $datacount = intval(race_manager_entrant_record_count());
  $size = race_manager_station_size();
  $size = intval($size[1]);
  $navigation = Array();
  $navigation['id'] = 'log';
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation = race_manager_get_navigation($navigation);
  $options['HIDE_DELETED_LAPS'] = TRUE;
  $logdata = race_manager_log(0, $navigation, $options);
  $output = '<div id="race-manager-listing">
    ';
  $output .= race_manager_entrant_station_selection();

  $output .= '<table id="race-manager-station-log">
    <tbody>
  ';
  $output .= race_manager_log_table_rows($logdata);
  $output .= '</tbody>
  ';
  $output .= '</table>
  ';
  $output .= '</div>';
  return $output;
}
  
 /**
 * Return a list of all current entrants keyed on bib number
 */
function race_manager_entrants_current($options=Array()) {
  $bib = 0;
  if (isset($options['bib'])) {
    $bib = $options['bib'];
  }
  $showlapdata = FALSE;
  if (array_key_exists('lapdata', $options)) {
    $showlapdata = $options['lapdata'];
  }
  $current = TRUE;
  if (array_key_exists('current', $options)) {
    $current = $options['current'];
  }
  $entrant_id = 0;
  if (array_key_exists('entrant_id', $options)) {
    $entrant_id = $options['entrant_id'];
    $current = FALSE;
  }
  $partcpt_id = 0;
  if (array_key_exists('partcpt_id', $options)) {
    $partcpt_id = $options['partcpt_id'];
    $current = FALSE;
  }
  $edition_id = 0;
  if (array_key_exists('edition_id', $options)) {
    $edition_id = $options['edition_id'];
    $current = FALSE;
  }
  $sql = 'SELECT race_entrant.ENTRANT_ID, race_entrant.partcpt_id, ';
  $sql .= 'race_entrant.name_code, race_entrant.bib, race_entrant.ctr_station,';
  $sql .= 'gender, race_partcpt.dob, race_event.name as race_name, ';
  $sql .= 'race_entrant.full_name, race_entrant.current_bio, ';
  $sql .= 'race_entrant.current_age, race_entrant.city, ';
  $sql .= 'race_entrant.state, race_entrant.country, ';
  $sql .= 'race_event.lap_distance, race_event.short_name, ';
  $sql .= 'race_event.reject_limit, ';
  $sql .= 'race_edition.EDITION_ID, race_edition.race_id, ';
  $sql .= 'race_edition.race_edition, '; 
  $sql .= 'race_edition.finish_time, race_edition.start_time, ';
  $sql .= 'TIMESTAMPADD(SECOND,(race_edition.offset_start * ';
  $sql .= 'race_entrant.alt_start), race_edition.start_time) as mod_start ';
  if ($showlapdata) {
    $sql .= ', MAX(race_laps.lap) AS lap ';
  }
  $sql .= 'FROM race_entrant INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'INNER JOIN race_partcpt ON race_entrant.partcpt_id = race_partcpt.ID ';
  $sql .= 'INNER JOIN race_event ON race_edition.race_id = race_event.RACE_ID ';
  if ($showlapdata) {
    $sql .= 'LEFT JOIN race_laps ';
    $sql .= 'ON race_entrant.ENTRANT_ID = race_laps.entrant_id ';
  }
  
  $wheres = Array();
  if ($current) {
    $wheres[] = 'race_edition.current = 1';
  }
  if ($showlapdata) {
    $wheres[] = 'race_laps.deleted = 0';
  }
  if ($edition_id) {
    $wheres[] = 'race_entrant.edition_id = ' . $edition_id;
  }
  if ($bib) {
    $wheres[] = 'race_entrant.bib = ' . $bib;
  }
  if ($entrant_id) {
    $wheres[] = 'race_entrant.ENTRANT_ID = ' . $entrant_id;
  }
  if ($partcpt_id) {
    $wheres[] = 'race_entrant.partcpt_id = ' . $partcpt_id;
  }
  if (count($wheres)) {
    $where = implode(') AND (', $wheres);
    $where = 'WHERE ((' . $where . ')) ';
    $sql .= $where;
  }

  if (array_key_exists('sort', $options)) {
    $sql .= 'ORDER BY ' . $options['sort'];
  }  
  $res = db_query($sql);
  $return = Array();
  foreach ($res AS $record) {
    $entrant_id = intval($record->ENTRANT_ID);
    if (isset($options['age'])) {
      if ($options['age']) {
        $start_time = $record->start_time;
        $dob = $record->dob;
        if (!$dob) {
          $dob = date('Y-m-d');
        }
        $age = race_manager_calculate_age($dob, $start_time);
        $record->age = $age;
      }
    }
    $key = $entrant_id;
    if (array_key_exists('index', $options)) {
      $index = $options['index'];
      if (property_exists($record, $index)) {
        $key = $record->$index;
      }
    }
    $return[$key] = $record;
  }
  return $return;
}

/* Return a lap listing for a race entrant
 */
function race_manager_entrant_table($entrant=NULL) {
  if (!$entrant) {
    return 'No Entrant Selected';
  }
  $lapeditpermssion = user_access('add edit delete laps');
  $entrant_id = $entrant->ENTRANT_ID;
  $datacount = intval(race_manager_entrant_record_count($entrant_id));
  // retrieve the batch size for the entrant table
  $size = race_manager_entrant_size();
  $size = intval($size[1]);
  $navigation = Array('id' => 'entrant');
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation['target'] = race_manager_entrant_base_path($entrant_id);
  $navigation['entrant-id'] = $entrant_id;
  $navigation = race_manager_get_navigation($navigation);
  $navbar = race_manager_navigation_bar($navigation);

  $output = '<div id="race-manager-listing">
    ';
  $lapdata = race_manager_entrant_laps($entrant_id, $navigation, 'time');
  $entrant_name = $entrant->full_name;
  $bib = $entrant->bib;
  // load the form here to process any lap edit requests
  $form_edit_lap = FALSE;
  $output .= '<table id="race-manager-subheader">
    <tr><td>
    ';
  $output .= $navbar;
  $content = drupal_render($form_goto_entrant_data);
  $output .= $content;
  if ($lapeditpermssion) {
    $form_edit_lap = drupal_get_form('race_manager_edit_lap_form');
    $html = '<div class="back-link"> <a ' . 'href="' ;
    $html .= 'race-admin/registration?select_bib='.$bib;
    $html .= '" >Personal Data</a></div>';
    $output .= $html;
  }
  $output .= '</td><td>';
    // v 7.x-0.18
    if ($lapeditpermssion) {
      $output .= '<div id="lapcounter-subtitle" >
         Insert Lap
        </div>';
      $output .= '
      </td><td>
     ';
      $path = trim(RACE_MANAGER_BASE_PATH);
      if ($path == '/') {
        $path = '';
      }
      $output .= '<form id="race-manager-insert-lap" method = "POST" 
        enctype="multipart/form-data" action="' . $path . '/insert-lap">';
      $output .= '<input type="submit" value="Insert Lap"/>';
      $output .= '<input name="entrant-id" type="hidden" 
                  value="' . $entrant_id . '"/>';
      $output .= '</form>
    ';
    }
    $output .= '
      </td></tr></table>
 ';
      
  $output .= '<table id="race-manager-entrant">
 ';
      $output .= '
        <tr class="header-row">
        <th class="lap col-data-small aligncenter">Lap</th>
        <th class="lap col-data-small aligncenter">Km</th>
        <th class="lap col-data-small aligncenter">Mi</th>
        <th class="time col-time-med">Time</th>
        <th class="split col-time-small">Split</th>
        <th class="break col-data-small aligncenter">Brk</th>
        <th class="delete col-data-small aligncenter">Del</th>
     ';
     if ($lapeditpermssion) {
       $output .= '
        <th class="break col-data-med aligncenter">Break&nbsp;&nbsp;&nbsp;&nbsp;Delete</th>
       ';
     }
      $output .= '
        <th class="notes col-data-large">Notes</th>
        </tr>
        ';
    $ctr = 0;
    $start_time = $entrant->start_time;
    $sttime = strtotime($start_time);
    $count = count($lapdata) - 1;
    foreach ($lapdata as $laprecord) {
      $ctr++;
      $last = FALSE;
      if ($ctr == $count) {
         $last = TRUE;
      }
      $split = $laprecord['split'];
      $deleted = $laprecord['deleted'];

      $output .= '
        <tr';
      $class = Array();
      if ($ctr/2 == intval($ctr/2)) {
        $class[] = 'evenrow';
      }
      else {
        $class[] = 'oddrow';
      }
      if (!$deleted) {
        $warning = intval(race_manager_warning_threshold());
/*        if ($split < $avgsplit * (1 - $warning/100)) {
          $class[] = 'warning';
        }
 * 
 */
      }
      if ($class) {
        $class = implode(' ', $class);
        $class = ' class="' . $class .'"';
        $output .= $class;
      }
      $output .= '>
        ';
      $laps = $laprecord['lap'];
      $lap_id = $laprecord['LAP_ID'];
      $output .= '<td class="aligncenter">';
      if ($last) {
        $output .= '<a name="lastlap"></a>';
      }
      $output .= $laps . '</td>
        ';
      $value = intval($laps)*.4; // Km
      $output .= '<td class="aligncenter">';
      $output .= $value . '</td>
        ';
      $value = intval(intval($laps)*2.48548)/10; // Mi
      $output .= '<td class="aligncenter">';
      $output .= $value . '</td>
        ';
      $time = $laprecord['lap_time'];
      $time1 = strtotime($time);
      $ntime = $time1 - $sttime;
      $ftime = race_manager_format_timestamp($ntime);
      $output .= '<td class="time alignright">' . $ftime . '</td>
        ';
      $splittime = race_manager_format_timestamp($split);
      $output .= '<td class="split time alignright">' . $splittime . '</td>
        ';
      $output .= '<td class="aligncenter">';
      $break = $laprecord['break'];
      if ($break) {
        $output .= 'X';
      }
      $output .= '</td>
        ';
 
      $output .= '<td class="aligncenter">';
      $deleted = $laprecord['deleted'];
      if ($deleted) {
        $output .= 'X';
      }
      if ($lapeditpermssion) {
        $output .= '</td>
        <td>';
        $form_edit_lap['entrant-id']['#value'] = $entrant_id;
        $form_edit_lap['entrant-bib']['#value'] = $bib;
        $form_edit_lap['lap-id']['#value'] = $lap_id;
        $form_edit_lap['nav-last']['#value'] = '';
        $action = race_manager_entrant_base_url($entrant_id);
        $action .='&nav-start=' . $navigation['start'];
        $form_edit_lap['#action'] = $action;
        $form_edit_lap['nav-start']['#value'] = $navigation['start'];
        if ($break) {
          $value = 'REMOVE';
        }
        else {
          $value = 'BREAK';
        }
        $form_edit_lap['submit_break']['#value'] = $value;
        if ($deleted) {
          $value = 'UNDELETE';
        }
        else {
          $value = 'DELETE';
        }
        $form_edit_lap['submit_delete']['#value'] = $value;
        $content = drupal_render($form_edit_lap);
        $output .= $content;
      }
      $output .= '</td>
        <td class="notes">' . $laprecord['notes'] .'</td>
        ';
      $output .= '</tr>';
    }
    $output .= '</table>
      ';
    $output .= '<form id="race-manager-entrant-navigation" method = "POST" 
        enctype="multipart/form-data">';
      $output .= $navbar;
      $output .= '</form>
 ';
  return $output;
}

/* return a table listing the current entrants 
 * sorted by bib number
 */
 
function race_manager_entrants_table() {
  global $entrants;
  $base_url = race_manager_entrant_base_url();
  $options = Array('lapdata'=>FALSE);
  $options['sort'] = 'full_name ASC';
  $entrants = race_manager_entrants_current($options);
  $output = '<div id="race-manager-listing">
    ';
   $output .= '<table id="race-manager-entrants">
 ';
  $output .= '<tr class="header-row">
        <th class="aligncenter bib">Bib</th>
        <th class="full_name">Name</th>
        <th class="race_name">Race</th>
        <th class="aligncenter laps">Laps</th>
        </tr>
        ';
  $ctr = 0;
  foreach ($entrants as $entrant) {
    $ctr++;
    $entrant_id = $entrant->ENTRANT_ID;
    $lap = 0;
    if (property_exists($entrant, 'lap')) {
      $lap = intval($entrant->lap);
    }
//    $lap = race_manager_entrant_total($entrant);
//    $lap = 0;
    $race_name = $entrant->race_name;
    $bib = $entrant->bib;
    $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
    $output .= '<td class="aligncenter">' . $bib . '</td>';
    $output .= '<td><a href="'. race_manager_entrant_base_url($entrant_id);
    $output .= '&nav_last=1#lastlap">
      ';
    $output .= $entrant->full_name . '</a></td>
      ';
    $output .= '<td>' . $race_name . '</td>
      ';
    $output .= '<td class="aligncenter">' . $lap . '</td>
      </tr>';
  }
  $output .= '</table>
  ';
  $output .= '</div>
  ';
  return $output;
}

# Return race data for an entrant
function race_manager_entrant_info($options) {
  $entrants = race_manager_entrants_current($options);
  $entrant = NULL;
  foreach ($entrants as $entrant) {
    break;
  }
  return $entrant;
}

/* Return personal data for a participant
 * Note: this participant may not be registered in an event so there would
 * not be any entrant table data, just participant table data.
 */

function race_manager_entrant_personal_info($id) {
  $res = db_select('race_partcpt', 'p')
          ->fields('p')
          ->condition('ID', $id)
          ->execute();
  $partcpt = FALSE;
  foreach ($res as $partcpt) {
    break;
  }
  return $partcpt;
}


### Function: Return the total laps for an entrant
### DEPRECATED: now included in race_manager_entrants_current

### Function: Return the total number of lap records for this entrant
function race_manager_record_count($entrant_id = 0) {
  global $wpdb;
  $sql = 'SELECT COUNT(LAP_ID) AS lap_count FROM race_laps ';
  if ($entrant_id) {
    $sql .= ' WHERE entrant_id = ' . $entrant_id;
    $sql .= ' GROUP BY entrant_id';
  }
  else {
    $sql .= 'INNER JOIN race_entrant ON ';
    $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
    $sql .= 'INNER JOIN race_edition ON ';
    $sql .= 'race_entrant.race_id = race_edition.race_id AND ';
    $sql .= 'race_entrant.race_edition = race_edition.RACE_EDITION ';
    $sql .= ' WHERE current = 1';
  }
  $q = $wpdb->prepare($sql);
  $res = $wpdb->get_results($q, ARRAY_A);
  return intval($res[0]['lap_count']);
  return $sql;
}

/* Function: Return the total number of lap records for this entrant
 * 
 * Please note: this number is different from the total laps for the entrant
 * It is the number of records in the db for the purpose of listing all laps,
 * including deleted lap and milestone lap records.
 * 
 * TODO:  add timezone offset to this function
 */

function race_manager_entrant_record_count($entrant_id = 0) {
  $sql = 'SELECT COUNT(LAP_ID) AS lap_count FROM race_laps ';
  if ($entrant_id) {
    $sql .= ' WHERE entrant_id = ' . $entrant_id;
    $sql .= ' GROUP BY entrant_id';
  }
  else {
    $sql .= 'INNER JOIN race_entrant ON ';
    $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
    $sql .= 'INNER JOIN race_edition ON ';
    $sql .= 'race_entrant.edition_id = race_edition.EDITION_ID ';
    $sql .= ' WHERE current = 1';
  }
  $res = db_query($sql);
  $return = $res->fetchAssoc();
  return $return['lap_count'];
}

function race_manager_entrant_total($entrant_id = 0, $offset = FALSE ) {
  $options = Array('entrant_id' => $entrant_id);
  $entrants = race_manager_entrants_current($options);
  $c39 = chr(39);
  $race_data = $entrants[$entrant_id];
  $sql = 'SELECT lap FROM race_laps ';
  $sql .= ' WHERE ((deleted = 0) and (entrant_id = ' . $entrant_id . ')';
  if ($offset) {
    $t = intval(race_manager_get_timezone_offset());
    $t = $_SERVER['REQUEST_TIME'] - $t;
    $tlimit = $t - $race_data->reject_limit ;
    $time = date("Y-m-d H:i:s", $tlimit);
    $sql .= ' and (lap_time < ' . $c39 . $time . $c39 . ')';
  }
  $sql .= ')
      ORDER BY lap DESC LIMIT 1';
  $res = db_query($sql);
  $record = $res->fetchAssoc();
  $lap = 0;
  if (isset($record['lap'])) {
    $lap = intval($record['lap']);
  }

  return $lap;
}


### Function: Return an array of the most recent laps for an entrant
function race_manager_entrant_laps(
        $entrant_id = 0,
        $limit = Array('start'=>0,'size'=>30),
        $key = 'lap_id',
        $sort = "ASC"
        ) {
  $return = Array();
  if ($entrant_id){
    $laps = race_manager_log($entrant_id, $limit, Array('ORDER'=>$sort));
    $ctr = 0;
    foreach ($laps as $lap) {
      $lapnum = $ctr;
      switch ($key) {
        case 'lap':
          $lapnum = $lap['lap'];
          break;
        case 'time':
          return $laps;
          break;
        case 'lap_id':
          $lapnum = $lap['LAP_ID'];
          break;
        default :
          $lapnum = $lap['LAP_ID'];
          break;
      }
      if ($lapnum < 0) {
        $lapnum = $ctr;
      }
      $return[$lapnum] = $lap;
      $ctr = $ctr + 1;
    }
    asort($return);
  }
  return $return;
}

### Function: load the insert lap form
function race_manager_insert_lap($form_insert, $entrant_id = 0) {
  global $entrants;
  $entrant = $entrants[$entrant_id];
  $entrant_bib = $entrant->bib;
  $display_name = $entrant_bib . ' - ' . $entrant->full_name;
  $display_name .= ' (' . $entrant->short_name .')';
  $full_name = $entrant->full_name;
  // Provide a backlink to return if in error
  $content = '<div id ="back-link"><a href="' .
          race_manager_entrant_base_url($entrant_id);
  $content .= '"><-- Back</a></div>';
  $form_insert['insert-lap']['backlink'] = array(
    '#markup' => $content
   );

  // Make sure we return to the subject entrant
  $form_insert['insert-lap']['entrant-id']['#value'] = $entrant_id;
  $base_url = race_manager_entrant_base_url($entrant_id);
  // Make sure we return to the same location
  if (array_key_exists('nav', $_SESSION)) {
    $navigation = $_SESSION['nav'];
    $form_insert['insert-lap']['nav-start']['#value'] = $navigation['start'];
    $base_url .= '&nav-start=' . $navigation['start'];
  }
  $form_insert['#action'] = $base_url;
  $output = drupal_render($form_insert);
  return $output;
  
  $output .= '<div id="lapcounter-subtitle">
    ';
  $output .= '<a href="'. $base_url . '">';
  $output .= '<-- Back</a></div>';
  $output .= '<div class ="warninwg">';
  $output .= 'Warning! You must enter the split time here and not the time of day!
    </div>';
  $output .= '<form id="lapcounter-form-insert-lap" method = "POST" ';
  $output .= 'enctype="multipart/form-data" ';
  $output .= 'action="' . $base_url .'">';
  $output .= '<table id="lap-counter-insert-lap">';
  $output .= '<tr class="header-row">
      <td>
        Lap time
        (since start of race)
      </td>
      <td>';
  $output .= '<input name="entrant-id" type="hidden" value="';
  $output .= $entrant_id .'">';
  $output .= '<input name="lap-time" id="lap-time" type="text" SIZE = "10">
        (hh:mm:ss)
      ';
  $output .= '</td>
      <td>
        <input type="submit" name="insert-lap" value="Insert Lap">
      </td>';
  $output .= '</tr>
  </table>
  </form>
  </div>
</div>';
  $output .= '
    <script type="text/javascript"> 
    document.getElementById(';
  $output .= chr(39) . 'lap-time' . chr(39) . ').focus()
    </script>';
  return $output;
}

### Function: Assign entrants to counter stations
function race_manager_station_assign($args) {
  $station_id = $args['station-id'];
  $entrant_list = $args['entrants'];
  $entrants = race_manager_entrants_current();
  foreach ($entrant_list as $entrant_id) {
    $res = db_update('race_entrant')
          ->fields(Array('ctr_station' => $station_id))
          ->condition('ENTRANT_ID', $entrant_id, '=')
          ->execute();
  }
}

/* Function: find the entrant id from the bib
 * if $edition_id is provided then lookup bib in prior edition
 * otherwise search only active editions
 * (Version 7.x-1.003)
 */

function race_manager_get_entrant_id_from_bib($bib, $edition_id = 0) {
  $bib = intval($bib);
  if (!$bib) {
    return 0;
  }
  $sql = 'SELECT race_entrant.ENTRANT_ID, race_entrant.bib ';
  $sql .= 'FROM race_entrant INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((';
  if ($edition_id) {
    $sql .= 'race_edition.EDITION_ID = ' . $edition_id . ')';
  }
  else {
    $sql .= 'race_edition.current = 1)';
  }
  $sql .= ' AND (race_entrant.bib = ' . $bib .')';
  $sql .= ')';
  $res = db_query($sql);
  $record = FALSE;
  foreach ($res as $record) {
    break;
  }
  if ($record) {
    return $record->ENTRANT_ID;
  }
  return 0;
}

### Function: find the entrant's name from the bib
function race_manager_get_name_from_bib($bib) {
  if (!$bib) {
    return FALSE;
  }
  $sql = 'SELECT race_entrant.full_name, race_entrant.bib ';
  $sql .= 'FROM race_entrant INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((race_edition.current = 1)';
  $sql .= ' AND (race_entrant.bib = ' . $bib .')';
  $sql .= ')';
  $res = db_query($sql);
  $record = FALSE;
  foreach ($res as $record) {
    break;
  }
  if ($record) {
    return $record->full_name;
  }
  return '';
}

// Create a participant record from the provided data.
function race_manager_create_participant($data) {
  $pid = db_insert('race_partcpt')
          ->fields($data)
          ->execute();
  return $pid;
}


// Update the participant data as provided.
function race_manager_update_participant_info($input) {
  $fields = Array();
  $id = intval($input['id']);
  if ($id) {
    $fieldnames = Array('full_name', 'first_name', 'last_name', 'dob', 'gender');
    foreach ($fieldnames as $field) {
      if (isset($input[$field])) {
        $fields[$field] = $input[$field];
      }
    }
    $res = db_update('race_partcpt')
            ->fields($fields)
            ->condition('ID', $id)
            ->execute();
  }
}




/* Iterate though all currently registered participants and update
 * the age on race day value
 */
function race_manager_entrant_assign_categories(
        $entrant_id,
        $args = Array()
        ) {
  if (isset($args['category_regimes'])) {
    $regimes = $args['category_regimes'];
  }
  else {
    $opt = Array('entrant_id' => $entrant_id);
    $regimes = race_manager_get_category_regimes($opt);
  }
  $options = Array('entrant_id' => $entrant_id);
  $options['current'] = FALSE;
  $entrant = race_manager_entrant_info($options);
  foreach ($regimes as $regime_id) {
    $category = db_select('race_category', 'c')
            ->fields('c')
            ->range(0, 1)
            ->condition('upper_limit', $entrant->current_age, '>')
            ->condition('regime_id', $regime_id)
            ->orderBy('upper_limit')
            ->execute();
    $category = $category->fetchAssoc();
    $lookup = db_select('race_category_entrant','c')
            ->fields('c')
            ->condition('entrant_id', $entrant_id)
            ->condition('regime_id', $regime_id)
            ->execute();
    $record = $lookup->fetchAssoc();
    $fields = Array('category_id'=>$category['ID']);
    if ($record) {
      $res = db_update('race_category_entrant')
              ->fields($fields)
              ->condition('entrant_id', $entrant_id)
              ->condition('regime_id', $regime_id)
              ->execute();
    }
    else {
      $fields['entrant_id'] = $entrant_id;
      $fields['regime_id'] = $regime_id;
      $res = db_insert('race_category_entrant')
              ->fields($fields)
              ->execute();
    }
  }
}

// Update the participant data as provided.
function race_manager_update_entrant_info($data) {
  $fields = Array();
  $id = intval($data['id']);
  if ($id) {
    $fields = Array();
    foreach ($data as $key => $value) {
      if (db_field_exists('race_entrant', $key)) {
        $fields[$key] = $value;
      }
    }
    $res = db_update('race_entrant')
            ->fields($fields)
            ->condition('ENTRANT_ID', $id)
            ->execute();
  }
}

function race_manager_entrant_recalculate_ages(
        $options = Array()
        ) {
  $opt = Array('age'=>1);
  $entrants = race_manager_entrants_current($opt);
  $categories = FALSE;
  if (isset($options['categories'])) {
    $categories = $options['categories'];
  }
  foreach ($entrants as $entrant) {
    $entrant_id = $entrant->ENTRANT_ID;
    $data = Array('id' => $entrant_id);
    $data['current_age'] = $entrant->age;
    race_manager_update_entrant_info($data);
    $opt = Array();
    if ($categories) {
      if (is_array($categories)) {
        $opt['category_regimes'] = $categories;
      }
      race_manager_entrant_assign_categories(
              $entrant_id,
              $opt);
    }
  }
}


### Function: race_manager_entrants_update
function race_manager_entrants_update($entrydata=Array()) {
  // Update the entrants list for the chosen event / edition
  // updated 17 June 2012 mcw
  // migrated to drupal 7 Sep 2013 mcw
  // Taken from races_11 Drupal
  if (count($entrydata) == 0) {
    return;
  }
  
  $edition_id = $entrydata['edition-id'];
  global $entrants;
  $c39 = chr(39); // single quote character
  if (!array_key_exists('data', $entrydata)) {
    return;
  }
  $data = $entrydata['data'];
  $datalength = strlen($data);
  $tmp = '';
  $ctr = 0;
  // assemble a list
  $namelist = Array();
  $line_items = explode(chr(13), $data);
  foreach ($line_items as $line_item) {
    $ctr ++;
    if ($ctr == 1) {
      // parse the column labels
      $colnames = explode(chr(9), $line_item);
      continue;
    }
    // Capture data line by line and load into array
    $entrant_data = race_manager_parse_entrant_data($line_item, $colnames);
    if (!$entrant_data) {
      continue;
    }
    /* The entrants need to be assigned a unique bib number.
     *    At some point this module might provide a bib assignment 
     *    utility but for now it must be done fully in advance.
     */
    if (!array_key_exists('bib', $entrant_data)) {
      continue;
    }
    $bib = intval($entrant_data['bib']);
    if ($bib <=0) {
      // BIB <= 0 NOT ALLOWED
      continue;
    }
    $namecode = $entrant_data['name_code'];
    if (array_key_exists('full_name', $entrant_data)) {
      $fullname = $entrant_data['full_name'];
      $f = explode(' ', $fullname);
      $firstname = array_shift($fullname);
      $lastname = implode(' ', $fullname);
    }
    else {
      $fullname = Array();
      if (array_key_exists('first_name', $entrant_data)) {
        $firstname = $entrant_data['first_name'];
        $fullname[] = $firstname;
      }
      if (array_key_exists('last_name', $entrant_data)) {
        $lastname = $entrant_data['last_name'];
        $fullname[] = $lastname;
      }
      $fullname = trim(implode(' ', $fullname));
    }
    $age = 0;
    $dob = '';
    if (array_key_exists('dob', $entrant_data)) {
      $dob = $entrant_data['dob'];
      $start_time = race_manager_current_event_start_time($edition_id);
      $age = race_manager_calculate_age($dob, $start_time);
    }
    $gender = '';
    if (array_key_exists('gender', $entrant_data)) {
      $gender = $entrant_data[ 'gender'];
    }
    $alt_start = 0;
    if (array_key_exists('alt_start', $entrant_data)) {
      $alt_start = intval($entrant_data['alt_start']);
    }
    $add1 = '';
    if (array_key_exists('add1', $entrant_data)) {
      $add1 = $entrant_data['add1'];
    }
    $add2 = '';
    if (array_key_exists('add2', $entrant_data)) {
      $add2 = $entrant_data['add2'];
    }
    $city = '';
    if (array_key_exists('city', $entrant_data)) {
      $city = $entrant_data['city'];
    }
    $state = '';
    if (array_key_exists('state', $entrant_data)) {
      $state = $entrant_data['state'];
    }
    $country = '';
    if (array_key_exists('country', $entrant_data)) {
      $country = $entrant_data['country'];
    }
    $postal = '';
    if (array_key_exists('postalcode', $entrant_data)) {
      $postal = $entrant_data['postalcode'];
    }
    $club = '';
    if (array_key_exists('club', $entrant_data)) {
      $club = $entrant_data['club'];
    }
    $bio = '';
    if (array_key_exists('current_bio', $entrant_data)) {
      $bio = $entrant_data['current_bio'];
    }
    $pedition_id = 0;
    if (array_key_exists('edition_id', $entrant_data)) {
      $pedition_id = $entrant_data['edition_id'];
    }
    if (!$pedition_id) {
      $pedition_id = $edition_id;
    }
    $pid = 0;
    if (array_key_exists('partcpt_id', $entrant_data)) {
      $pid = intval($entrant_data['partcpt_id']);
    }
    $insert_data = Array();
    $insert_data['full_name'] = $fullname;
    $insert_data['name_code'] = $namecode;
    $insert_data['first_name'] = $firstname;
    $insert_data['last_name'] = $lastname;
    $insert_data['gender'] = $gender;
    $insert_data['dob'] = $dob;
    $update = TRUE;
    if (!$pid) {
      
      /* Try to find the partcpt_id from the name_code
       * The name_code may have changed (!!!).
       * Check both the participant table and the entrant table
       */
      
      $sql = 'SELECT p.*, e.name_code, e.partcpt_id ';
      $sql .= 'FROM race_partcpt as p ';
      $sql .= ' LEFT JOIN race_entrant as e ';
      $sql .= ' ON p.ID = e.partcpt_id ';
      $sql .= ' WHERE ((p.name_code = ' . $c39 . strval($namecode) . $c39 . ') ';
      $sql .= ' OR (e.name_code = ' . $c39 . strval($namecode) . $c39 . ')) ';
      $res = db_query($sql);
      $record = $res->fetchAssoc();
      if ($record) {
        $pid = intval($record['ID']);
      }
      else {
        //look at the entrant table by bib number
        $options = Array('bib' => $bib);
        $entrant = race_manager_entrant_info($options);
        if ($entrant) {
          $pid = $entrant->partcpt_id;
        }
        else {
          // If it is not found then create a new participant
          $pid = race_manager_create_participant($insert_data);
          $update = FALSE;
        }
      }
    }
    if ($update) {
      $insert_data['id'] = $pid;
      race_manager_update_participant_info($insert_data);
    }
    $res = db_select('race_entrant', 'e')
            ->fields('e')
            ->condition('partcpt_id', $pid, '=')
            ->condition('edition_id', $pedition_id, '=')
            ->execute();
    $record = $res->fetchAssoc();
    // Prepare the line item data for insert or update in the entrants table  
    $insert_data = Array();
    $insert_data['partcpt_id'] = $pid;
    $insert_data['name_code'] = $namecode;
    $insert_data['alt_start'] = $alt_start;
    $insert_data['edition_id'] = $pedition_id;
    $insert_data['bib'] = $bib;
    $insert_data['full_name'] = $fullname;
    $insert_data['current_bio'] = $bio;
    $insert_data['current_age'] = $age;
    $insert_data['add1'] = $add1;
    $insert_data['add2'] = $add2;
    $insert_data['city'] = $city;
    $insert_data['state'] = $state;
    $insert_data['country'] = $country;
    $insert_data['postalcode'] = $postal;
    $insert_data['club'] = $club;
    // Here is the test
    if (!$record) {
      // This participant is not registered in the db for this edition.
        $entrant_id = db_insert('race_entrant')
                ->fields($insert_data)
                ->execute();
      }
      else {
        // Do an update query on the existing record
        $entrant_id = $record['ENTRANT_ID'];
        $res = db_update('race_entrant')
                ->fields($insert_data)
                ->condition('ENTRANT_ID', $entrant_id, '=')
                ->execute();
      }
      race_manager_entrant_assign_categories($entrant_id);
      $namelist[$pid] = $namecode;
    }
}


### Function: race_manager_parse_entrant_data
// Extract data form data string and return as array
/* 2012/3/31
 * Modular function to handle future input formats
 */

function race_manager_parse_entrant_data($data, $colnames) {
  $return = Array();

  // tab-delimited data is required
  $dataset = explode(chr(9), $data);
  $t = trim(implode('', $dataset));
  if (strlen($t) > 0) {
    // Capture data line by line and load into array
    /* the partcpt_id is optional - really useful if provided
     *   but may not be available in the case of new registrants etc
     */
    $partcpt_id = 0;
    $startpos = 0;
    if ($colnames[0] == 'partcpt_id') {
      $partcpt_id = trim($dataset[0]);
      $startpos = 1;
    }
    $return['partcpt_id'] = $partcpt_id;
    $name_code = '';
    $names = Array('first_name'=>1,'last_name'=>1);
    for ($ctr = $startpos; $ctr < count($colnames); $ctr++) {
      $colname = $colnames[$ctr];
      $value = trim($dataset[$ctr]);
      $return[$colname] = $value;
      if (array_key_exists($colname, $names)) {
        // generate name code
        $n = explode(' ', $value);
        foreach ($n as $name) {
          $name_code .= ucfirst($name);
        }
      }
    }
    $return['name_code'] = $name_code;
  }
  return $return;
}


### Function: race_manager_entrants_insert_first_lap
// Bulk Insert a first lap for all participants 

function race_manager_entrants_insert_first_lap($args=Array()) {
  global $entrants;
  $edition_id = $args['edition-id'];
  $options = Array('edition_id' => $edition_id);
  $entrants = race_manager_entrants_current($options);
  $average = intval($args['average']);
  $state = Array('input'=>Array());
  foreach ($entrants as $entrant) {
    $entrant_id = $entrant->ENTRANT_ID;
    $laps = race_manager_entrant_total($entrant_id);
    if (intval($laps) == 0) {
      $state['input']['entrant-id'] = $entrant_id;
      $atime = $average + mt_rand(-10, 10);
      $atime = strftime("%M:%S", $average + mt_rand(-10, 10));
      $state['input']['lap-time'] = $atime;
      race_manager_lap_insert(Array(), $state);
    }
  }
}


