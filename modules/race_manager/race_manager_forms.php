<?php


function race_manager_admin_entrant_form($form, &$form_state) {
  $entrants = race_manager_entrant_select_list();
  $editions = race_manager_edition_select_list();
  $form['recalculate-ages'] = array(
      '#type' => 'fieldset',
      '#title' => t('Calculate Ages'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Recalculate Age on Race Day for all participants.')
  );
  $form['recalculate-ages']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-entrants-recalculate-ages-submit',
      '#value' => t('Recalculate Ages'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $form['first-lap'] = array(
      '#type' => 'fieldset',
      '#title' => t('Insert First Lap'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Insert a first lap for all participants.')
  );
  $form['first-lap']['first-lap-edition-id'] = array(
      '#type' => 'select',
      '#title' => t('Edition ID'),
      '#options' => $editions,
      '#description' => t('The id of the event edition being inserted.'),
  );
  $form['first-lap']['average'] = array(
      '#type' => 'textfield',
      '#title' => t('Average First Lap (seconds)'),
      '#value' => 150,
  );
  $form['first-lap']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-entrants-insert-first-lap-submit',
      '#value' => t('Insert'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $form['stations'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage counting stations'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Add, Delete and Edit counting station settings.')
  );
  $stations = Array();
  $res = db_select('race_counter_stations', 's')
          ->fields('s', Array('ID'))
          ->execute();
  foreach ($res as $sid) {
    $id = $sid->ID;
    $stations[$id] = $id;
  }
  $form['stations']['station-id'] = array(
      '#prefix' => '<table><tr><td>',
      '#type' => 'radios',
      '#title' => t('Assign counting stations'),
      '#description' => t('Assign counting stations.'),
      '#options' => $stations,
  );
  $form['stations']['entrants'] = array(
      '#title' => t('Select participants to be assigned.'),
      '#prefix' => '</td><td>',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#description' => t('select a participant.'),
      '#options' => $entrants,
      '#size' => 15,
  );
  $form['stations']['close'] = array(
      '#suffix' => '</td></tr></table>',
  );
  $form['stations']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-entrants-station-select-submit',
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );

  return $form;
}

function race_manager_registration_form($form, &$form_state) {
  $options = Array();
  $type1 = 'textfield';
  $type2 = 'submit';
  $input = $form_state['input'];
  $submitted = FALSE;
  if (array_key_exists('triggering_element', $form_state)) {
    $submitted = TRUE;
  }
  if (isset($input['form_id'])) {
    if ($input['form_id'] == 'race_manager_registration_goto_form') {
      $form_state['rebuild'] = FALSE;
//      $form_state['input']['form_id'] = 'race_manager_registration_form';
      $submitted = TRUE;
    }
  }
  if (isset($_REQUEST['select_bib'])) {
    $submitted = TRUE;
    $input['select_bib'] = $_REQUEST['select_bib'];
  }
  $partcpt_id = '';
  $edition_id = '';
  if ($submitted) {
    $type1 = 'hidden';
    $type2 = 'hidden';
    $data = FALSE;
    if (isset($input['select_name'])) {
      $data = trim($input['select_name']);
    }
    $bib = '0';
    if (isset($input['select_bib'])) {
      $bib = trim($input['select_bib']);
    }
    if (strlen($data) == 0 ) {
      if ($bib) {
        $entrant_id = race_manager_get_entrant_id_from_bib($bib);
        if ($entrant_id) {
          $options = Array();
          $options['entrant_id'] = $entrant_id;
          $entrant = race_manager_entrant_info($options);
          if ($entrant) {
            $partcpt_id = $entrant->partcpt_id;
          }
          else {
            $submitted = FALSE;
            drupal_set_message('Invalid Entrant');
          }
        }
        else {
          $submitted = FALSE;
          drupal_set_message('Invalid Bib Number');
        }
      }
    }
    else {
      $partcpt_id = '';
      if (isset($input['current-id'])) {
        $partcpt_id = trim($input['current-id']);
      }
      if (!$partcpt_id) {
        // look up the id from the Selected Name field
        $selection = $input['select_name'];
        $selection = explode('(', $selection);
        if (count($selection) > 1) {
          $selection = $selection[1];
          $selection = explode(')', $selection);
          $partcpt_id = intval($selection[0]);
        }
        else {
          $submitted = FALSE;
          drupal_set_message('Invalid Entry');
        }
      }
    }
    $partcpt = race_manager_entrant_personal_info($partcpt_id);
    if ($partcpt) {
      $full_name = $partcpt->full_name;
      $reg = race_manager_registration_info($partcpt_id);
      if ($reg) {
        $edition_id = $reg->edition_id;
        $edition_desc = $reg->description;
      }
    }
    else {
      $submitted = FALSE;
      drupal_set_message('Invalid Bib Number');
    }
  }
  $form['current-id'] = array(
      '#type' => 'hidden',
      '#title' => t('ID'),
      '#prefix' => '<div id="partcpt-id">',
      '#suffix' => '</div>',
      '#value' => $partcpt_id,
      '#attributes' => array(
          'id' => 'current-id'),
      );
  if (!$submitted) {
    $type1 = 'textfield';
    $type2 = 'submit';
    $form['select_name'] = array(
      '#prefix' => '<div id="select-name" class="form-item" onclick="SetNameClick();"
         >',
      '#suffix' => '</div>',
      '#type' => $type1,
      '#title' => t('Select Name'),
      '#autocomplete_path' => 'race-admin/registration/autocomplete',
      '#attributes' => array(
         'onchange' => 'SetNameChange();',
         'onblur' => 'SetNameBlur();'),
  );
    $form['or'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="notice floatleft">OR</div>',
  );
    $form['select_bib'] = array(
      '#type' => 'textfield',
      '#title' => t('Select Bib'),
  );
    $form['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );    
    $form['submit-container']['choose'] = array(
      '#type' => $type2,
      '#id' => 'race-manager-registration-choose-submit',
      '#title' => t('Choose'),
      '#value' => t('Lookup Entrant'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
    $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("edit-select-name");';
    $content .= 'ob.focus();
    </script>';
    $form['script'] = array(
      '#markup' => $content,
  );
  }
  else {
    $options = Array('current' => TRUE);
    $options['sort'] = 'EDITION_ID ASC';
    $edition_list = race_manager_edition_select_list($options);
    $edition_count = count($edition_list);
    $form['selected_name'] = array(
        '#type' => 'markup',
        '#markup' => $full_name,
        '#prefix' => '<div class="clearboth" id="full_name">',
        '#suffix' => '</div>',
        );
    if ($reg) {
      $form['isregistered'] = array(
          '#type' => 'markup',
          '#markup' => 'is registered for: <strong>'.$edition_desc.'</strong>.',
          '#prefix' => '<div class="notice">',
          '#suffix' => '</div>',
          );
      }
    else {
      $form['notregistered'] = array(
          '#type' => 'markup',
          '#markup' => 'is not registered for any event.',
          '#prefix' => '<div class="notice">',
          '#suffix' => '</div>',
              );
    }
    $form['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
    $form['submit-container']['cancel'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-registration-cancel-submit',
      '#title' => t('Cancel and go back to previous page.'),
      '#value' => t('<- Cancel'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
    );
    $form['submit-container']['next'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-registration-save-submit',
      '#title' => t('Register this entrant and Continue'),
      '#value' => t('Save ->'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
    );
    $suggestion = 99;
    if ($reg) {
      $bib = $reg->bib;
    }
    else {
      $bib = race_manager_get_next_bib_number();
    }
    $collapsed = FALSE;
    $form['current'] = array(
      '#type' => 'fieldset',
      '#title' => t('Current Event'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
        );
    $t = 'Bib Number';
    if (!$reg) {
      $t = 'Suggested ' . $t;
    }
    $form['current']['bib'] = array(
      '#type' => 'textfield',
      '#title' => t($t),
      '#value' => $bib,
        );
    $form['current']['select_edition_id'] = array(
        '#prefix' => '<div id="select-edition-id">',
        '#suffix' => '</div>',
        '#type' => 'radios',
        '#default_value' => $edition_id,
        '#title' => t('Select Event'),
        '#options' => $edition_list,
        );
    $show_offset = FALSE;
    $value = FALSE;
    if (!empty($reg->offset_start)) {
      if (intval($reg->offset_start)) {
        // Don't bother if the offset is not active
        $show_offset = TRUE;
        if (!empty($reg->alt_start)) {
          if (intval($reg->alt_start)) {
            $value = TRUE;
          }
        }
      }
    }
    $type = 'hidden';
    $type = 'checkbox';
    if ($show_offset) {
      $type = 'checkbox';
    }
    $form['current']['alt_start'] = array(
        '#type' => $type,
        '#default_value' => $value,
        '#title' => t('Use second race start'),
 //      '#id' => 'offset_start',
        );
    $collapsed = FALSE;
//    $collapsed = TRUE;
    $form['personal'] = array(
      '#type' => 'fieldset',
      '#title' => t('Personal Details'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
        );
    $collapsed = FALSE;
    $form['personal']['identification'] = array(
      '#type' => 'fieldset',
      '#title' => t('Name, DOB, Age'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
      );
    $fname = trim($partcpt->first_name);
    if (!$fname) {
      $fname = explode(' ', $partcpt->full_name);
      $fname = $fname[0];
    }
    $form['personal']['identification']['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#value' => $fname,
      );
    $lname = trim($partcpt->last_name);
    if (!$lname) {
      $lname = $partcpt->full_name;
      $lname = explode(' ', $lname);
      if (count($lname) > 1) {
        $lname = $lname[1];
      }
      else {
        $lname = '';
      }
    }
    $form['personal']['identification']['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#value' => $lname,
      );
    $gender = '';
    if ($partcpt) { // consolidate these tests into one assignment set
      $gender = $partcpt->gender;
    }
    $form['personal']['identification']['gender'] = array(
      '#prefix' => '<div id="gender">',
      '#suffix' => '</div>',
      '#type' => 'radios',
      '#value' => $gender,
      '#default_value' => $gender,
      '#title' => t('Gender'),
      '#options' => Array('M'=>'M','F'=>'F'),
      );
    $name_code = trim($partcpt->name_code);
    if (!$name_code) {
      $name_code = race_manager_calculate_name_code($partcpt->full_name);
    }
    if ($reg) {
      $finish_time = $reg->finish_time;
    }
    else {
      $finish_time = date('Y-m-d H:i:s');
    }
    $dob = race_manager_get_date_default_value($partcpt->dob);
    $form['personal']['identification']['dob'] = array(
      '#type' => 'date',
      '#title' => t('Date of Birth'),
      '#default_value' => $dob,
        );
    $age = race_manager_calculate_age(
            $partcpt->dob,
            $finish_time
            );
    $form['personal']['identification']['age'] = array(
      '#type' => 'textfield',
      '#title' => t('Age on Race Day'),
      '#value' => $age,
      '#attributes' => array(
          'disabled' => TRUE
          ),
        );
    $collapsed = TRUE;
    $form['personal']['address'] = array(
      '#type' => 'fieldset',
      '#title' => t('Address'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
      );
    $form['personal']['address']['add1'] = array(
      '#type' => 'textfield',
      '#title' => t('Address 1'),
        );
    $form['personal']['address']['add2'] = array(
      '#type' => 'textfield',
      '#title' => t('Address 2'),
        );
    $form['personal']['address']['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
        );
    $form['personal']['address']['state'] = array(
      '#type' => 'textfield',
      '#title' => t('State'),
        );
    $form['personal']['address']['country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
        );
    $form['personal']['address']['postalcode'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal Code'),
        );
    $collapsed = TRUE;
    $form['personal']['other'] = array(
      '#type' => 'fieldset',
      '#title' => t('Club Affiliation, Bio, &c'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
      );
    $form['personal']['other']['club'] = array(
      '#type' => 'textfield',
      '#title' => t('Club Affiliation'),
        );
    $form['personal']['other']['bio'] = array(
      '#type' => 'textarea',
      '#title' => t('Current Bio'),
        );
  }
  return $form;
}

function race_manager_registration_short_form($form, &$form_state) {
  $type1 = 'textfield';
  $type2 = 'submit';
  $submitted = FALSE;
  if (array_key_exists('triggering_element', $form_state)) {
    $submitted = TRUE;
  }
  $partcpt_id = '';
  $edition_id = '';
  if ($submitted) {
    $type1 = 'hidden';
    $type2 = 'hidden';
    $data = trim($form_state['input']['select_name']);
    if (strlen($data) > 0 ) {
      $partcpt_id = trim($form_state['input']['current-id']);
      if (!$partcpt_id) {
        // look up the id from the Selected Name field
        $selection = trim($form_state['input']['select_name']);
        $selection = explode('(', $selection);
        $len = count($selection);
        switch ($len) {
          case 0 :
            $form_state['rebuild'] = FALSE;
            $submitted = FALSE;
            break;
          case 1 :
            // check for bib number
            $bib = intval($selection[0]);
            if ($bib > 0) {
              $entrant_id = race_manager_get_entrant_id_from_bib($bib);
              if ($entrant_id) {
                $options = Array();
                $options['entrant_id'] = $entrant_id;
                $entrant = race_manager_entrant_info($options);
                if ($entrant) {
                  $partcpt_id = $entrant->partcpt_id;
                }
              }
            }
            break;
          default :
            if (count($selection) > 1) {
              $selection = $selection[1];
              $selection = explode(')', $selection);
              $partcpt_id = intval($selection[0]);
            }
        }
      }
      $partcpt = race_manager_entrant_personal_info($partcpt_id);
      $full_name = $partcpt->full_name;
      $reg = race_manager_registration_info($partcpt_id);
      if ($reg) {
        $edition_id = $reg->edition_id;
        $edition_desc = $reg->description;
      }
    }
  }
  $form['current-id'] = array(
      '#type' => 'hidden',
      '#title' => t('ID'),
      '#prefix' => '<div id="partcpt-id">',
      '#suffix' => '</div>',
      '#value' => $partcpt_id,
      '#attributes' => array(
          'id' => 'current-id'),
      );
  if (!$submitted) {
  $form['select_name'] = array(
      '#prefix' => '<div id="select-name" onclick="SetNameClick();"
         >',
      '#suffix' => '</div>',
      '#type' => $type1,
      '#title' => t('Select Name'),
      '#autocomplete_path' => 'race-admin/registration/autocomplete',
      '#attributes' => array(
         'onchange' => 'SetNameChange();',
         'onblur' => 'SetNameBlur();'),
  );
  $form['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
  $form['submit-container']['choose'] = array(
      '#type' => $type2,
      '#id' => 'race-manager-registration-choose-submit',
      '#title' => t('Choose'),
      '#value' => t('Lookup Entrant'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
    );
  }
  else {
    $options = Array();
    $options['current'] = TRUE;
    $options['sort'] = 'EDITION_ID ASC';
    $edition_list = race_manager_edition_select_list($options);
    $edition_count = count($edition_list);
    $fname = trim($partcpt->first_name);
    if (!$fname) {
      $fname = explode(' ', $partcpt->full_name);
      $fname = $fname[0];
    }
    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#value' => $fname,
      );
    $gender = '';
    if ($partcpt) { // consolidate these tests into one assignment set
      $gender = $partcpt->gender;
    }
    $form['gender'] = array(
      '#type' => 'radios',
      '#value' => $gender,
      '#default_value' => $gender,
      '#title' => t('Gender'),
      '#options' => Array('M'=>'M','F'=>'F'),
      );
    $t = 'Bib Number';
    if (!$reg) {
      $t = 'Suggested ' . $t;
    }
    $suggestion = 99;
    if ($reg) {
      $bib = $reg->bib;
    }
    else {
      $bib = race_manager_get_next_bib_number();
    }
    $form['bib'] = array(
      '#type' => 'textfield',
      '#title' => t($t),
      '#value' => $bib,
        );
    $form['selected_name'] = array(
        '#type' => 'markup',
        '#markup' => $full_name,
        '#prefix' => '<div class="clearboth" id="full_name">',
        '#suffix' => '</div>',
        );
    if ($reg) {
      $form['isregistered'] = array(
          '#type' => 'markup',
          '#markup' => 'is registered for: <strong>'.$edition_desc.'</strong>.',
          '#prefix' => '<div class="notice">',
          '#suffix' => '</div>',
          );
      }
    else {
      $form['notregistered'] = array(
          '#type' => 'markup',
          '#markup' => 'is not registered for any event.',
          '#prefix' => '<div class="notice">',
          '#suffix' => '</div>',
              );
    }
    $form['select_edition_id'] = array(
        '#type' => 'radios',
          '#default_value' => $edition_id,
        '#title' => t('Event'),
        '#options' => $edition_list,
        );
    $show_offset = FALSE;
    $value = FALSE;
    if (!empty($reg->offset_start)) {
      if (intval($reg->offset_start)) {
        // Don't bother if the offset is not active
        $show_offset = TRUE;
        if (!empty($reg->alt_start)) {
          if (intval($reg->alt_start)) {
            $value = TRUE;
          }
        }
      }
    }
    $type = 'hidden';
    $type = 'checkbox';
    if ($show_offset) {
      $type = 'checkbox';
    }
    $form['current-race']['registration']['alt_start'] = array(
        '#type' => $type,
        '#default_value' => $value,
        '#title' => t('Use second race start'),
 //      '#id' => 'offset_start',
        );
    $form['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
    $form['submit-container']['cancel'] = array(
      '#prefix' => '<div class="clearboth form-submit-container">',
      '#type' => 'submit',
      '#id' => 'race-manager-registration-cancel-submit',
      '#title' => t('Cancel and go back to previous page.'),
      '#value' => t('<- Cancel and Go Back'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
    );
    $form['submit-container']['save'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-registration-save-submit',
      '#title' => t('Save the data for this entrant'),
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
      '#suffix' => '</div>',
    );
    $form['detail'] = array(
      '#type' => 'hidden', // Go to the detail registration for more data entry
      '#id' => 'race-manager-registration-detail-submit',
      '#title' => t('Register this entrant and Continue'),
      '#value' => t('Save and Next ->'),
      '#executes_submit_callback' => TRUE,
      '#suffix' => '</div>',
      '#submit' => array('race_manager_admin_submit'),
    );
  }
  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("edit-select-name");';
  $content .= 'ob.focus();
    
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );

  return $form;
}

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 */
function  race_manager_admin_form($form, &$form_state) {
  $base = RACE_MANAGER_BASE_PATH;
//  $base = $base[1];
  $form['race_manager'] = array(
      '#type' => 'fieldset',
      '#title' => t('Race Manager Management'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  $form['race_manager']['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('System Options'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/race_manager/options';
  $form['race_manager']['options']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Options</a>'
  );
  $form['race_manager']['entrants'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Entrants'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/race_manager/entrants';
  $form['race_manager']['entrants']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Entrants</a>'
  );
  $form['race_manager']['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Events'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/race_manager/events';
  $form['race_manager']['events']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Events</a>'
  );
  return $form;
}


/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 */
function race_manager_admin_options_form($form, &$form_state) {
  $base = RACE_MANAGER_BASE_PATH;
  $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Race Manager Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Manage the module\'s settings.')
  );
  $form['options']['list'] = array(
      '#type' => 'markup',
      '#prefix' => '<table id="race-manager-editions">
      <tr class="header">
      <th><label for="option_id">ID</label></th>
      <th><label for="option_name">Name</label></th>
      <th><label for="option_value">Value</label></th>
      </tr>',
      '#suffix' => '</table>',
      '#markup' => race_manager_admin_options_table(71),
  );
  $form['options']['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
  $form['options']['submit-container']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-options-save-submit',
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $form['system'] = array(
      '#type' => 'fieldset',
      '#title' => t('System Functions'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['system']['process-laps'] = array(
      '#type' => 'fieldset',
      '#title' => t('Process Laps'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Load the lap processing page. This is required when
        counting laps.'),
  );
  $url = $base . 'process-laps';
  $form['system']['process-laps']['process-laps-link'] = array(
      '#markup' => '<a href="' . $url . '">
      Process Laps</a>',
  );
  $form['system']['menu-setup'] = array(
      '#type' => 'fieldset',
      '#title' => t('Menu Setup'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Load the System Menus. Required when installing or 
        updating the software.'),
  );
  $form['system']['menu-setup']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-options-menu-setup-submit',
      '#value' => t('Setup Menus'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}

function race_manager_registration_clear_form($form, &$form_state) {
  $stuff = $form_state['%value'];
  $form['clear'] = array(
      '#type' => 'fieldset',
      '#title' => t('Are You Sure?'),
      '#id' => 'race-manager-registration-clear-submit',
      '#collapsible' => FALSE,
      '#description' => t('This action will clear the current edition of 
        all entrant records.'),

  );
  $form['cancel'] = array(
      '#type' => 'submit',
      '#title' => t('Cancel'),
      '#id' => 'race-manager-registration-cancel-submit',
      '#value' => '<-- Cancel',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Are You Sure?'),
      '#id' => 'race-manager-registration-clear-submit',
      '#value' => 'Clear Edition',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}

function race_manager_registration_manage_form($form, &$form_state) {
  $collapsed = FALSE;
  $form['import'] = array(
      '#type' => 'fieldset',
      '#title' => t('Import Registration List'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
  );
  $options = Array('sort'=>'EDITION_ID DESC');
  $editions = race_manager_edition_select_list($options);
  $form['import']['edition-id'] = array(
      '#type' => 'select',
      '#title' => t('Edition ID'),
      '#options' => $editions,
      '#description' => t('The id of the event edition being imported.'),
  );
  $form['import']['data'] = array(
      '#type' => 'textarea',
      '#title' => t('Enter the data here.'),
      '#description' => t('The raw entrant data.'),
  );
  $form['import']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-registration-import-submit',
      '#value' => t('Import'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 * Entrants Management
 * 
 */
// The edit lap form 
// Modify or delete laps here
function race_manager_edit_lap_form($form, &$form_state) {
  $form['entrant-bib'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-bib',
      '#name' => 'entrant-bib',
      '#value' => 'placeholder',
  );
  $form['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['lap-id'] = array(
      '#type' => 'hidden',
      '#id' => 'lap-id',
      '#name' => 'lap-id',
      '#value' => 'placeholder',
  );
  $form['nav-last'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-last',
      '#name' => 'nav-last',
      '#value' => 'placeholder',
  );
  $form['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $form['submit_break'] = array(
      '#type' => 'submit',
      '#id' => 'edit-lap-break-submit',
      '#value' => t('BREAK'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_lap_edit'),
  );
  $form['submit_delete'] = array(
      '#type' => 'submit',
      '#id' => 'edit-lap-delete-submit',
      '#value' => t('DELETE'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_lap_edit'),
  );
  return $form;
}

### The Insert lap form 
function race_manager_insert_lap_form($form, &$form_state) {
  $c39 = chr(39);
  $form['insert-lap'] = array(
      '#title' => t('Insert Lap'),
      '#type' => 'fieldset',
      '#id' => 'insert-lap-wrapper',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t(
              'Warning! You must enter the elapsed time here and not the time of day!')
  );
  // Put this here to place the item at the top
  $form['insert-lap']['backlink'] = array(
      '#markup' => 'placeholder'
  );
  $form['insert-lap']['split'] = array(
      '#title' => t('Enter lap time here'),
      '#type' => 'textfield',
      '#id' => 'lap-time',
      '#name' => 'lap-time',
      '#description' => t('(HHH:MM:SS) since start of race')
  );
  $form['insert-lap']['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['insert-lap']['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $content = '
    <script type="text/javascript"> 
    document.getElementById(';
  $content .= chr(39) . 'lap-time' . chr(39) . ').focus()
    </script>';
  $form['insert-lap']['script'] = array(
      '#markup' => $content
  );
  $form['insert-lap']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'insert-lap-submit',
      '#value' => t('Insert Lap'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_lap_insert'),
  );
  return $form;
}

function race_manager_navigation_form($form, &$form_state) {
  $form['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $form['nav-size'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-size',
      '#name' => 'nav-size',
      '#value' => 'placeholder',
  );
  $form['nav-total'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-total',
      '#name' => 'nav-total',
      '#value' => 'placeholder',
  );
  $form['submit-first'] = array(
      '#type' => 'submit',
      '#id' => 'submit-first',
      '#name' => 'submit-first',
      '#value' => '|<',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_process_navigation'),
  );
  $form['submit-prev'] = array(
      '#type' => 'submit',
      '#id' => 'submit-prev',
      '#name' => 'submit-prev',
      '#value' => '<',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_process_navigation'),
  );
  $form['submit-next'] = array(
      '#type' => 'submit',
      '#id' => 'submit-next',
      '#name' => 'submit-next',
      '#value' => '>',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_process_navigation'),
  );
  $form['submit-last'] = array(
      '#type' => 'submit',
      '#id' => 'submit-last',
      '#name' => 'submit-last',
      '#value' => '>|',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_process_navigation'),
  );
  return $form;
}

function race_manager_navigation_bar($nav) {
  $spacer = race_manager_module_location();
  $spacer .= '/images/spacer.gif';
  $spacer = '<img class = "spacer" src="/' . $spacer . '">';
  $start = 0;
  if (array_key_exists('start', $nav)) {
    $start = $nav['start'];
  }
  $size = 0;
  if (array_key_exists('size', $nav)) {
    $size = $nav['size'];
  }
  $total = 0;
  if (array_key_exists('total', $nav)) {
    $total = $nav['total'];
  }
  $output = '<div class="navigation-bar" >';
  $output .= '<form id="race_manager-entrant-navigation" method = "POST" 
        enctype="multipart/form-data" ';
  $target = '';
  if (array_key_exists('target', $nav)) {
    $target = $nav['target'];
    $output .= 'action="' . $target . '"';
  }
  $output .= '>';
  $output .= '<input type ="hidden" name="nav-start" value="' . $start . '" />';
  $output .= '<input type ="hidden" name="nav-size" value="' . $size . '" />';
  $output .= '<input type ="hidden" name="nav-total" value="' . $total . '" />';
  $output .= '<input type ="hidden" name="nav-last" value="" />';

  $entrant_id = '';
  if (array_key_exists('entrant-id', $nav)) {
    $entrant_id = $nav['entrant-id'];
    $output .= '
      <input type ="hidden" name="entrant-id" value="' . $entrant_id . '" />';
  }
  $output .= '<table><tr>';
  $output .= '<td>
    ';
  if ($start > 0) {
    $output .= '<div>
      <input type="submit" title="First" value="<<" name="submit-first">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start >= $size) {
    $output .= '<div>
      <input type="submit" title="Prev" value="<" name="submit-prev">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start < $total - $size) {
    $output .= '<div>
      <input type="submit" title="Next" value=">" name="submit-next">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start < $total - $size) {
    $output .= '<div>
      <input type="submit" title="Last" value=">>" name="submit-last">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>';
  $output .= '</tr></table>
    ';
  $output .= '</form></div>';

  return $output;
}


/**
 *
 * Event add form
 * 
 */
function race_manager_admin_event_add_form($form, &$form_state) {
  
  $form['cancel'] = array(
      '#prefix' => '<div class="cancel">',
      '#suffix' => '</div>',
      '#markup' => '<a href="event" >Cancel</a>',
  );
  $id = race_manager_race_id_suggestion();
  $form['event_id'] = array(
      '#type' => 'textfield',
      '#id' => 'event-id',
      '#required' => TRUE,
      '#title' => t('Event ID'),
      '#default_value' => $id,
  );
  $form['name'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Name'),
  );
  $form['short_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Short Name'),
      '#required' => TRUE,
      '#description' => 'For use in select lists and reports',
  );
  $form['lap_distance'] = array(
      '#type' => 'textfield',
      '#title' => t('Event lap distance'),
      '#description' => 'The length of a full lap for this event.',
  );
  $form['reject_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Reject Limit'),
      '#description' => 'The threshold (in seconds) for rejecting duplicate entries.',
  );
  $form['submit_add'] = array(
      '#type' => 'submit',
      '#value' => t('Add Event'),
      '#id' => 'race-manager-event-add-submit',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}


/**
 *
 * Edition add form
 * 
 */
function race_manager_admin_edition_add_form($form, &$form_state) {
  if (array_key_exists('race_id', $_REQUEST)) {
    $race_id = $_REQUEST['race_id'];
  }
  $url = RACE_MANAGER_BASE_PATH . 'race-admin/event';
  $form['cancel'] = array(
      '#prefix' => '<div class="cancel">',
      '#suffix' => '</div>',
      '#markup' => '<a href="' . $url . ' " >Cancel</a>',
    );
  if (!$race_id) {
    $form['cancel'] = array(
      '#markup' => '<h3>No Event Selected</h3><a href="' . $url . ' " ><-- Cancel</a>',
    );
    return $form;
  }
  $options = Array();
  $options['current'] = FALSE;
  $options['race_id'] = $race_id;
  $options['sort'] = 'EDITION_ID DESC';
  $events = race_manager_events($options);
  $edition = FALSE;
  foreach ($events as $edition) {
  }
  $race_name = 'No Event Selected';
  $suggestions = Array();
  $suggestions['current'] = 1;
  $suggestions['race_edition'] = 1;
  $suggestions['description'] = 'Edition 1';
  $suggestions['offset_start'] = 0;
  $start_time = Date("Y-m-d");
  $stime = strtotime($start_time);
  $suggestions['start_time'] = date("Y-m-d H:i:s", $stime);
  $suggestions['finish_time'] = date("Y-m-d H:i:s", $stime);
  if ($edition) {
    $race_name = $edition->name;
    $suggestions['race_edition'] = $edition->race_edition + 1;
    $suggestions['description'] = $edition->description;
    $suggestions['offset_start'] = intval($edition->offset);
    $start_time = $edition->start_time;
    if ($start_time) {
      $stime = strtotime($start_time);
      $suggestions['start_time'] = date("Y-m-d H:i:s", $stime);
      $suggestions['finish_time'] = date("Y-m-d H:i:s", $stime);
    }
  }
  $form['title'] = array(
      '#type' => 'markup',
      '#markup' => '<h4>New Edition for: '. $race_name . '</h4>',
  );
  $form['event_id'] = array(
      '#type' => 'hidden',
      '#id' => 'race-id',
      '#default_value' => $race_id,
      '#value' => $race_id,
  );
  $form['current'] = array(
      '#type' => 'hidden',
      '#id' => 'current',
      '#default_value' => $suggestions['current'],
      '#value' => $suggestions['current']
  );
  $form['race_edition'] = array(
      '#type' => 'textfield',
      '#title' => t('Edition'),
      '#value' => $suggestions['race_edition']
  );
  $form['description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#value' => $suggestions['description']
  );
  $form['offset_start'] = array(
      '#type' => 'textfield',
      '#title' => t('Second race start'),
      '#description' => t('Offset for start of second race. (in seconds)'),
      '#value' => $suggestions['offset_start']
  );
  $form['start_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Start Time'),
      '#value' => $suggestions['start_time']
  );
  $form['finish_time'] = array(
      '#type' => 'textfield',
      '#title' => t('Finish Time'),
      '#value' => $suggestions['finish_time']
  );
  $form['submit_add'] = array(
      '#type' => 'submit',
      '#value' => t('Add Edition'),
      '#id' => 'race-manager-edition-add-submit',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}

/**
 * 
 *  * Events Management
 * 
 */
function race_manager_admin_event_form($form, &$form_state) {
  $form['current'] = array(
      '#type' => 'fieldset',
      '#title' => t('Current Editions'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  $html = '<table>';
  $html .= '<tr>';
  $html .= '<th>Race Name';
  $html .= '</th>';
  $html .= '<th>Start Time';
  $html .= '</th>';
  $html .= '<th>Reg Count';
  $html .= '</th>';
  $html .= '<th>Finisher Count';
  $html .= '</th>';
  $html .= '<th>Current';
  $html .= '</th>';
  $form['current']['start'] = array(
      '#prefix' => $html,
      '#suffix' => '</tr>',
      '#type' => 'markup',
      '#id' => 'race-manager-edition-start-submit',
      '#value' => t('Start Event'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $html = '</table>';
  $form['current']['table'] = array(
      '#type' => 'markup',
      '#suffix' => $html,
      '#markup' => race_manager_current_editions_table(),
  );

  $form['current']['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
  $form['current']['submit-container']['update'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-edition-submit',
      '#title' => t('Update'),
      '#value' => t('Update'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $collapsed = TRUE;
  if (array_key_exists('values', $form_state)) {
    if (array_key_exists('select', $form_state['values'])) {
      if (intval($form_state['values']['select'])) {
        $collapsed = FALSE;
      }
    }
  }
  $form['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Events'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  $url = RACE_MANAGER_BASE_PATH . 'race-admin/event/add';
  $form['events']['add'] = array(
      '#type' => 'markup',
      '#title' => t('Add Event'),
      '#prefix' => '<div class="action-links">',
      '#suffix' => '</div>',
      '#markup' => '<a href="' . $url .'" >Add Event</a>',
  );
  $form['events']['select'] = array(
      '#type' => 'select',
      '#title' => t('Select Event'),
      '#options' => race_manager_event_select_list(),
  );
  $form['events']['submit-container'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
        );
  $form['events']['submit-container']['choose'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-event-choose-submit',
      '#title' => t('Choose'),
      '#value' => t('Choose Event'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $id = 0;
  if (array_key_exists('select', $form_state['input'])) {
    $id = intval($form_state['input']['select']);
  }
  $selected_id = '0';
  $record = FALSE;
  if ($id) {
    $res = db_select('race_event', 'e')
            ->fields('e')
            ->condition('RACE_ID', $id)
            ->execute();
    foreach ($res as $record) {
      break;
    }
  }
  $name = '';
  $collapsed = TRUE;
  if ($record) {
    $collapsed = FALSE;
    $selected_id = $record->RACE_ID;
    $name = $record->name;
  }
  $form['events']['name'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="event-name">',
      '#suffix' => '</div>',
      '#markup' => $name,
  );
  $button = '';
  if (array_key_exists('triggering_element', $form_state)) {
    $button = trim($form_state['triggering_element']['#id']);
  }
  if ($button == 'race-manager-edition-submit') {
    $collapsed = TRUE;
  }
  if (!$button) {
    $collapsed = TRUE;
  }
  $collapsed = TRUE;
  $form['events']['data'] = array(
      '#type' => 'fieldset',
      '#title' => t('Event data'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['events']['data']['id'] = array(
      '#type' => 'hidden',
      '#value' => $selected_id,
  );
  $data = Array();
  $data['name'] = '';
  $data['short_name'] = '';
  $data['lap_distance'] = 0;
  $data['reject_limit'] = 0;
  if ($record) {
    $name = $record->name;
    $data['name'] = $name;
    $data['short_name'] = $record->short_name;
    $data['lap_distance'] = $record->lap_distance;
    $data['reject_limit'] = $record->reject_limit;
    $form['event']['data']['#collapsed'] = FALSE;
  }
   $form['events']['data']['name'] = array(
     '#type' => 'textfield',
//     '#required' => TRUE,
     '#title' => t('Name'),
     '#value' => $data['name'],
   );
  $form['events']['data']['short_name'] = array(
     '#type' => 'textfield',
     '#title' => t('Short Name'),
     '#value' => $data['short_name'],
   );
  $form['events']['data']['lap_distance'] = array(
     '#type' => 'textfield',
     '#title' => t('Lap Distance (miles)'),
     '#value' => $data['lap_distance'],
   );
  $form['events']['data']['reject_limit'] = array(
     '#type' => 'textfield',
      '#title' => t('Reject Limit (seconds)'),
     '#value' => $data['reject_limit'],
   );
  $form['events']['data']['submit'] = array(
      '#prefix' => '<div class="form-submit-container">',
      '#suffix' => '</div>',
      '#type' => 'submit',
      '#id' => 'race-manager-event-submit',
      '#value' => t('Save Event Data'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
 $type = 'hidden';
 if ($selected_id) {
   $type = 'fieldset';
 }
 $collapsed = FALSE;
 $form['events']['edition'] = array(
      '#type' => $type,
      '#title' => t(trim($name . ' Editions')),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
  );
  $url = RACE_MANAGER_BASE_PATH;
  $url .= 'race-admin/edition/add?race_id='.$selected_id;
  $form['events']['edition']['add'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="action-links">',
      '#suffix' => '</div>',
      '#title' => t('Add Edition'),
      '#markup' => '<a href="' . $url .'" >Add Edition</a>',
  );
  $form['events']['edition']['list'] = array(
      '#type' => 'markup',
      '#prefix' => '<table id="race-manager-editions">
      <tr class="header">
      <th><label for="edition_id">ID</label></th>
      <th><label for="description">Description</label></th>
      <th><label for="race_edition">Edition</label></th>
      <th><label for="start_time">Start Time</label></th>
      <th><label for="offset">Offset</label></th>
      <th><label for="reg count">Reg Count</label></th>
      <th><label for="finisher count">Finisher Count</label></th>
      <th><label for="archive count">Archive Count</label></th>
      <th><label for="current">Active</label></th>
      <th><label for="select">Select</label></th>
      </tr>',
      '#suffix' => '</table>',
      '#markup' => race_manager_editions_admin_table($selected_id),
  );
  /*   * 
   */
  $form['events']['edition']['save'] = array(
      '#prefix' => '<div class="form-submit-container">',
      '#type' => 'submit',
      '#id' => 'race-manager-edition-save-submit',
      '#value' => t('Save Edition Data'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  $form['events']['edition']['reset'] = array(
      '#suffix' => '</div>',
      '#type' => 'submit',
      '#id' => 'race-manager-edition-reset-submit',
      '#title' => t('Reset'),
      '#value' => t('Reset'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
 $collapsed = TRUE;
 $form['events']['archive'] = array(
      '#type' => 'fieldset',
      '#title' => t('Archive Results'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
  );
  $form['events']['archive']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-archive-submit',
      '#value' => t('Archive'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
 $collapsed = TRUE;
 $form['events']['import'] = array(
      '#type' => 'fieldset',
      '#title' => t('Import Results'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
  );
  $form['events']['import']['results-data'] = array(
      '#type' => 'textarea',
      '#title' => t('Enter the data here.'),
      '#description' => t('The raw results: [bib]<tab>[place]<tab>[split (in seconds)]'),
  );
  $form['events']['import']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'race-manager-results-import-submit',
      '#value' => t('Import'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('race_manager_admin_submit'),
  );
  return $form;
}

function race_manager_admin_home_form($form, &$form_state) {
  $base = RACE_MANAGER_BASE_PATH;
  global $user;
  $uid = 0;
  if ($user) {
    $uid = $user->uid;
  }
  $c39 = chr(39);
  $url = RACE_MANAGER_BASE_PATH;
  if ($uid) {
    $content = '<h3><a href="' . $url . 'chute-entry">Chute Entry</a></h3>';
    $form['link1'] = array(
        '#markup' => $content
    );
    $content = '<h3><a href="' . $url . 'chute-exit">Chute Exit</a></h3>';
    $form['link2'] = array(
        '#markup' => $content
    );
    $content = '<h3><a href="' . $url . 'race-admin/chute">Admin</a></h3>';
    $form['link3'] = array(
        '#markup' => $content
    );
    $content = '<h3><a href="' . $url . 'user/logout">Logout</a></h3>';
    $form['link5'] = array(
        '#markup' => $content
    );
  }
  $content = '<h3><a href="' . $url . 'user">Login</a></h3>';
  $form['link4'] = array(
        '#markup' => $content
  );
  $content = '<h3><a href="' . $url . '/log">Log</a></h3>';
  $form['link6'] = array(
        '#markup' => $content
  );
  return $form;
}

function race_manager_registration_form_validate($form, &$form_state) {
  if (array_key_exists('triggering_element', $form_state)) {
    $trigger = $form_state['triggering_element'];
  }
  if ($trigger['#id'] == 'race-manager-registration-choose-submit') {
    $select_name = '';
    if (array_key_exists('select_name', $form_state['input'])) {
      $select_name = trim($form_state['input']['select_name']);
    }
    $bib = 0;
    if (array_key_exists('select_bib', $form_state['input'])) {
      $bib = trim($form_state['input']['select_bib']);
    }
    if (!$select_name) {
      if (!$bib) {
        form_set_error('select_bib', 'Please choose a participant or a
          bib number.');
      }
    }
  }
  if ($trigger['#id'] == 'race-manager-registration-save-submit') {
    $edition_id = 0;
    if (array_key_exists('select_edition_id', $form_state['values'])) {
      $edition_id = intval($form_state['values']['select_edition_id']);
    }
    $bib = $form_state['input']['bib'];
    if (!$bib) {
      form_set_error('bib', 'A bib number is required.');
    }
    if (!$edition_id) {
      drupal_set_message('Warning: No event was selected.');
    }
    // dob
    if (isset($form_state['input']['dob'])) {
      $dob = Array(strval($form_state['input']['dob']['year']));
      $dob[] = strval($form_state['input']['dob']['month']);
      $dob[] = strval($form_state['input']['dob']['day']);
      $dob = implode('-', $dob);
      $age = race_manager_calculate_age($dob, date('Y-m-d'));
      if ($age < 5) {
        form_set_error('dob', 'Invalid Date of Birth.');
      }
    }
  }
}

function race_manager_registration_short_form_validate($form, &$form_state) {
  if (array_key_exists('triggering_element', $form_state)) {
    $trigger = $form_state['triggering_element'];
  }
  if ($trigger['#id'] == 'race-manager-registration-choose-submit') {
    if (array_key_exists('select_name', $form_state['input'])) {
      $select_name = trim($form_state['input']['select_name']);
      if (strlen($select_name) == 0) {
        $form_state['rebuild'] = FALSE;
        form_set_error('select_name', 'Please choose a participant.');
      }
      else {
        $parts = explode('(', $select_name);
        if (count($parts) < 2) {
          $test = intval($select_name);
          if ($test == 0) {
            $msg = 'Please select a participant from the predefined list.';
            $form_state['rebuild'] = FALSE;
            form_set_error('select_name', $msg);
          }
        }
      }
    }
  }
  if ($trigger['#id'] == 'race-manager-registration-save-submit') {
    $edition_id = '';
    if (array_key_exists('select_edition_id', $form_state['values'])) {
      $edition_id = $form_state['values']['select_edition_id'];
    }
    $bib = $form_state['input']['bib'];
    if (!$bib) {
      form_set_error('bib', 'A bib number is required.');
    }
    if (!$edition_id) {
      form_set_error('select_edition_id', 'Choose one of the events listed.');
    }
  }
}