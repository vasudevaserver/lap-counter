<?php

function race_manager_admin_start_timer_form($form, &$form_state) {
  $form['#attributes'] = array(
      'onsubmit' => 'return false');
/* $form['debug'] = array(
      '#id' => 'debug',
      '#type' => 'textarea',
      '#value' => 0,
      '#attributes' => array(
          'id' => 'debug'),
  );
 * 
 */
 $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array(
          'id' => 'base-path',
          'onsubmit' => 'return false'),
  );
  $html = '<table id="start-timer>"';
  $html .= '<tr>';
  $html .= '<th>Race Name';
  $html .= '</th>';
//  $html .= '<th>Start Timer';
//  $html .= '</th>';
  $html .= '<th>';
  $form['start'] = array(
      '#prefix' => $html,
      '#suffix' => '</th></tr>',
      '#type' => 'button',
      '#id' => 'race-manager-start-submit',
      '#value' => t('Start'),
      '#executes_submit_callback' => FALSE,
      '#attributes' => array(
      'onclick'=>'StartTimers();')
//      '#submit' => array('race_manager_admin_submit'),
  );
  $html = '</table>';
  $form['table'] = array(
      '#type' => 'markup',
      '#suffix' => $html,
      '#markup' => race_manager_edition_start_timer_table(),
  );
  return $form;
}

// The bib data entry form with javascript
function race_manager_bib_data_js_entry_form($form, &$form_state) {
  global $user;
  $uid = 0;
  if ($user) {
    $uid = $user->uid;
  }
  $c39 = chr(39);
 $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array('id' => 'base-path'),
  );
  $form['user-id'] = array(
      '#id' => 'userid',
      '#type' => 'hidden',
      '#value' => $uid,
      '#attributes' => array('id' => 'userid'),
  );
  $form['bib-number'] = array(
      '#title' => t('Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
          'onfocus' => 'this.value = ""',
          'onchange' => 'SetBibNumber(this.value);this.value = "";',
//          'onblur' => 'SetBibNumber(this.value);
//        ',
      ),
  );

  $form['log'] = array(
      '#prefix' => '<div id="bib-entry">',
      '#suffix' => '</div>',
      '#type' => 'textarea',
      '#id' => 'entry-items',
      '#rows' => 10,
      '#default_value' => '',
      '#attributes' => array(
          'onchange' => 'document.getElementById("bib-number").focus();return (false);',
          'onfocus' => 'document.getElementById("bib-number").focus();'
          ),
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#attributes' => array(
          'onclick' => 'document.getElementById("bib-number").focus();return (false);'),
//      '#attributes' => array('onclick' => 'return (false);'),
      '#executes_submit_callback' => FALSE,
      '#submit' => array('race_manager_bib_data_entry'),
  );

  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib-number");';
  $content .= 'ob.focus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}

// Javascript key assignments
function race_manager_js_key_assignments_form($form_type='entry') {
  $keys = race_manager_js_key_assignments();
  $return = Array();
  $return['enter'] = array(
      '#id' => 'enter',
      '#type' => 'hidden',
//      '#type' => 'textfield',
      '#value' => $keys['enter'],
      '#attributes' => array('id' => 'enter'),
  );
  $return['cancel'] = array(
      '#id' => 'cancel',
      '#type' => 'hidden',
//      '#type' => 'textfield',
      '#value' => $keys['cancel'],
      '#attributes' => array('id' => 'cancel'),
  );
    $return['deleterecord'] = array(
      '#id' => 'deleterecord',
      '#type' => 'hidden',
//    '#type' => 'textfield',
      '#value' => $keys['deleterecord'],
     '#attributes' => array('id' => 'deleterecord'),
  );
  switch ($form_type) {
    case 'entry':
      $return['pre-enter'] = array(
          '#id' => 'pre-enter',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['pre-enter'],
          '#attributes' => array('id' => 'pre-enter'),
          );
      $return['prev-finisher'] = array(
          '#id' => 'prev-finisher',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['prev-finisher'],
          '#attributes' => array('id' => 'prev-finisher'),
      );
      $return['same-time'] = array(
          '#id' => 'same-time',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['same-time'],
          '#attributes' => array('id' => 'same-time'),
      );
      break;
    case 'exit':
      $return['bandit'] = array(
          '#id' => 'bandit',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['bandit'],
          '#attributes' => array('id' => 'bandit'),
      );
      $return['altstart'] = array(
          '#id' => 'altstart',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['altstart'],
          '#attributes' => array('id' => 'altstart'),
      );
      $return['acceptall'] = array(
          '#id' => 'acceptall',
          '#type' => 'hidden',
//        '#type' => 'textfield',
          '#value' => $keys['acceptall'],
          '#attributes' => array('id' => 'bandit'),
      );
  }
  return $return;
}

// An on-screen key pad
function race_manager_mobile_key_pad($mode = 'entry') {
  $c39 = chr(39);
  $module_location = race_manager_module_location();
  $img = $module_location . '/images/keypad/';
  $html = '<div id="key-pad">';
  $html .= '<table id="mobile-key-pad"><tr>';
  $html .= '<td><input class="num" id="n7" type="button" value="7" onmouseup="SetBibNum(7);"/></td>';
  $html .= '<td><input class="num" id="n8" type="button" value="8" onmouseup="SetBibNum(8);"/></td>';
  $html .= '<td><input class="num" id="n9" type="button" value="9" onmouseup="SetBibNum(9);"/></td>';
  $html .= '<td><input class="fn" id="clr" type="button" value="clr--" onmouseup="BibFn(-1);"/></td>';
  $html .= '</tr><tr>';
  $html .= '<td><input class="num" id="n4" type="button" value="4" onmouseup="SetBibNum(4);"/></td>';
  $html .= '<td><input class="num" id="n5" type="button" value="5" onmouseup="SetBibNum(5);"/></td>';
  $html .= '<td><input class="num" id="n6" type="button" value="6" onmouseup="SetBibNum(6);"/></td>';
  if ($mode == 'entry') {
    $html .= '<td><input class="fn" id="pre" type="button" value="pre+" onmouseup="BibFn(2);"/></td>';
  }
  if ($mode == 'exit') {
    $html .= '<td><input class="fn" id="bdt" type="button" value="bdt=" onmouseup="BibFn(2);"/></td>';
  }
  $html .= '</tr><tr>';
  $html .= '<td><input class="num" id="n1" type="button" value="1" onmouseup="SetBibNum(1);"/></td>';
  $html .= '<td><input class="num" id="n2" type="button" value="2" onmouseup="SetBibNum(2);"/></td>';
  $html .= '<td><input class="num" id="n3" type="button" value="3" onmouseup="SetBibNum(3);"/></td>';
  if ($mode == 'entry') {
    $html .= '<td><input class="fn" id="pf" type="button" value="st =" onmouseup="BibFn(4);"/></td>';
  }
  if ($mode == 'exit') {
    $html .= '<td><input class="fn" id="alt" type="button" value="alt+" onmouseup="BibFn(3);"/></td>';
  }
  $html .= '</tr><tr>';
  $html .= '<td><input class="num" id="n0" type="button" value="0" onmouseup="SetBibNum(0);"/></td>';
  $html .= '<td colspan="2"><input id="record" type="button" value="record" onmouseup="BibFn(1);"/></td>';
  if ($mode == 'entry') {
    $html .= '<td><input class="fn" id="pf" type="button" value="pf /" onmouseup="BibFn(3);"/></td>';
  }
  if ($mode == 'exit') {
    $html .= '<td><input class="fn" id="accept" type="button" value="del /" onmouseup="BibFn(9);"/></td>';
  }
  $html .= '</tr></table></div>';
  return $html;
}

// The Chute Finish Line entry form for mobile devices
function race_manager_chute_entry_mobile_form($form, &$form_state) {
  $form = race_manager_js_key_assignments_form('entry');
  $form['#attributes'] = array(
      'name' => 'bibentryform',
      'onsubmit' => 'return false');
  $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array('id' => 'base-path'),
  );
  $form['chute-type'] = array(
      '#id' => 'chute-type',
      '#type' => 'hidden',
      '#value' => 'entry',
      '#attributes' => array('id' => 'chute_type'),
  );
  $form['bib_entry'] = array(
      '#type' => 'textfield',
      '#id' => 'bib_entry',
      '#name' => 'bib_entry',
      '#default_value' => '',
      '#attributes' => array(
//          'disabled' => FALSE,
      ),
  );
  $form['key-pad'] = array(
      '#type' => 'markup',
      '#markup' => race_manager_mobile_key_pad('entry'),
  );
  $log = race_manager_chute_entry_log(RACE_MANAGER_CHUTE_ENTRY_STATION_ID);
  $form['log'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="chute-items">',
      '#suffix' => '</div>',
      '#id' => 'bib-entry-log',
      '#markup' => $log,
      '#attributes' => array(
          'id' => 'entry-log',
          ),
  );
  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib_entry");';
  $content .= 'ob.value="";ob.focus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );  
  return $form;
}

// The Chute Finish Line entry form
function race_manager_chute_entry_form($form, &$form_state) {
  global $user;
  $name = $user->name;
  $form = race_manager_js_key_assignments_form('entry');
  $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array('id' => 'base-path'),
  );
/*  $form['user'] = array(
      '#title' => t('User'),
      '#type' => 'textfield',
      '#id' => 'user',
      '#name' => 'user',
      '#default_value' => '',
      '#value' => $name,
  );
 * 
 */
  $form['bib-number'] = array(
      '#title' => t('Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
      ),
  );
  $log = race_manager_chute_entry_log(RACE_MANAGER_CHUTE_ENTRY_STATION_ID);
  $form['log'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="chute-items">',
      '#suffix' => '</div>',
      '#id' => 'bib-entry',
      '#markup' => $log,
      '#attributes' => array(
          'onchange' => 'document.getElementById("bib-number").focus();return (false);',
//          'onfocus' => 'document.getElementById("bib-number").focus();'
          ),
  );
  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib_entry");';
  $content .= 'ob.value="";ob.focus();return false;
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}
  
// The Chute Exit form
function race_manager_chute_exit_form($form, &$form_state) {
  $form = race_manager_js_key_assignments_form('exit');
  $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array('id' => 'base-path'),
  );
  $form['bib-number'] = array(
      '#title' => t('Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib_entry',
      '#name' => 'bib_entry',
      '#default_value' => '',
      '#attributes' => array(
//          'onfocus' => 'this.value = ""',
//          'onchange' => 'SetChuteEntry(this.value)',
//          'onkeyup' => 'SetChuteEntry(event)',
//          'onblur' => 'SetChuteEntry(this.value);
//           document.getElementById("bib-number").focus();,
      ),
  );
  $log = race_manager_chute_exit_log();
  $form['log'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="chute-items">',
      '#suffix' => '</div>',
      '#id' => 'bib-entry',
      '#markup' => $log,
      '#attributes' => array(
          'onchange' => 'SetFocus();',
          ),
  );

  $form['submit'] = array(
      '#type' => 'textfield',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#attributes' => array(
          'onfocus' => 'SetFocus();',
          ),
  );
  $content = '
    <script type="text/javascript"> 
     SetFocus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}  

// The Chute Exit form - mobile
function race_manager_chute_exit_mobile_form($form, &$form_state) {
  $form = race_manager_js_key_assignments_form('exit');
  $form['#attributes'] = array(
      'onsubmit' => 'return false');
  $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array(
          'id' => 'base-path',
      'name' => 'bibexitform')
  );
  $form['chute-type'] = array(
      '#id' => 'chute-type',
      '#type' => 'hidden',
      '#value' => 'exit',
      '#attributes' => array('id' => 'chute_type'),
  );
  $option = race_manager_option_get(27);
  $option = $option[1];
  // disable input field for mobile devices?
  $form['bib_entry'] = array(
      '#type' => 'textfield',
      '#id' => 'bib_entry',
      '#name' => 'bib_entry',
      '#default_value' => '',
//      '#attributes' => array(
//          'disabled' => TRUE,
//          'disabled' => FALSE,
//      ),
  );
  if ($option) {
    $form['bib_entry']['#attributes'] = array('disabled' => TRUE);
  }
  $form['key-pad'] = array(
      '#type' => 'markup',
      '#markup' => race_manager_mobile_key_pad('exit'),
  );
  $log = race_manager_chute_exit_log();
  $form['log'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="chute-items">',
      '#suffix' => '</div>',
      '#id' => 'bib-entry',
      '#markup' => $log,
      '#attributes' => array(
          'onchange' => 'SetFocus();',
//          'onfocus' => 'document.getElementById("bib-number").focus();'
          ),
  );
  $content = '
    <script type="text/javascript"> 
     SetFocus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}  

// The Chute Admin form
function race_manager_admin_chute_form($form, &$form_state) {
  global $user;
  $form = race_manager_js_key_assignments_form('admin');
  $form['base-path'] = array(
      '#id' => 'base-path',
      '#type' => 'hidden',
      '#value' => RACE_MANAGER_BASE_PATH,
      '#attributes' => array('id' => 'base-path'),
  );
  $log = race_manager_chute_admin_log();
  $form['log'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="chute-items">',
      '#suffix' => '</div>',
      '#id' => 'bib-entry',
      '#markup' => $log,
  );
  return $form;
}  

// The bib data entry form
function race_manager_bib_data_entry_form($form, &$form_state) {
  global $user;
  $uid = 0;
  if ($user) {
    $uid = $user->uid;
  }
  $c39 = chr(39);
  $check = $_SERVER['REDIRECT_URL'];
  $url = RACE_MANAGER_BASE_PATH;
  $url_array = Array();
  $url_array[] = $url . 'entry-only';
  $url_array[] = $url . 'entry-js';
  if ($check != $url) {
    $use_js = race_manager_use_js();
    $use_js = $use_js[1];
    if ($use_js) {
      $url .= 'entry-js';
    } else {
      $url .= 'entry-only';
    }
    $content = '<a href="' . $url . '">Remove Header</a>&nbsp;';
    $form['link1'] = array(
        '#markup' => $content
    );
    $content = '<a href="chute-entry">Chute: Finish Line</a>&nbsp;';
    $form['link2'] = array(
        '#markup' => $content
    );
    $content = '<a href="chute-exit">Chute: Exit</a>';
    $form['link3'] = array(
        '#markup' => $content
    );
  }
  $form['entry'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bib Number entry'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
  );
  $form['entry']['user-id'] = array(
      '#id' => 'userid',
      '#type' => 'hidden',
      '#value' => $uid,
      '#attributes' => array('id' => 'userid'),
  );
  $form['entry']['bib-number'] = array(
      '#title' => t('Enter Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
          'onfocus' => 'this.value = ""',
      ),
  );
  $log = race_manager_entry_log($uid);
  $form['entry']['log'] = array(
      '#prefix' => '<div id="bib-entry">',
      '#suffix' => '</div>',
      '#type' => 'textarea',
      '#id' => 'entry-items',
      '#rows' => 10,
      '#default_value' => $log,
  );
  $form['entry']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#executes_submit_callback' => FALSE,
      '#submit' => array('race_manager_bib_data_entry'),
  );

  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib-number");';
  $content .= 'ob.focus();
    
    </script>';
  $form['entry']['script'] = array(
      '#markup' => $content,
  );
  return $form;
}
