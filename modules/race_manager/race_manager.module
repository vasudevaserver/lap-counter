  <?php
/**
 * @file
 * Provides support for race management.
 */

/**
* Implements hook_help.
*
* Displays help and module information.
*
* @param path
*   Which path of the site we're using to display help
* @param arg
*   Array that holds the current path as returned from arg() function

$basename = explode('.php', basename( __FILE__));
$basename = $basename[0];
$file = join(DIRECTORY_SEPARATOR, array(dirname(__FILE__)));
$file .= DIRECTORY_SEPARATOR . 'lib';
$file .= DIRECTORY_SEPARATOR . $basename;
$file .= DIRECTORY_SEPARATOR . $basename;
$tmp = $file . '.php';
if (file_exists($tmp)) {
   include_once $tmp;
 }
$tmp = $file . '.inc';
if (file_exists($tmp)) {
   include_once $tmp;
 }
*/

require_once 'race_manager.inc';

// Return the file system location of this module
  function race_manager_module_location() {
  
  $file = drupal_get_path('module', 'race_manager');
  return $file;
}

/**
 * Implements hook_permission().
 *
 * Since the access to our new custom pages will be granted based on
 * special permissions, we need to define what those permissions are here.
 * This ensures that they are available to enable on the user role
 * administration pages.
 */

function race_manager_permission() {
  return array(
    'add edit delete laps' => array(
      'title' => t('Add Edit Delete Laps'),
      'description' => t('Allow user to edit the laps of an entrant'),
    ),
    'administer race manager' => array(
      'title' => t('Administer Race Manager'),
      'description' => t('Allow user to administer Race Manager settings'),
    ),
  );
}

function race_manager_help($path, $arg) {
  switch ($path) {
  case 'admin/help#race_manager':
    $output = '<p>'. t('Main admin menu entry for Lap Counter.') .'</p>';
    return $output;
}
}

/**
 * Implementation of hook_menu().
 *
 * Called when Drupal is building menus.  Cache parameter lets module know
 * if Drupal intends to cache menu or not - different results may be
 * returned for either case.
 *
 * @param may_cache true when Drupal is building menus it will cache
 *
 * @return An array with the menu path, callback, and parameters.
 */

function race_manager_menu() {
  $items = array();
  /* Admin menus */
  $items['admin/config/race_manager'] = array(
    'title' => 'Race Manager',
    'description' => 'Count laps in a race.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('race_manager_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/race_manager/options'] = array(
    'title' => 'Race Manager Options',
    'description' => 'Manage Options.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('race_manager_admin_options_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/race_manager/entrants'] = array(
    'title' => 'Race Manager Entrants',
    'description' => 'Manage Entrants.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('race_manager_admin_entrant_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/race_manager/events'] = array(
    'title' => 'Manage Events',
    'description' => 'Manage Events.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('race_manager_admin_event_form'),
    'access arguments' => array('add edit delete laps'),
  );
  /* User interface */
  $items['scoreboard'] = array(
    'title' => 'Scoreboard',
    'description' => 'Show the current standings.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('scoreboard'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['final'] = array(
    'title' => 'Final Results',
    'description' => 'Show the Final Results.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('final'),
    'access arguments' => array('access content'),
  );
  $items['entrant'] = array(
    'title' => 'Entrant',
    'description' => 'Show the selected entrant\'s info.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('entrant'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['insert-lap'] = array(
    'title' => 'Insert Lap',
    'description' => 'Insert a lap for the entrant.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('insertlap'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['log'] = array(
    'title' => 'Log',
    'description' => 'Show the most recent activity.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('log'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['test'] = array(
    'title' => 'Test Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('test'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['entry'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('dataentry'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['entry-only'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('dataentryonly'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['entry-js'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter bib data.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('dataentryjs'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['chute-entry'] = array(
    'title' => 'Chute Finish Line',
    'description' => 'Enter participants at finish line.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('dataentrychute'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['chute-exit'] = array(
    'title' => 'Chute Exit',
    'description' => 'Enter bib numbers for participants exiting chute.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('dataentrychuteexit'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['counter'] = array(
    'title' => 'Station Log',
    'description' => 'Log of Station.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('stationlog'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['entrants'] = array(
    'title' => 'Runners',
    'description' => 'List of all Runners.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('entrants'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['race-admin/system'] = array(
    'title' => 'System Settings',
    'description' => 'Manage global options and settings.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminsystem'),
    'access arguments' => array('administer race manager'), 
  );
  $items['awards'] = array(
    'title' => 'Awards',
    'description' => 'Awards / Results.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('awards'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['process-laps'] = array(
    'title' => 'Process Laps',
    'description' => 'Process the lap data.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('cronjob'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['race-admin'] = array(
    'title' => 'Admin',
    'description' => 'Administer Race Manager',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadmin'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/timer'] = array(
    'title' => 'Start',
    'description' => 'Start an Event with a timer.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminstarttimer'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/chute'] = array(
    'title' => 'Confirm Chute Data',
    'description' => 'Administer Race Manager',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminchute'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/entrant'] = array(
    'title' => 'Admininster Entrants',
    'description' => 'Manage participant data and settings.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminentrant'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/registration/short'] = array(
    'title' => 'Registration Short Form',
    'description' => 'Participant registration short form.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminregistrationshort'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/registration'] = array(
    'title' => 'Entrant Registration',
    'description' => 'Participant registration.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminregistration'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/registration/manage'] = array(
    'title' => 'Registration Management',
    'description' => 'Manage registration.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminregistrationmanage'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/registration/clear'] = array(
    'title' => 'Clear edition of all registrants',
    'description' => 'Empty the registration list for this edition.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminregistrationclear'),
    'access arguments' => array('administer race manager'), 
      );
  $items['race-admin/registration/autocomplete'] = array(
    'title' => 'Select Participant',
    'description' => 'Manage participant registration.',
    'page callback' => '_race_manager_registration_autocomplete',
    'access arguments' => array('access content'), /* can do better */
    'type' => MENU_CALLBACK
      );

  $items['race-admin/event'] = array(
    'title' => 'Manage Events',
    'description' => 'Manage Events.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminevent'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/event/add'] = array(
    'title' => 'Create New Event',
    'description' => 'Add new event.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadmineventadd'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/edition/add'] = array(
    'title' => 'Create New Edition',
    'description' => 'Add new edition.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadmineditionadd'),
    'access arguments' => array('administer race manager'), 
  );
  $items['race-admin/home'] = array(
    'title' => 'Race Administration',
    'description' => 'Race Administration.',
    'page callback' => 'race_manager_page_content',
    'page arguments' => array('raceadminhome'),
    'access arguments' => array('administer race manager'), 
  );
  return $items;
}

function race_manager_theme() {

  return array(
    'race_manager_page_content' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_test' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_scoreboard' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_final' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_log' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_stationlog' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_entrant' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_awards' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_entrants' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_insert_lap' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_dataentry' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_dataentryonly' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_dataentryjs' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_dataentrychute' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_dataentrychuteexit' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_cronjob' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadmin' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminentrant' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminregistration' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminregistrationshort' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminregistrationmanage' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminregistrationclear' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminchute' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadmineventadd' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadmineditionadd' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminevent' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminhome' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminstarttimer' => array(
      'template' => 'race_manager-content',
    ),
    'race_manager_raceadminsystem' => array(
      'template' => 'race_manager-content',
    ),
  );
}

function race_manager_preprocess_race_manager_test(&$variables) {
  $variables['page_title'] = 'TEst';
  $variables['display_id'] = 'test';
  $variables['messages'] = '';
  }

/**
 * Implementation of template_preprocess_race_manager_scoreboard().
 */
function race_manager_preprocess_race_manager_scoreboard(&$variables) {
  $variables['page_content'] = race_manager_standings_table();
  $variables['page_title'] = 'Scoreboard';
  $variables['display_id'] = 'scoreboard';
  $variables['messages'] = '';
  }

/**
 * Implementation of template_preprocess_race_manager_final().
 */
function race_manager_preprocess_race_manager_final(&$variables) {
  $show_lap = 0;
  if (array_key_exists('show_lap', $_REQUEST)) {
    $show_lap = $_REQUEST['show_lap'];
  }
  $variables['page_content'] = race_manager_results_table($show_lap);
  $variables['page_title'] = 'Final Results';
  $variables['display_id'] = 'final';
  $variables['messages'] = '';
  }

function race_manager_preprocess_race_manager_awards(&$variables) {
  $variables['page_content'] = race_manager_awards_table();
  $variables['page_title'] = 'Results by Category';
  $variables['display_id'] = 'awards';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Entrant view.
 */
function race_manager_preprocess_race_manager_entrant(&$variables) {
  global $entrants;
  $entrant_id = race_manager_get_request_value( 'entrant-id' ,'');
  $entrant = $entrants[$entrant_id];
// Load the insert form here to process any inserts
  $form_insert = drupal_get_form('race_manager_insert_lap_form');
  $variables['page_content'] = race_manager_entrant_table($entrant);
  $variables['page_title'] = $entrant->full_name . ' (' . $entrant->bib . ') - ' .
          $entrant->race_name;
  $variables['display_id'] = 'entrant';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Entrants view.
 */
function race_manager_preprocess_race_manager_entrants(&$variables) {
  
  $variables['page_content'] = race_manager_entrants_table();
  $variables['page_title'] = 'Runners';
  $variables['display_id'] = 'entrants';
  $variables['messages'] = '';
  }

  
/**
 * Set up variables for Lap Insert form.
 */
function race_manager_preprocess_race_manager_insert_lap(&$variables) {
  global $entrants;
  $entrant_id = race_manager_get_request_value( 'entrant-id' ,'');
  $entrant = $entrants[$entrant_id];
  $title = 'Insert Lap for: ' . $entrant->full_name . ' (' . $entrant->bib . ')';
  // Load the form here so we can return to the entrant page
  $form_insert = drupal_get_form('race_manager_insert_lap_form');
  $content = race_manager_insert_lap($form_insert, $entrant_id);
  $variables['page_content'] = $content;
  $variables['page_title'] = $title;
  $variables['display_id'] = 'insertlap';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Data Entry.
 */
function race_manager_preprocess_race_manager_dataentry(&$variables) {  
  $dataform = drupal_get_form('race_manager_bib_data_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentry';
  $variables['messages'] = '';
  }
/**
 * Set up variables for Data Entry.
 */
function race_manager_preprocess_race_manager_dataentryonly(&$variables) {  
  $dataform = drupal_get_form('race_manager_bib_data_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentryonly';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Javascript-based Data Entry.
 */
function race_manager_preprocess_race_manager_dataentryjs(&$variables) {  
  $dataform = drupal_get_form('race_manager_bib_data_js_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentryjs';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Chute - Finish Line.
 */
function race_manager_preprocess_race_manager_dataentrychute(&$variables) {  
  global $user;
  $name = $user->name;
  $mobile = race_manager_detect_mobile();
  $form_id = 'race_manager_chute_entry';
//  if ($mobile) {
  if (TRUE) {
      $form_id .= '_mobile';
    if (substr($name, 0, 1) == '1') {
    }
  }
  $form_id .= '_form';
  $dataform = drupal_get_form($form_id);
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Chute: Finish Line';
  $variables['display_id'] = 'dataentrychute';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Chute - Finish Line.
 */
function race_manager_preprocess_race_manager_dataentrychuteexit(&$variables) {  
  $mobile = race_manager_detect_mobile();
  $form_id = 'race_manager_chute_exit';
//  if ($mobile) {
  if (TRUE) {
      $form_id .= '_mobile';
  }
  $form_id .= '_form';
  $dataform = drupal_get_form($form_id);
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Chute: Exit';
  $variables['display_id'] = 'dataentrychuteexit';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Station Log view.
 */
function race_manager_preprocess_race_manager_stationlog(&$variables) {
  $filter = Array();
  $edition_id = race_manager_get_request_value( 'edition-id' ,'');
  $current_station_id = race_manager_get_request_value( 'station-id' ,'');
  $races = race_manager_current_events();
  $text = 'All Runners';
  if ($current_station_id) {
    $filter['station_id'] = $current_station_id;
    $text = 'Station: ' . $current_station_id;
  }
  if ($edition_id) {
    $filter['EDITION_ID'] = $edition_id;
    foreach ($races as $race) {
      if ($race->EDITION_ID == $edition_id) {
            $text = $race->short_name . ' Race';
            break;
          }
        }
        unset($filter['station_id']);
      }
  $variables['page_content'] = race_manager_entrant_station_log_table($filter);
  $variables['page_title'] = $text;
  $variables['display_id'] = 'stationlog';
  $variables['messages'] = '';
  }

/**
 * Set up variables to process lap data.
 */
function race_manager_preprocess_race_manager_cronjob(&$variables) {
  $variables['page_content'] = race_manager_cron_job();
  $variables['page_title'] = 'Process Laps';
  $variables['display_id'] = 'cronjob';
  $variables['messages'] = '';
  $refresh_rate = race_manager_cron_rate();
  $args = Array();
  $args['#tag'] = 'meta';
  $args['#attributes'] = Array(
      'http-equiv' => 'Refresh',
      'content' => $refresh_rate
  );
    drupal_add_html_head($args, 'refresh_rate');
  }

/**
 * Set up variables Race Manager Admin page.
 */
function race_manager_preprocess_race_manager_raceadmin(&$variables) {
  $variables['page_content'] = race_manager_admin();
  $variables['page_title'] = 'Administer Race Manager';
  $variables['display_id'] = 'raceadmin';
  $variables['messages'] = '';
}


/**
 * Set up a home page for the Race Manager Admin.
 */

function race_manager_preprocess_race_manager_raceadminhome(&$variables) {
  $form = drupal_get_form('race_manager_admin_home_form');
  $variables['page_content'] = drupal_render($form);
  $variables['page_title'] = 'Race Manager Home Page';
  $variables['display_id'] = 'raceadminhome';
  $variables['messages'] = '';
}

/**
 * Set up variables Race Manager Admin page.
 */

function race_manager_preprocess_race_manager_raceadminchute(&$variables) {
  $variables['page_content'] = race_manager_admin_chute();
  $variables['page_title'] = 'Finisher Processing';
  $variables['display_id'] = 'raceadminchute';
  $variables['messages'] = '';
}

function race_manager_preprocess_race_manager_raceadminregistrationmanage(&$variables) {
  $variables['page_content'] = race_manager_admin_registration('manage');
  $variables['page_title'] = 'Manage Registration';
  $variables['display_id'] = 'raceadminregistrationmanage';
  $variables['messages'] = '';
}

function race_manager_preprocess_race_manager_raceadminregistrationshort(&$variables) {
  $variables['page_content'] = race_manager_admin_registration('short');
  $variables['page_title'] = 'Quick Registration:';
  $variables['display_id'] = 'raceadminregistrationshort';
  $variables['messages'] = '';
}
function race_manager_preprocess_race_manager_raceadminregistration(&$variables) {
  $variables['page_content'] = race_manager_admin_registration('');
  $variables['page_title'] = 'Registration:';
  $variables['display_id'] = 'raceadminregistration';
  $variables['messages'] = '';
}

function race_manager_preprocess_race_manager_raceadminregistrationclear(&$variables) {
  $variables['page_content'] = race_manager_admin_registration_clear_confirm();
  $variables['page_title'] = 'Clear Registration';
  $variables['display_id'] = 'raceadminregistrationclear';
  $variables['messages'] = '';
}

function race_manager_preprocess_race_manager_raceadminentrant(&$variables) {
  $variables['page_content'] = race_manager_admin_entrant();
  $variables['page_title'] = 'Entrants';
  $variables['display_id'] = 'raceadminentrant';
  $variables['messages'] = '';
}

/**
 * Set up variables for Events management.
 */
function race_manager_preprocess_race_manager_raceadminevent(&$variables) {
  $variables['page_content'] = race_manager_admin_event();
  $variables['page_title'] = 'Events';
  $variables['display_id'] = 'raceadminevents';
  $variables['messages'] = '';
  }

function race_manager_preprocess_race_manager_raceadminstarttimer(&$variables) {
  $module_location = race_manager_module_location();
  $jsfile = $module_location . '/js/timer.js';
  drupal_add_js($jsfile);
  $variables['page_content'] = race_manager_admin_start_timer();
  $variables['page_title'] = 'Start Timer';
  $variables['display_id'] = 'raceadminstarttimer';
  $variables['messages'] = '';
  }

function race_manager_preprocess_race_manager_raceadmineventadd(&$variables) {
  $variables['page_content'] = race_manager_admin_event_add();
  $variables['page_title'] = 'Add Event';
  $variables['display_id'] = 'raceadmineventsadd';
  $variables['messages'] = '';
  }

function race_manager_preprocess_race_manager_raceadmineditionadd(&$variables) {  
  $variables['page_content'] = race_manager_admin_edition_add();
  $variables['page_title'] = 'Add Edition';
  $variables['display_id'] = 'raceadmineditionadd';
  $variables['messages'] = '';
  }

function race_manager_preprocess_race_manager_raceadminsystem(&$variables) {  
  $variables['page_content'] = race_manager_admin_system_options();
  $variables['display_id'] = 'raceadminsystem';
  $variables['page_title'] = 'System Settings';
  $variables['messages'] = '';
  }
  

/**
 * Set up variables for Log view.
 */
  
function race_manager_preprocess_race_manager_log(&$variables) {
  $variables['page_content'] = race_manager_log_table();
  $variables['page_title'] = 'Log';
  $variables['display_id'] = 'log';
  $variables['messages'] = '';
}