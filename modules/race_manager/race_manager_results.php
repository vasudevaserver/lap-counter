<?php
/*
Name: race_manager_results
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.10
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2012  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * Import a results set for a race edition from an external spreadsheet
 * Row Format:  |  Bib  |  place  |  Time |  Lap ( = 1 ) |
 * 
 */
function race_manager_import_results($edition_id, $data) {
  if (!$edition_id) {
    return;
  }
  $report = Array();
  $done = Array();
  $missing = Array();
  $options = Array('edition_id' => $edition_id);
  $editions = race_manager_events($options);
  if (!$editions) {
    return;
  }
  $edition = FALSE;
  foreach ($editions as $edition) {
    break;
  }
  if (!$edition) {
    return;
  }
  $start_time = $edition->start_time;
  $sttime = strtotime($start_time);
  $line_items = explode(chr(13), $data);
  foreach ($line_items as $line_item) {
    $results = Array();
    // tab-delimited data is required
    $dataset = explode(chr(9), $line_item);
    $t = trim(implode('', $dataset));
    if (strlen($t) == 0) {
      continue;
    }
    // Capture data line by line and load into array
    $bib = intval(trim($dataset[0]));
    $entrant_id = race_manager_get_entrant_id_from_bib($bib, $edition_id);
    if (!$entrant_id) {
      $missing[] = $bib;
      continue;
    }
    $done[] = $bib;
    $results['bib'] = $bib;
    $results['entrant_id'] = $entrant_id;
    $results['entry_id'] = -1;
    $place = intval(trim($dataset[1]));
    $split = trim($dataset[2]);
    $ssplit = race_manager_strtime_to_seconds($split);
    if (!$ssplit) {
      continue;
    }
    $results['split'] = $ssplit;
    // generate finish timestamp
    $ftime = $sttime + $ssplit;
    $fttime = strftime("%Y-%m-%d %X", $ftime);
    $results['lap_time'] = $fttime;
    // calculate lap value
    $lap = 1;
    if (count($dataset) > 3) {
      $lap = intval(trim($dataset[3]));
    }
    $results['lap'] = $lap;
    $res = db_select('race_laps', 'l')
            ->fields('l')
            ->condition('lap', $lap)
            ->condition('entrant_id', $entrant_id)
            ->execute();
    $found = FALSE;
    foreach ($res as $value) {
      $found = TRUE;
      break;
    }
    if (!$found) {
      // Insert the result into the lap table
      $entry_id = db_insert('race_laps')
            ->fields($results)
            ->execute();
    }
    // Update the place value in the entrant table
    $res = db_update('race_entrant')
            ->fields(Array('place'=>$place,'alt_start'=>0))
            ->condition('ENTRANT_ID', $entrant_id)
            ->execute();
  }
  $report['done'] = $done;
  $report['missing'] = $missing;
  return $report;
}

/**
 * generate the Final Results table
 */

function race_manager_results_table($show_lap = 0) {
  $standings = race_manager_standings($show_lap);
  $gender_select = Array('M'=>'Men','F'=>'Women');
  $output = '<div id="race-manager-listing">
    ';
/*  $output .= '<div class="lap-links">';
  $output .= '<a href="?show_lap=0">All</a>';
  $output .= '<a href="?show_lap=1">1 Mile</a>';
  $output .= '<a href="?show_lap=2">2 Miles</a>';
  $output .= '<a href="?show_lap=3">3 Miles</a>';
  $output .= '<a href="?show_lap=4">4 Miles</a>';
  $output .= '<a href="?show_lap=5">5 Miles</a>';
  $output .= '</div>';
 * 
 */
  foreach ($standings as $edition_id => $race_data) {
    $info = $race_data['info'];
    $race_name = $info['race_description'];
    $output .= '<table id="race-manager-final">
      <tbody>
        <tr>
 ';
    $output .= '<td class="race-manager-subtitle"
        colspan = "2" >' . $race_name . '</td>
        </tr>';
      $output .= '<tr>';
      foreach ($gender_select as $gender => $gtitle) {
        $output .= '<td><table class="race-manager-splits"><tr>
          ';
        $output .= '<td class="race-manager-subtitle" 
          colspan = "4">' . $gtitle . '</td></tr>
        ';
//          <th class="aligncenter col-data-small">Laps</th>
        $output .= '<tr class="header-row">
          <th class="aligncenter col-data-small">Place</th>
          <th class="col-data-large">Name (bib)</th>
          <th class="aligncenter col-data-small">Time</th>
          </tr>
          ';
        $data = $race_data[$gender];
        $ctr = 0;
        foreach ($data as $datum) {
          $ctr++;
          $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
          $output .= '<td class="aligncenter">' . $ctr. '</td>
            ';
          $entrant_url = race_manager_entrant_base_url($datum->entrant_id);
          $output .= '<td class="full-name"><a href="' . $entrant_url
           . '&nav_last=1#lastlap">
          ';
          $output .= $datum->full_name . ' (' . $datum->bib . ')';
          $output .= '</a></td>
          ';
          $value = $datum->laps;
//          $output .= '<td class="aligncenter">' . $value . '</td>';
          $value = race_manager_format_timestamp($datum->elapsed_time);
          $output .= '<td class="aligncenter">' . $value . '</td>
           </tr>';
        }
        $output .= '</table></td>
        ';
      }
      $output .= '</tr></table>
        ';
      $output .= '</td></tr>
        ';
    $output .= '</tbody></table>
        ';
  }
  $output .= '</div>';
  return $output;
}

/**
 *  A generic open results table divided by gender (no categories)
 */

function race_manager_awards_table($catgories = Array(1), $show_lap = 0) {
  $standings = race_manager_category_standings($catgories, $show_lap);
  $gender_select = Array('M'=>'Men','F'=>'Women');
  $output = '<div id="race-manager-listing">
    ';
  foreach ($standings as $edition_id => $edition_data) {
    $info = $edition_data['info'];
    $race_name = $info['race_name'];
    foreach ($edition_data['data'] as $regime_data){
      $r_name = $regime_data['info'];
      $regime_data = $regime_data['data'];
      foreach ($regime_data as $cat_data){
        $cat_name = $cat_data['info'];
        $cat_data = $cat_data['data'];
      $output .= '<table id="race-manager-scoreboard">
      <tbody>
        <tr>
 ';
//      $race_edition = $resultset['race_edition'];
    $output .= '<td class="race-manager-subtitle"
        colspan = "2" >' . $race_name . ' : ' . $cat_name . '</td>
        </tr>';
      $output .= '<tr>';
      foreach ($gender_select as $gender => $gtitle) {
        $output .= '<td><table class="race-manager-standings"><tr>
          ';
        $output .= '<td class="race-manager-subtitle" 
          colspan = "4">' . $gtitle . '</td></tr>
        ';
        $output .= '<tr class="header-row">
          <th class="aligncenter col-data-small">Place</th>
          <th class="col-data-large">Name (bib)</th>
          <th class="aligncenter col-data-small">Time</th>
          </tr>
          ';
        $data = $cat_data[$gender];
        $ctr = 0;
        foreach ($data as $datum) {
          $ctr++;
          $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
          $output .= '<td class="aligncenter">' . $ctr. '</td>
            ';
          $entrant_url = race_manager_entrant_base_url($datum->entrant_id);
//          $entrant_url = '/scoreboard?entrant_id=' . $datum->entrant_id;
          $output .= '<td class="full-name"><a href="' . $entrant_url
           . '&nav_last=1#lastlap">
          ';
          $output .= $datum->full_name . ' (' . $datum->bib . ')';
          $output .= '</a></td>
          ';
          $value = race_manager_format_timestamp($datum->elapsed_time);
          $output .= '<td class="aligncenter">' . $value . '</td>
           </tr>';
        }
        $output .= '</table></td>
        ';
      }
      
      $output .= '</tr></table>
        ';
      $output .= '</td></tr>
        ';
    $output .= '</tbody></table>
        ';
      }
  }
    }
  $output .= '</div>';
  return $output;
}

/**
 * generate the standings table
 */

function race_manager_standings_table() {
  $standings = race_manager_standings();
  $gender_select = Array('M'=>'Men','F'=>'Women');
  $output = '<div id="race-manager-listing">
    ';
  foreach ($standings as $edition_id => $race_data) {
    $info = $race_data['info'];
    $race_name = $info['race_name'];
    $output .= '<table id="race-manager-scoreboard">
      <tbody>
        <tr>
 ';
//      $race_edition = $resultset['race_edition'];
    $output .= '<td class="race-manager-subtitle"
        colspan = "2" >' . $race_name . '</td>
        </tr>';
      $output .= '<tr>';
      foreach ($gender_select as $gender => $gtitle) {
        $output .= '<td><table class="race-manager-standings"><tr>
          ';
        $output .= '<td class="race-manager-subtitle" 
          colspan = "4">' . $gtitle . '</td></tr>
        ';
        $output .= '<tr class="header-row">
          <th class="aligncenter col-data-small">Place</th>
          <th class="col-data-large">Name (bib)</th>
          <th class="aligncenter col-data-small">Laps</th>
          <th class="aligncenter col-data-small">Km</th>
          </tr>
          ';
        $data = $race_data[$gender];
        $ctr = 0;
        foreach ($data as $datum) {
          $ctr++;
          $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
          $output .= '<td class="aligncenter">' . $ctr. '</td>
            ';
          $entrant_url = race_manager_entrant_base_url($datum->entrant_id);
//          $entrant_url = '/scoreboard?entrant_id=' . $datum->entrant_id;
          $output .= '<td class="full-name"><a href="' . $entrant_url
           . '&nav_last=1#lastlap">
          ';
          $output .= $datum->full_name . ' (' . $datum->bib . ')';
          $output .= '</a></td>
          ';
          $output .= '<td class="aligncenter">' . $datum->laps . '</td>
            <td class="aligncenter">' . intval(intval($datum->laps)*0.4)/1 . '</td>
           </tr>';
        }
        $output .= '</table></td>
        ';
      }
      
      $output .= '</tr></table>
        ';
      $output .= '</td></tr>
        ';
    $output .= '</tbody></table>
        ';
  }
  $output .= '</div>';
  return $output;
}

/**
 * Prepare results data for the desired output
 */
function race_manager_results($args = Array()) {
  $type = 0;
  if (array_key_exists('type', $args)) {
    $type = $args['type'];
  }
  switch ($type) {
    case 0 :
      // Prepare a roster of participants
      $edition_list = Array();
      $res = db_select('race_edition', 'e')
              ->fields('e')
              ->condition('race_date', '2014-01-03', '>')
              ->execute();
      $edition_list[] = Array();
      foreach ($res as $record) {
        $edition_list[] = $record->EDITION_ID;
      }
      $sql1 = 'SELECT EDITION_ID FROM race_edition ';
      $sql1 .= 'WHERE race_date > "2014-01-05"';
      $sql2 = 'SELECT bib, split, place FROM race_laps INNER JOIN race_entrant ';
      $sql2 .= 'ON race_laps.entrant_id = race_entrant.ENTRANT_ID ';
      $sql2 .= 'INNER JOIN ( '.$sql1 .') AS edition';
      $res = db_query($sql2);
      break;
  }
    
  
  $sql = 'SELECT race_edition.EDITION_ID, race_edition.race_id, ';
}
/**
 * Prepare the standings data for the current editions
 */
function race_manager_standings($show_lap = 0) {
  $sql = 'SELECT race_edition.EDITION_ID, race_edition.race_id, ';
  $sql .= 'name AS race_name, race_edition.race_edition, ';
  $sql .= 'race_edition.description ';
  $sql .= 'FROM race_edition INNER JOIN race_event ';
  $sql .= 'ON race_edition.race_id = race_event.RACE_ID ';
  $sql .= ' WHERE (';
  $sql .= 'race_edition.current=1) ';
  $sql .= 'ORDER BY RACE_ID DESC';
  $res = db_query($sql);
  $race_editions = Array();
  $result = Array();
  foreach ($res as $record) {
    $data = Array();
    $edition_id = $record->EDITION_ID;
    $data['race_id'] = $record->race_id;
    $data['race_edition'] = $record->race_edition;
    $data['race_description'] = $record->description;
    $data['race_name'] = $record->race_name;
    $race_editions[] = $edition_id;
    $result[$edition_id]['info'] = $data;
  }
  $gender_select = Array('M', 'F');
  foreach ($gender_select as $gender) {
    $setgender = db_update('race_control')
      ->fields(array('current_gender' => $gender))
      ->condition('control_id', 1)
      ->execute();
    foreach ($race_editions as $edition_id) {
      $standings = race_manager_edition_standings(
              $edition_id,
              $gender,
              $show_lap);
      $result[$edition_id][$gender] = $standings;
    }
  }
  return $result;
}

/**
 * Prepare the standings data for the current editions
 * separated by category
 */
function race_manager_category_standings(
        $categories = Array(1),
        $show_lap = 0) {
  $editions = race_manager_current_events();
  $result = Array();
  foreach ($editions as $edition_id => $edition) {
    $standings = race_manager_edition_category_standings(
            $edition_id,
            $categories,
            $show_lap);
    $data = Array();
    $data['race_id'] = $edition->race_id;
    $data['race_edition'] = $edition->race_edition;
    $data['race_description'] = $edition->description;
    $data['race_name'] = $edition->name;
    $result[$edition_id]['info'] = $data;
    $result[$edition_id]['data'] = $standings;
  }
  return $result;
}


### Function: Return an array of the status of the race
function race_manager_edition_split($edition_id, $gender) {
  
}

### Function: Return an array of the current standings
function race_manager_edition_standings($edition_id, $gender, $show_lap = 0) {
  $offset = race_manager_get_offset($edition_id);
//  $offset = race_manager_get_offset(0);
  $start_time = race_manager_current_event_start_time($edition_id);
  $sstime = strftime("%Y-%m-%d %X", $start_time);
  $sql  = 'SELECT MAX(race_laps.lap) AS laps, race_laps.entrant_id, ';
  $sql .= 'COUNT(race_laps.lap) AS lap_count, ';  
  $sql .= 'MAX(race_laps.lap_time) AS last_time, ';  
  $sql .= '(TIME_TO_SEC(';
  $sql .= 'TIMEDIFF(MAX(race_laps.lap_time), MAX(race_edition.start_time))';
  $sql .= ') - (MAX(race_edition.offset_start)* MAX(race_entrant.alt_start))) ';
  $sql .= 'as elapsed_time, ';
  $sql .= 'MAX(race_entrant.full_name) AS full_name, ';
  $sql .= 'MAX(race_entrant.bib) as bib ';
  $sql .= 'FROM race_laps INNER JOIN race_entrant ON ';
  $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= 'INNER JOIN race_partcpt ';
  $sql .= 'ON race_entrant.partcpt_id = race_partcpt.ID ';
  $sql .= 'INNER JOIN race_control ';
  $sql .= 'ON race_partcpt.gender = race_control.current_gender ';
  $sql .= 'INNER JOIN race_edition ';
  $sql .= 'ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((race_entrant.edition_id = ' . $edition_id . ')';
  $sql .= ' AND (race_laps.lap_time < NOW() - INTERVAL ' . $offset . ' SECOND)';
  if ($show_lap) {
    $sql .= ' AND (race_laps.lap = ' . $show_lap . ')';
  }
  $sql .= ' AND (race_laps.deleted = 0)';
  $sql .= ' AND (race_control.control_id = 1)';
  $sql .= ') ';
  $sql .= 'GROUP BY race_laps.entrant_id ';
  $sql .= 'ORDER BY laps DESC, elapsed_time';
  $res = db_query($sql);
  $return = Array();
  if ($res) {
    $return = $res;
  }
  return $return;
}


### Function: Return an array of the current standings
function race_manager_edition_category_standings(
        $edition_id,
        $regimes,
        $show_lap = 0) {
  $c = chr(39);
  $gender_select = Array('M', 'F');
  $results = Array();
  $offset = race_manager_get_offset($edition_id);
  $start_time = race_manager_current_event_start_time($edition_id);
  $sstime = strftime("%Y-%m-%d %X", $start_time);
  foreach ($regimes as $regime_id) {
    $regime = db_select('race_category_regime', 'r')
            ->fields('r')
            ->condition('ID', $regime_id)
            ->execute();
    $regime = $regime->fetchAssoc();
    $results[$regime_id]['info'] = $regime['description'];
    $categories = db_select('race_category', 'c')
            ->fields('c')
            ->condition('regime_id', $regime_id)
            ->orderBy('upper_limit')
            ->execute();
    $catresults = Array();
    foreach ($categories as $category) {
      $cat_id = $category->ID;
      $catresults[$cat_id]['info'] = $category->description;
      $catresult = Array();
      foreach ($gender_select as $gender) {
        $sql  = 'SELECT MAX(race_laps.lap) AS laps, race_laps.entrant_id, ';
        $sql .= 'COUNT(race_laps.lap) AS lap_count, ';
        $sql .= 'MAX(race_laps.lap_time) AS last_time, ';
        $sql .= '(TIME_TO_SEC(';
        $sql .= 'TIMEDIFF(MAX(race_laps.lap_time), MAX(race_edition.start_time))';
        $sql .= ') - (MAX(race_edition.offset_start)* MAX(race_entrant.alt_start))) ';
        $sql .= 'as elapsed_time, ';
        $sql .= 'MAX(race_entrant.full_name) AS full_name, ';
        $sql .= 'MAX(race_entrant.bib) as bib ';
        $sql .= 'FROM race_laps INNER JOIN race_entrant ON ';
        $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
        $sql .= 'INNER JOIN race_category_entrant ';
        $sql .= 'ON race_entrant.ENTRANT_ID = race_category_entrant.entrant_id ';
        $sql .= 'INNER JOIN race_partcpt ';
        $sql .= 'ON race_entrant.partcpt_id = race_partcpt.ID ';
        $sql .= 'INNER JOIN race_edition ';
        $sql .= 'ON race_entrant.edition_id = race_edition.EDITION_ID ';
        $sql .= 'WHERE ((race_entrant.edition_id = '.$edition_id.' )';
        $sql .= ' AND (race_laps.lap_time < NOW() - INTERVAL :off SECOND)';
        if ($show_lap) {
          $sql .= ' AND (race_laps.lap = ' . $show_lap . ')';
        }
        $sql .= ' AND (race_laps.deleted = 0)';
        $sql .= ' AND (race_partcpt.gender = '.$c. $gender .$c .' )';
        $sql .= ' AND (race_category_entrant.category_id = :cat )';
        $sql .= ') ';
        $sql .= 'GROUP BY race_laps.entrant_id ';
        $sql .= 'ORDER BY laps DESC, elapsed_time, race_laps.lap_msec';
        $args = Array(':off' => $offset);
        $args[':eid'] = $edition_id;
        $args[':off'] = $offset;
        $args = Array(':off' => $offset);
        $args[':cat'] = $cat_id;
        $res = db_query($sql, $args);
        $gresult = Array();
        foreach ($res as $record) {
          $gresult[$record->entrant_id] = $record;
        }
        $catresult[$gender] = $gresult;
      }
      $catresults[$cat_id]['data'] = $catresult;
    }
    $results[$regime_id]['data'] = $catresults;
  }
  return $results;
}


/**
 * generate the log table
 */

function race_manager_log_table_rows($logdata = Array()) {
  $options = Array('lapdata'=>FALSE);
  global $entrants;
  $output = '
    <tr class="header-row">';
  $output .= '
     <th class="time col-time-med">Time</th>
     <th class="split col-time-small">Split</th>
     <th class="bib col-data-small">Bib</th>
     <th class="name col-full-name">Name</th>
     <th class="lap col-data-small">Lap</th>
     <th class="break col-data-small">Brk</th>
     <th class="delete col-data-small">Del</th>
     <th class="break col-data-large">Notes</th>
    </tr>
    ';
  $ctr=0;
  foreach ($logdata as $resultset) {
    $ctr++;
    $output .= '
      <tr class="';
    $class = 'oddrow';
    if ($ctr/2 == intval($ctr/2)) {
      $class = 'evenrow';        
    }
    $output .= $class . '">
    ';
    $entrant_id = $resultset['entrant_id'];
    $bib = $resultset['bib'];
    $entrant_data = $entrants[$entrant_id];
    $start_time = $entrant_data->start_time;
    $sttime = strtotime($start_time);
    $time = $resultset['lap_time'];
    $time1 = strtotime($time);
    $ntime = $time1 - $sttime;
    $ftime = race_manager_format_timestamp($ntime);
    $output .= '<td class="time text-right">' . $ftime . '</td>
    ';
    $split = $resultset['split'];
    $split = race_manager_format_timestamp($split);
    $output .= '<td class="time alignright">' . $split . '</td>
    ';
    $output .= '<td class="aligncenter">' . $bib . '</td>
    ';
    $entrant_url = race_manager_entrant_base_url($entrant_id);
    $output .= '<td class="full-name"><a href="' . $entrant_url
    . '&nav_last=1#lastlap">
    ';
    $output .= $resultset['full_name'] . ' ('. $resultset['short_name'] . ')';
    $output .= '</a></td>
    ';
    $laps = $resultset['lap'];
    $output .= '<td class="aligncenter">' . $laps . '</td>
    ';
    $output .= '<td class="aligncenter">';
    $break = $resultset['break'];
    if ($break) {
      $output .= 'X';
    }
    $output .= '</td>
    ';
    $output .= '<td class="aligncenter">';
    $deleted = $resultset['deleted'];
    if ($deleted) {
      $output .= 'X';
    }
    $output .= '</td>';
    $output .= '<td class="notes">' . $resultset['notes'] . '</td>
    ';
    $output .= '
      </tr>';
    }
  return $output;
}

/**
 * generate the log table
 */

function race_manager_log_table() {
  global $entrants;
  $datacount = intval(race_manager_entrant_record_count());
  $size = race_manager_log_size();
  $size = intval($size[1]);
  $navigation = Array();
  $navigation['id'] = 'log';
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation = race_manager_get_navigation($navigation);
  $options = Array('HIDE_DELETED_LAPS'=>TRUE);
  $options['ORDER'] = 'ASC';
  $logdata = race_manager_log(0, $navigation, $options);
  $output = '<div id="race-manager-listing">
    ';
  $output .= '<form id="race-manager-entrant-navigation" method = "POST" 
        enctype="multipart/form-data">';
  $output .= race_manager_navigation_bar($navigation);      
  $output .= '</form>
 ';
  $output .= '<table id="race-manager-log">
    <tbody>
  ';
  $output .= race_manager_log_table_rows($logdata);
  $output .= '</tbody>
  ';
  $output .= '</table>
  ';
  $output .= '</div>';
  return $output;
}

//  $offset = race_manager_option_get(8);
### Function: Return an array of the most recent laps counted
function race_manager_log($entrant_id = 0,
        $limit = Array('start' => 0, 'size' => 30),
        $options = Array()) {
//  $offset = race_manager_option_get(8);
  $offset = 0;
  $sql = 'SELECT race_laps.*, race_entrant.full_name, race_entrant.bib, ';
  $sql .= 'race_entrant.ctr_station, race_edition.race_id, ';
  $sql .= 'race_edition.race_edition, race_event.short_name, ';
  $sql .= 'race_edition.start_time ';
  $sql .= 'FROM race_laps INNER JOIN race_entrant ON ';
  $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= 'INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'INNER JOIN race_event ON ';
  $sql .= 'race_edition.race_id = race_event.RACE_ID ';
  
  
  $wheres = Array();
  $wheres[] = 'race_edition.current = 1';
  $wheres[] = 'race_laps.lap_time < NOW() - INTERVAL race_edition.offset SECOND';
  if ($entrant_id) {
    $wheres[] = 'race_laps.entrant_id = ' . $entrant_id;
  }
  $order = 'DESC';
  if (count($options)) {
    $ak = array_keys($options);
    if (in_array('station_id', $ak)) {
      $where = 'race_entrant.ctr_station = ' . chr(39);
      $where .= $options['station_id'] . chr(39);
      $wheres[] = $where;
    }
    
    if (in_array('EDITION_ID', $ak)) {
      $wheres[] = 'race_entrant.edition_id = ' . $options['EDITION_ID'];
    }
    if (in_array('HIDE_DELETED_LAPS', $ak)) {
      $wheres[] = 'race_laps.deleted = 0';
    }
    if (in_array('LAP_NUMBER', $ak)) {
      $wheres[] = 'race_laps.lap = ' . $options['LAP_NUMBER'];
    }
    if (in_array('ORDER', $ak)) {
      $order = $options['ORDER'];
    }
  }
  if (count($wheres)) {
    $where = implode(') AND (', $wheres);
    $where = 'WHERE ((' . $where . ')) ';
    $sql .= $where;
  }
  $sql .= ' ORDER BY race_laps.lap_time ';
  $sql .= $order;
  $sql .= ', LAP_ID';
  if ($limit) {
    $start = 0;
    if (is_array($limit)) {
      if (array_key_exists('start', $limit)) {
        $start = $limit['start'];
      }
      $size = intval(RACE_MANAGER_STATION_SIZE);
      if (array_key_exists('size', $limit)) {
        $size = $limit['size'];
      }
      $sql .= ' LIMIT ' . strval($start) . ', ' . strval($size);
    }
  }
  $res = db_query($sql);
  $return = Array();
  while (1) {
    $laprecord = $res->fetchAssoc();
    if (!$laprecord) {
      break;
    }
    $return[] = $laprecord;
  }
  
  return $return;
}
