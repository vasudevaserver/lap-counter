<?php
/*
Name: Race Manager Admin Functions
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
*/

/*
	Copyright 2014 Medur Wilson  (email : jpmw777@zoho.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * Main Submit handler for admin funtions
 */

function race_manager_admin_submit($form, &$form_state) {
  $form_id = $form_state['build_info']['form_id'];
  $button = $form_state['triggering_element']['#id'];
  $input = $form_state['input'];
//  drupal_set_message($button);
//  dpm($input);
  switch ($form_id) {
    case 'race_manager_admin_options_form' :
      switch ($button) {
        case 'race-manager-options-menu-setup-submit':
          race_manager_admin_menu_setup();
          drupal_set_message('Menus have been set up.');
        break;
        case 'race-manager-options-save-submit':
          $res = race_manager_admin_update_options($input);
          drupal_set_message('Options were successfuly updated.');
        break;
      }
    break;
    case 'race_manager_registration_manage_form' :
      switch ($button) {
        case 'race-manager-registration-import-submit':
          $args = Array();
          $args['edition-id'] = $input['edition-id'];
          $args['data'] = $input['data'];
          $res = race_manager_entrants_update($args);
          drupal_set_message('Entrant data was successfuly imported.');
        break;
      }
    break;
    case 'race_manager_admin_event_form' :
      $form_state['rebuild'] = TRUE;
//      $editions = race_manager_events();
      $input = $form_state['input'];
      switch ($button) {
        case 'race-manager-archive-submit':
          $save = race_manager_admin_archive_edition($form_state);
        break;
        case 'race-manager-edition-reset-submit':
          $edition_id = intval($input['selectedition']);
          if ($edition_id) {
            $reset = race_manager_edition_reset($edition_id);
          }
        break;
        case 'race-manager-event-submit':
          $save = race_manager_event_update($input);
        break;
        case 'race-manager-edition-save-submit':
          $editions = race_manager_events();
          $edition_ids = $input['edition_id'];
          foreach ($edition_ids as $key => $id) {
            $update = Array();
            $edition = $editions[$id];
            if (array_key_exists('description', $input)) {
              $value = $input['description'][$key];
              if ($value != $edition->description) {
                $update['description'] = $value;
              }
            }
            if (array_key_exists('race_edition', $input)) {
              $value = $input['race_edition'][$key];
              if ($value != $edition->race_edition) {
                $update['race_edition'] = $value;
              }
            }
            if (array_key_exists('start_time', $input)) {
              $value = $input['start_time'][$key];
              if ($value != $edition->start_time) {
                $update['start_time'] = $value;
              }
            }
            if (array_key_exists('offset', $input)) {
              $value = $input['offset'][$key];
              if ($value != $edition->offset_start) {
                $update['offset_start'] = intval($value);
              }
            }
            $value = FALSE;
            if (array_key_exists('current-' . $id, $input)) {
              $value = TRUE;
            }
            if ($value != $edition->current) {
              $update['current'] = intval($value);
            }
            if (count($update)) {
              $res = db_update('race_edition')
                      ->condition('EDITION_ID', $id, '=')
                      ->fields($update)
                      ->execute();
              if (array_key_exists('start_time', $update)) {
                race_manager_update_splits($id);
              }
            }
          }
          break;
        case 'race-manager-edition-submit':
          $edition_ids = $input['editionid'];
          $editions = race_manager_events();
          foreach ($edition_ids as $key => $id) {
            $update = Array();
            $edition = $editions[$id];
            $value = FALSE;
            if (array_key_exists('current_' . $id, $input)) {
              $value = TRUE;
            }
            if ($value != $edition->current) {
              $update['current'] = intval($value);
            }
            if ($update) {
              $res = db_update('race_edition')
                      ->condition('EDITION_ID', $id, '=')
                      ->fields($update)
                      ->execute();
            }
          }
        break;
        case 'race-manager-results-import-submit':
          $data = $input['results-data'];
          $edition_id = $input['selectedition'];
          if ($edition_id) {
            race_manager_import_results($edition_id, $data);
          }
        break;
      }
      break;
    case 'race_manager_admin_start_form' :
      switch ($button) {
        case 'race-manager-start-submit':
          $start_time = strftime("%Y-%m-%d %X", time() +
                  RACE_MANAGER_TZ_CORRECTION);
          $editions = race_manager_events();
          $edition_ids = $input['edition_id'];
          foreach ($edition_ids as $key => $id) {
            $edition = $editions[$id];
            $start = FALSE;
            if (array_key_exists('start-' . $id, $input)) {
              $start = TRUE;
            }
            if ($start) {
              $update = Array();
              $update['start_time'] = $start_time;
              $update['status'] = 1;
              $res = db_update('race_edition')
                      ->condition('EDITION_ID', $id, '=')
                      ->fields($update)
                      ->execute();
            }
          }
          break;
      }
      break;
    case 'race_manager_registration_short_form' :
      $form_state['rebuild'] = TRUE;
      switch ($button) {
        case 'race-manager-registration-cancel-submit':
          $form_state['rebuild'] = FALSE;
        break;
        case 'race-manager-registration-choose-submit':
          // Nothing to do: the form api fills in the fields
        break;
        case 'race-manager-registration-save-submit':
          $args = Array('form'=>$form, 'form_state'=>$form_state);
          $args['reg-type'] = 'short';
          $res = race_manager_registration_submit($args, 'short');
          $form_state['rebuild'] = FALSE;
        break;
      }
      break;
    case 'race_manager_registration_clear_form' :
      $form_state['rebuild'] = FALSE;
      $form_state['redirect'] = 'race-admin/registration';
      switch ($button) {
        case 'race-manager-registration-cancel-submit':
        break;
        case 'race-manager-registration-clear-submit':
          $edition_id = $_SESSION['#%edition_id'];
          unset($_SESSION['#%edition_id']);
          race_manager_registration_clear($edition_id);
        break;
      }
      break;
    case 'race_manager_registration_form' :
      $url = 'race-admin/registration';
      $form_state['rebuild'] = TRUE;
      $args = Array();
      switch ($button) {
        case 'race-manager-registration-choose-submit':
          $args = Array();
        break;
        case 'race-manager-registration-clear-submit':
          $form_state['rebuild'] = FALSE;
          $form_state['redirect'] = $url;
          $_SESSION['#%edition_id'] = $input['select_edition'];
          $form_state['%value'] = $form_state['input']['select_edition'];
        break;
        case 'race-manager-registration-save-submit':
          // This causes the form to reset back to blank
          // after saving the data
          $args = Array('form'=>$form, 'form_state'=>$form_state);
          $res = race_manager_registration_submit($args);
          $form_state['rebuild'] = FALSE;
          $form_state['redirect'] = $url;
        break;
        case 'race-manager-registration-cancel-submit':
          // This causes the form to reset back to blank
          $choose = FALSE;
          if (isset($input['op'])) {
            if ($input['op']=='Lookup Entrant') {
              $choose = TRUE;
            }
          }
          if (!$choose) {
            $form_state['rebuild'] = FALSE;
            $form_state['redirect'] = $url;
          }
        break;
        case 'race-manager-registration-save-submit':
          $args = Array('form'=>$form, 'form_state'=>$form_state);
          $res = race_manager_registration_submit($args);
        break;
        case 'race-manager-registration-import-submit':
          $args['edition-id'] = $form_state['values']['select_edition'];
          $args['data'] = $form_state['values']['data'];
          $res = race_manager_registration_import_submit($args);
        break;
        case 'race-manager-registration-rollover-submit':
          $args['target'] = $form_state['values']['select_edition'];
          $args['source'] = $form_state['values']['source'];
          $res = race_manager_registration_rollover_submit($args);
        break;
      }
      break;
    case 'race_manager_admin_entrant_form' :
      $form_state['rebuild'] = TRUE;
      switch ($button) {
        case 'race-manager-entrants-recalculate-ages-submit':
          $args = $form_state['input'];
          $args['categories'] = 1;
          $res = race_manager_entrant_recalculate_ages($args);
          drupal_set_message('Entrant ages were successfuly updated.');
        break;
        case 'race-manager-entrant-insert-first-lap-submit':
          $args = Array();
          $args['edition-id'] = $form_state['values']['first-lap-edition-id'];
          $args['average'] = $form_state['values']['average'];
          $res = race_manager_entrants_insert_first_lap($args);
        break;
        case 'race-manager-entrant-station-select-submit':
          $args = Array();
          $args['station-id'] = $form_state['values']['station-id'];
          $args['entrants'] = $form_state['values']['entrants'];
          $res = race_manager_station_assign($args);
        break;
      }
      break;
    case 'race_manager_admin_edition_add_form' :
      switch ($button) {
        case 'race-manager-edition-add-submit':
          $default_date = strftime("%Y-%m-%d %X", 0);
          $event_id = $form_state['values']['event_id'];
          $form_state['redirect'] = 'race-admin/event';
          $form_state['values']['select'] = $event_id;
          $args = Array();
          $args['RACE_ID'] = $event_id;
          $description = ($_REQUEST['description']) ?
          $_REQUEST['description'] : '' ;
          $args['description'] = $description;
          $value = ($_REQUEST['race_edition']) ?
          $_REQUEST['race_edition'] : 0 ;
          $args['race_edition'] = $value;

          $value = ($_REQUEST['offset_start']) ?
          $_REQUEST['offset_start'] :  0;
          $args['offset_start'] = $value;

          $value = ($_REQUEST['current']) ?
          $_REQUEST['current'] :  1;
          $args['current'] = $value;

          $value = ($_REQUEST['start_time']) ?
          $_REQUEST['start_time'] :  $default_date;
          
          $args['start_time'] = $value;
          $args['race_date'] = $value;
          $value = ($_REQUEST['finish_time']) ?
          $_REQUEST['finish_time'] :  $default_date;
          $args['finish_time'] = $value;
          $result = race_manager_edition_add($args);
          drupal_set_message('Edition: ' . $description . ' was added.');
        break;
      }
      break;
    case 'race_manager_admin_event_add_form' :
      switch ($button) {
        case 'race-manager-event-add-submit':
          $event_id = $form_state['values']['event_id'];
          $form_state['redirect'] = 'race-admin/event';
          $form_state['input']['select'] = $event_id;
          $args = Array();
          $args['RACE_ID'] = $event_id;
          $args['name'] = $form_state['values']['name'];
          $args['short_name'] = $form_state['values']['short_name'];
          $value = ($form_state['values']['lap_distance']) ?
          $form_state['values']['lap_distance'] : 0 ;
          $args['lap_distance'] = $value;
          $value = ($form_state['values']['reject_limit']) ?
          $form_state['values']['reject_limit'] : 0 ;
          $args['reject_limit'] = $value;
          $result = race_manager_event_add($args);
        break;
      }
      break;
  }
  return;
}

// Update the current edition start time from remote timer
function race_manager_admin_update_start_time($offset, $id) {
  $time = time() + RACE_MANAGER_TZ_CORRECTION;
  $offset = intval($offset);
  $time -= $offset;
  $timeval = strftime("%Y-%m-%d %X", $time);
  $values = Array();
  $rec = explode('-', $id);
  if (count($rec) == 0) {
    break;
  }
  $recid = trim($rec[0]);
  $alt = 1;
  if (count($rec) > 1) {
    $alt = intval(trim($rec[1]));
  }
  $res = db_select('race_edition', 'e')
          ->fields('e', Array('start_time', 'status', 'offset_start'))
          ->condition('EDITION_ID', $recid)
          ->execute();
  $rec = $res->fetchAssoc();
  $status = intval($rec['status']);
  $offset_start = intval($rec['offset_start']);
  $starttime = $rec['start_time'];
  $status1 = ($status & 1);
  $status2 = ($status & 2);
  if ($alt == 1) {
    if (!$status1) {
      // Do not allow restart of race
      $values['start_time'] = $timeval;
      $status += 1;
      if ($status2) {
        // !!! ??? the second race has already started !!!
        
      }
    }
  }
  else {
    if (!($status2)) {
      $status +=2;
    }
    $starttime = strtotime($starttime);
    $offset = $time - $starttime;
    $values['offset_start'] = $offset;
  }
  $values['status'] = $status;
  $res = db_update('race_edition')
          ->fields($values)
          ->condition('EDITION_ID', $recid)
          ->execute();
}

function race_manager_admin() {
  $output = '$menu_admin_html';
  return $output;
}

function race_manager_admin_system_options() {
  $adminform = drupal_get_form('race_manager_admin_options_form');
  return drupal_render($adminform);
}

function race_manager_admin_chute() {
  $adminform = drupal_get_form('race_manager_admin_chute_form');
  return drupal_render($adminform);
}

function race_manager_admin_registration_clear_confirm() {
  $adminform = drupal_get_form('race_manager_registration_clear_form');
  return drupal_render($adminform);
}

function race_manager_admin_entrant() {
  $adminform = drupal_get_form('race_manager_admin_entrant_form');
  return drupal_render($adminform);
}

function race_manager_admin_event() {
  $adminform = drupal_get_form('race_manager_admin_event_form');
  return drupal_render($adminform);
}

function race_manager_admin_event_add() {
  $adminform = drupal_get_form('race_manager_admin_event_add_form');
  return drupal_render($adminform);
}

function race_manager_admin_edition_add() {
  $adminform = drupal_get_form('race_manager_admin_edition_add_form');
  return drupal_render($adminform);
}

function race_manager_admin_registration($type='long') {
  switch ($type) {
    case 'manage':
      $adminform = drupal_get_form('race_manager_registration_manage_form');
      break;
    case 'short':
      $adminform = drupal_get_form('race_manager_registration_short_form');
      break;
    default :
      $adminform = drupal_get_form('race_manager_registration_form');
      break;
  }
  return drupal_render($adminform);
}

function race_manager_admin_start_timer() {
  $adminform = drupal_get_form('race_manager_admin_start_timer_form');
  return drupal_render($adminform);
}

// Update the event data as provided.
function race_manager_admin_archive_edition($form_state) {
  $input = $form_state['input'];
  $eid = intval($input['selectedition']);
  if (!$eid) {
    return;
  }
  $sql = 'SELECT race_laps.entrant_id, race_entrant.edition_id, race_laps.bib, ';
  $sql .= 'race_laps.lap, race_partcpt.gender, ';
  $sql .= 'race_entrant.current_age AS age, race_entrant.full_name AS name, ';
  $sql .= 'race_laps.station_id, race_laps.lap_msec, ';
  $sql .= 'race_laps.last_lap_id, race_laps.break, race_laps.notes, ';
  $sql .= 'race_entrant.alt_start, race_edition.offset_start, ';
  $sql .= 'race_edition.start_time, race_laps.lap_time,';
  $sql .= '(TIME_TO_SEC(TIMEDIFF(race_laps.lap_time, race_edition.start_time))'; 
  $sql .= '- (race_edition.offset_start * race_entrant.alt_start)) as ssplit ';
  $sql .= 'FROM race_laps ';
  $sql .= 'INNER JOIN race_entrant ';
  $sql .= 'ON race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= 'INNER JOIN race_partcpt ';
  $sql .= 'ON race_entrant.partcpt_id = race_partcpt.ID ';
  $sql .= 'INNER JOIN race_edition ';
  $sql .= 'ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'WHERE ((race_entrant.edition_id = ' . $eid ;
  $sql .= ') AND (race_laps.deleted = 0))';
  $sql .= ' ORDER BY ssplit, race_laps.lap_msec';
  $query = db_query($sql);
  $place = 0;
  $mplace = 0;
  $gplace = 0;
  $fplace = 0;
  while (TRUE) {
    if ($gplace == 30) {
      $t = 1;  // Stop the code here for debugging
    }
    $fields = $query->fetchAssoc();
    if (!$fields) break;
    $res = db_select('race_archive', 'a')
            ->fields('a', array('entrant_id', 'lap'))
            ->condition('entrant_id', $fields['entrant_id'])
            ->condition('lap', $fields['lap'])
            ->execute();
    $record = FALSE;
    foreach ($res as $record) {
      break;
    }
    if ($record) continue;
    if ($fields['gender'] == 'F') {
      $fplace += 1;
      $gplace = $fplace;
    }
    else {
      $mplace += 1;
      $gplace = $mplace;
    }
    $place += 1;
    $fields['place'] = $place;
    $fields['gplace'] = $gplace;
    $offset = intval($fields['offset_start']);
    unset($fields['offset_start']);
    $start_time = $fields['start_time'];
    $sstime = strtotime($start_time);
    unset($fields['start_time']);
    $use_offset = intval($fields['alt_start']);
    unset($fields['alt_start']);
    $split = intval($fields['ssplit']);
    unset($fields['ssplit']);
    $fields['split'] = $split;
//    return;

    $res = db_insert('race_archive')
            ->fields($fields)
            ->execute();
  }
}

// Update the event data as provided.
function race_manager_event_update($input) {
  $fields = Array();
  $id = intval($input['id']);
  if ($id) {
    $fields['name'] = $input['name'];
    $fields['short_name'] = $input['short_name'];
    $fields['lap_distance'] = intval($input['lap_distance']);
    $fields['reject_limit'] = intval($input['reject_limit']);
    $res = db_update('race_event')
            ->fields($fields)
            ->condition('RACE_ID', $id)
            ->execute();
  }
}

// Build a table of current editions for direct actions
function race_manager_edition_start_timer_table() {
  $editions = race_manager_current_events();
  $gstarttime = race_manager_current_event_start_time();
  $ctime = time() + RACE_MANAGER_TZ_CORRECTION;
  $ctr = 0;
  $html = '<tr>';
  foreach ($editions as $edition) {
    $id = $edition->EDITION_ID;
    $start_time = $edition->start_time;
    $sstime = strtotime($start_time);
    $html .= '<td class="name">';
    $html .= '<input type="hidden" name="edition_id[]"';
    $html .= ' value="' . $id . '">';
    $html .= $edition->description;
    $html .= '</td>';
//    $html .= '<td class="time">';
//    $html .= $edition->start_time;
//    $html .= '</td>';
    $html .= '<td class="start">';
    $status = $edition->status;
    if (!($status & 1)) {
      $html .= '<span class="button" id="start1c-' . $ctr . '">';
      $html .= '<input type="checkbox" id="choose1-' . $ctr . '" ';
      $html .= '>';
      $html .= '<input value="start" type="hidden" id="start1-' . $ctr . '">';
      $html .= '</input>';
      $html .= '<input value="0" type="hidden" id="starttime1-' . $ctr . '">';
      $html .= '</input></span>';
      $html .= '<span  class="timer" id="timer1-' . $ctr . '" ></span>';
      $html .= '<span class="button"><input value="reset" type="button" id="reset1-' . $ctr . '" ';
      $html .= 'onclick="ResetTimer(' . $ctr . ',1);">';
      $html .= '</input></span>';
      $html .= '<span class="button"><input value="send" type="button" id="send1-' . $ctr . '" ';
      $html .= 'onclick="SendTimer(' . $ctr . ', 1, '. $id .');">';
      $html .= '</input></span>';
    }
    else {
      $html .= '<span class="button" id="start1c-' . $ctr . '">';
      $html .= '<input value="started" type="hidden" id="start1-' . $ctr . '">';
      $html .= '</input>';
      $html .= '<input value="0" type="hidden" id="starttime1-' . $ctr . '">';
      $html .= '</input></span>';
      $html .= '<span  class="timer" id="timer1-' . $ctr . '" ></span>';
        // race has started
      $html .= '<script type="text/javascript"> 
        RaceStarted('. ($ctime - $sstime) . ',1 ,' . $ctr . ');</script>';
    }
    $html .= '</td>';
    $html .= '</tr>';
    $offset = intval($edition->offset_start);
    if ($offset) {
      $sstime += $offset;
      $html .= '<tr>';
      $html .= '<td class="name">';
      $html .= $edition->description . ' second start';
      $html .= '</td>';
//      $html .= '<td class="time">';
//      $html .= strftime("%Y-%m-%d %X", $sstime);
//      $html .= '</td>';
      $html .= '<td class="start">';
      if (!($status & 2)) {
        $html .= '<span class="button" id="start2c-' . $ctr . '">';
        $html .= '<input value="start" type="checkbox" id="choose2-' . $ctr . '" ';
        $html .= '>';
        $html .= '<input value="start" type="hidden" id="start2-' . $ctr . '">';
        $html .= '</input>';
        $html .= '<input value="0" type="hidden" id="starttime2-' . $ctr . '">';
        $html .= '</input></span>';
        $html .= '<span  class="timer" id="timer2-' . $ctr . '" ></span>';
        $html .= '<span class="button"><input value="reset" type="button" id="reset2-' . $ctr . '" ';
        $html .= 'onclick="ResetTimer(' . $ctr . ',2);">';
        $html .= '</input></span>';
        $html .= '<span class="button"><input value="send" type="button" id="send2-' . $ctr . '" ';
        $html .= 'onclick="SendTimer(' . $ctr . ', 2, '. $id .');">';
        $html .= '</input></span>';
      }
      else {
        $html .= '<span class="button" id="start2c-' . $ctr . '">';
        $html .= '<input value="started" type="hidden" id="start2-' . $ctr . '">';
        $html .= '</input>';
        $html .= '<input value="0" type="hidden" id="starttime2-' . $ctr . '">';
        $html .= '</input></span>';
        $html .= '<span  class="timer" id="timer2-' . $ctr . '" ></span>';
        $html .= '<script type="text/javascript"> 
         RaceStarted('. ($ctime - $sstime) . ',2 ,' . $ctr . ');</script>';
      }
      $html .= '</td>';
      $html .= '</tr>';
    }
    $ctr += 1;
  }
  return $html;
}

// Build a table of current editions for direct actions
function race_manager_current_editions_table() {
  $editions = race_manager_current_events();
  $sql = 'SELECT COUNT(entrant_id) AS total, MAX(edition_id) AS edid ';
  $sql .= 'FROM race_entrant GROUP BY edition_id ';
  $sql .= 'HAVING edition_id = :edid';
  $html = '';
  foreach ($editions as $edition) {
    $id = $edition->EDITION_ID;
    $html .= '<input type="hidden" name="editionid[]"';
    $html .= ' value="' . $id . '">';
    $html .= '<tr>';
    $html .= '<td class="name">';
    $html .= $edition->description;
    $html .= '</td>';
    $html .= '<td class="time">';
    $html .= $edition->start_time;
    $html .= '</td>';
    $html .= '<td>';
    $count = db_query($sql, Array(':edid' => $id));
    $count = $count->fetchAssoc();
    $total = 0;
    if ($count) {
      $total = $count['total'];
    }
    $html .= strval($total);
    $html .= '</td>';
    $html .= '<td>';
    if ($edition->ecount) {
      $html .= $edition->ecount;
    }
    else {
      $html .= '0';
    }
    $html .= '</td>';
    $html .= '<td><span>';
    $html .= '<input type="checkbox"  name="current_' . $id .'"';
    if (intval($edition->current) == 1) {
      $html .= 'checked ';
    }
    $html .= '/></span>';
    $html .= '</td>';
    $html .= '</tr>';
  }
  return $html;
}

// Build a table of editions for a particular event
function race_manager_editions_admin_table($id = '') {
  $editions = '';
  $sql = 'SELECT count(*) as total FROM race_entrant ';
  $sql .= 'WHERE edition_id = :id';
  $sql .= ' group by edition_id';
  $options = Array();
  $options['current'] = FALSE;
  $options['race_id'] = $id;
  $options['entrant_count'] = TRUE;
  $options['archive_count'] = TRUE;
  $options['sort'] = 'race_edition.start_time DESC';
  $events = race_manager_events($options);
  foreach ($events as $edition) {
    $id = $edition->EDITION_ID;
    $editions .= '<tr>
      ';
    $input = '<input type="hidden" name="edition_id[]"';
    $input .= ' value="' . $id . '">';
    $editions .= $input;
    $editions .= '<td>';
    $editions .= $edition->EDITION_ID;
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="description[]"';
    $editions .= ' class="large" value="' . $edition->description . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="race_edition[]"';
    $editions .= ' class="small" value="' . $edition->race_edition . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="start_time[]"';
    $editions .= ' class="time" value="' . $edition->start_time . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="offset[]"';
    $editions .= ' class="small" value="' . $edition->offset_start . '" />';
    $editions .= '</td>';
    $editions .= '<td><div>';
    $count = db_query($sql, Array(':id' => $id));
    $count = $count->fetchAssoc();
    $total = 0;
    if ($count) {
      $total = $count['total'];
    }
    $editions .= strval($total);
    $editions .= '</div></td>';
    $editions .= '<td><div>';
    if ($edition->ecount) {
      $editions .= $edition->ecount;
    }
    else {
      $editions .= '0';
    }
    $editions .= '</div></td>';
    $editions .= '<td><div>';
    if ($edition->acount) {
      $editions .= $edition->acount;
    }
    else {
      $editions .= '0';
    }
    $editions .= '</div></td>';
    $editions .= '<td><div>';
    
    $editions .= '<input type="checkbox"  name="current-' . $id .'"';
    if (intval($edition->current) == 1) {
      $editions .= 'checked ';
    }
    $editions .= '/>';
    $editions .= '</div></td>';
    $editions .= '<td><div>';
    $editions .= '<input type="radio"  name="selectedition" value="'.$id . '"';
    $editions .= '/>';
    $editions .= '</div></td>';
    $editions .= '</tr>';
  }
  return $editions;
}


// Build a table of system options
function race_manager_admin_options_table($id = '') {
  $editions = '';
  $options = db_select('race_options','o')
          ->fields('o')
          ->execute();
  $html = '';
  foreach ($options as $option) {
    $id = $option->option_id;
    $name = $option->option_name;
    $value = $option->option_value;
    $html .= '<tr>
      ';
    $input = '<input type="hidden" name="option_id[]"';
    $input .= ' value="' . $id . '">';
    $html .= $input;
    $html .= '<td>';
    $html .= '<input type="text"  name="edit_option_id[]"';
    $html .= ' class="small" value="' . $id . '" />';
    $html .= '</td>';
    $html .= '<td>';
    $html .= '<input type="text"  name="edit_option_name[]"';
    $html .= ' class="larger" value="' . $name . '" />';
    $html .= '</td>';
    $html .= '<td>';
    $html .= '<input type="text"  name="edit_option_value[]"';
    $html .= ' class="larger" value="' . $value . '" />';
    $html .= '</td>';
    $html .= '</tr>';
  }
  return $html;
}


// Submit handler for the race_manager_admin_options_table
function race_manager_admin_update_options($args = Array()) {
  if (!isset($args['option_id'])) {
    return;
  }
  $option_ids = $args['option_id'];
  $edit_option_ids = $args['edit_option_id'];
  $edit_option_names = $args['edit_option_name'];
  $edit_option_values = $args['edit_option_value'];
  $options = db_select('race_options', 'o')
          ->fields('o')
          ->execute();
  $opt = Array();
  foreach ($options as $option) {
    $opt[$option->option_id] = $option; 
  }
  $count = count($option_ids);
  for ($i = 0; $i < $count;$i++) {
    $option_id = $option_ids[$i];
    $option = $opt[$option_id];
    $newid = $edit_option_ids[$i];
    $newname = $edit_option_names[$i];
    $newvalue = $edit_option_values[$i];
    $update = Array();
    if ($newid != $option_id) {
      $update['option_id'] = $newid;
    }
    if ($newname != $option->option_name) {
      $update['option_name'] = $newname;
    }
    if ($newvalue != $option->option_value) {
      $update['option_value'] = $newvalue;
    }
    if ($update) {
      $res =  db_update('race_options')
              ->fields($update)
              ->condition('option_id', $option_id)
              ->execute();
    }
  }
}

// Submit handler for the race_manager_admin_options_table
function race_manager_admin_menu_setup() {
  $file = race_manager_module_location() . '/race_manager_config_install.php';
  require_once $file;

  $menu_items = race_manager_config_install_menus();
  $menus = $menu_items['menus'];
  
  foreach ($menus as $menu) {
    $result = menu_delete($menu);
  }

  foreach ($menus as $menu) {
    $result = menu_save($menu);
  }
  $items = $menu_items['items'];
  foreach ($items as $item) {
    menu_link_save($item);
  }
}