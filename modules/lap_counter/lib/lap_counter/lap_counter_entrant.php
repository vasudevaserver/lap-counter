<?php
/*
Name: Lap Counter
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2012  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


 /**
 * Return a list of counter stations as a select menu
 */
function lap_counter_entrant_station_selection($options=Array()) {
    $races = lap_counter_current_events();
    $stations = lap_counter_counter_stations();
    $output = '<h6 id="lap_counter-station-select-menu">';
    $output = 'Show Runners for: Race: ';
    $race_array = Array();
    $qstring = $_SERVER['QUERY_STRING'];
    $base_url = $_SERVER['REDIRECT_URL'];
    $qstr = Array();
    parse_str($qstring, $qstr);
    unset($qstr['station-id']);
    foreach ($races as $edition_id => $race) {
      $qstr['edition-id'] = $edition_id;
      $qstring = http_build_query($qstr);
      $href = $base_url . '?' . $qstring;
      $output .= ' <a href="' . $href .'">' . $race->short_name . '</a>';
    }
    unset($qstr['edition-id']);
    $output .= ' Station: ';
    foreach ($stations as $station) {
      $sid = $station->ID;
      $qstr['station-id'] = $sid;
      $qstring = http_build_query($qstr);
      $href = $base_url . '?' . $qstring;
      $output .= ' <a href="' . $href .'">' . $sid . '</a>';
    }
    $output .= '</h6>';
    return $output;
}
  
 /**
 * Return a log of laps filtered by counter station
 */
function lap_counter_entrant_station_log_table($options=Array()) {
  // $postresults = lap_counter_handle_post_submit();
  $datacount = intval(lap_counter_entrant_record_count());
  $size = lap_counter_station_size();
  $size = intval($size[1]);
  $navigation = Array();
  $navigation['id'] = 'log';
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation = lap_counter_get_navigation($navigation);
  $options['HIDE_DELETED_LAPS'] = TRUE;
  $logdata = lap_counter_log(0, $navigation, $options);
  $output = '<div id="lap_counter-listing">
    ';
  $output .= lap_counter_entrant_station_selection();

  $output .= '<table id="lap_counter-station-log">
    <tbody>
  ';
  $output .= lap_counter_log_table_rows($logdata);
  $output .= '</tbody>
  ';
  $output .= '</table>
  ';
  $output .= '</div>';
  return $output;
}
  
 /**
 * Return a list of all current entrants keyed on bib number
 */
function lap_counter_entrants_current($options=Array()) {
  $showlapdata = FALSE;
  if (array_key_exists('lapdata', $options)) {
    $showlapdata = $options['lapdata'];
  }
  $entrant_id = 0;
  if (array_key_exists('entrant_id', $options)) {
    $entrant_id = $options['entrant_id'];
  }
  $edition_id = 0;
  if (array_key_exists('edition_id', $options)) {
    $edition_id = $options['edition_id'];
  }
  $sql = 'SELECT race_entrant.ENTRANT_ID, race_entrant.partcpt_id, ';
  $sql .= 'race_entrant.name_code, race_entrant.bib, race_entrant.ctr_station,';
  $sql .= 'race_entrant.full_name, race_entrant.current_bio, ';
  $sql .= 'race_entrant.current_age, race_entrant.city, ';
  $sql .= 'race_entrant.state, race_entrant.country, ';
  $sql .= 'gender, race_event.name as race_name, ';
  $sql .= 'race_event.lap_distance, race_event.short_name, ';
  $sql .= 'race_event.reject_limit, ';
  $sql .= 'race_edition.EDITION_ID, race_edition.race_id, ';
  $sql .= 'race_edition.race_edition, '; 
  $sql .= 'race_edition.finish_time, race_edition.start_time ';
  if ($showlapdata) {
    $sql .= ', MAX(race_laps.lap) AS lap ';
  }
  $sql .= 'FROM race_entrant INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'INNER JOIN race_partcpt ON race_entrant.partcpt_id = race_partcpt.ID ';
  $sql .= 'INNER JOIN race_event ON race_edition.race_id = race_event.RACE_ID ';
  if ($showlapdata) {
    $sql .= 'LEFT JOIN race_laps ';
    $sql .= 'ON race_entrant.ENTRANT_ID = race_laps.entrant_id ';
  }
  $sql .= 'WHERE ((race_edition.current = 1)';
  if ($showlapdata) {
    $sql .= ' AND (race_laps.deleted = 0)';
  }
  if ($edition_id) {
    $sql .= ' AND (race_entrant.edition_id = ' . $edition_id . ')';
  }
  if ($entrant_id) {
    $sql .= ' AND (race_entrant.ENTRANT_ID = ' . $entrant_id .')';
  }
  $sql .= ')';
  if (!$entrant_id) {
    $sql .= ' GROUP BY race_entrant.ENTRANT_ID';
    $sql .= ' ORDER BY bib';
  }
  
  $res = db_query($sql);
  $return = Array();
  foreach ($res AS $record) {
    $entrant_id = intval($record->ENTRANT_ID);
    $return[$entrant_id] = $record;
  }
  return $return;
}

/* Return a lap listing for a race entrant
 */
function lap_counter_entrant_table($entrant=NULL) {
  if (!$entrant) {
    return 'No Entrant Selected';
  }
  $lapeditpermssion = user_access('add edit delete laps');
  $entrant_id = $entrant->ENTRANT_ID;
  $datacount = intval(lap_counter_entrant_record_count($entrant_id));
  // retrieve the batch size for the entrant table
  $size = lap_counter_entrant_size();
  $size = intval($size[1]);
  $navigation = Array('id' => 'entrant');
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation['target'] = lap_counter_entrant_base_path($entrant_id);
  $navigation['entrant-id'] = $entrant_id;
  $navigation = lap_counter_get_navigation($navigation);
  $navbar = lap_counter_navigation_bar($navigation);

  // load the form here to process any lap edit requests
  $form_edit_lap = FALSE;
  if ($lapeditpermssion) {
    $form_edit_lap = drupal_get_form('lap_counter_edit_lap_form');
  }
  $lapdata = lap_counter_entrant_laps($entrant_id, $navigation, 'time');
  $entrant_name = $entrant->full_name;
  $bib = $entrant->bib;
  $output = '<div id="lap_counter-listing">
    ';
  $output .= '<table id="lap_counter-subheader">
    <tr><td>
    ';
      $output .= $navbar;
    $output .= '
      </td><td>
 ';
    // v 7.x-0.18
    if ($lapeditpermssion) {
      $output .= '<div id="lapcounter-subtitle" >
         Insert Lap
        </div>';
      $output .= '
      </td><td>
     ';
      $path = trim(LAP_COUNTER_BASE_PATH);
      if ($path == '/') {
        $path = '';
      }
      $output .= '<form id="lap_counter-insert-lap" method = "POST" 
        enctype="multipart/form-data" action="' . $path . '/insert-lap">';
      $output .= '<input type="submit" value="Insert Lap"/>';
      $output .= '<input name="entrant-id" type="hidden" 
                  value="' . $entrant_id . '"/>';
      $output .= '</form>
    ';
    }
    $output .= '
      </td></tr></table>
 ';
      
  $output .= '<table id="lap_counter-entrant">
 ';
      $output .= '
        <tr class="header-row">
        <th class="lap col-data-small aligncenter">Lap</th>
        <th class="lap col-data-small aligncenter">Km</th>
        <th class="lap col-data-small aligncenter">Mi</th>
        <th class="time col-time-med">Time</th>
        <th class="split col-time-small">Split</th>
        <th class="break col-data-small aligncenter">Brk</th>
        <th class="delete col-data-small aligncenter">Del</th>
     ';
     if ($lapeditpermssion) {
       $output .= '
        <th class="break col-data-med aligncenter">Break  Delete</th>
       ';
     }
      $output .= '
        <th class="notes col-data-large">Notes</th>
        </tr>
        ';
    $ctr = 0;
    $start_time = $entrant->start_time;
    $sttime = strtotime($start_time);
    foreach ($lapdata as $laprecord) {
      $ctr++;
      $last = FALSE;
      if ($ctr == $count) {
         $last = TRUE;
      }
      $split = $laprecord['split'];
      $deleted = $laprecord['deleted'];

      $output .= '
        <tr';
      $class = Array();
      if ($ctr/2 == intval($ctr/2)) {
        $class[] = 'evenrow';
      }
      else {
        $class[] = 'oddrow';
      }
      if (!$deleted) {
        $warning = intval(lap_counter_warning_threshold());
        if ($split < $avgsplit * (1 - $warning/100)) {
          $class[] = 'warning';
        }
      }
      if ($class) {
        $class = implode(' ', $class);
        $class = ' class="' . $class .'"';
        $output .= $class;
      }
      $output .= '>
        ';
      $laps = $laprecord['lap'];
      $lap_id = $laprecord['LAP_ID'];
      $output .= '<td class="aligncenter">';
      if ($last) {
        $output .= '<a name="lastlap"></a>';
      }
      $output .= $laps . '</td>
        ';
      $value = intval($laps)*.4; // Km
      $output .= '<td class="aligncenter">';
      $output .= $value . '</td>
        ';
      $value = intval(intval($laps)*2.48548)/10; // Mi
      $output .= '<td class="aligncenter">';
      $output .= $value . '</td>
        ';
      $time = $laprecord['lap_time'];
      $time1 = strtotime($time);
      $ntime = $time1 - $sttime;
      $ftime = lap_counter_format_timestamp($ntime);
      $output .= '<td class="time alignright">' . $ftime . '</td>
        ';
      $splittime = lap_counter_format_timestamp($split);
      $output .= '<td class="split time alignright">' . $splittime . '</td>
        ';
      $output .= '<td class="aligncenter">';
      $break = $laprecord['break'];
      if ($break) {
        $output .= 'X';
      }
      $output .= '</td>
        ';
 
      $output .= '<td class="aligncenter">';
      $deleted = $laprecord['deleted'];
      if ($deleted) {
        $output .= 'X';
      }
      if ($lapeditpermssion) {
        $output .= '</td>
        <td>';
        $form_edit_lap = drupal_get_form('lap_counter_edit_lap_form');
        $form_edit_lap['entrant-id']['#value'] = $entrant_id;
        $form_edit_lap['entrant-bib']['#value'] = $bib;
        $form_edit_lap['lap-id']['#value'] = $lap_id;
        $form_edit_lap['nav-last']['#value'] = '';
        $action = lap_counter_entrant_base_url($entrant_id);
        $action .='&nav-start=' . $navigation['start'];
        $form_edit_lap['#action'] = $action;
        $form_edit_lap['nav-start']['#value'] = $navigation['start'];
        if ($break) {
          $value = 'REMOVE';
        }
        else {
          $value = 'BREAK';
        }
        $form_edit_lap['submit_break']['#value'] = $value;
        if ($deleted) {
          $value = 'UNDELETE';
        }
        else {
          $value = 'DELETE';
        }
        $form_edit_lap['submit_delete']['#value'] = $value;
        $content = drupal_render($form_edit_lap);
        $output .= $content;
      }
      $output .= '</td>
        <td class="notes">' . $laprecord['notes'] .'</td>
        ';
      $output .= '</tr>';
    }
    $output .= '</table>
      ';
    $output .= '<form id="lap_counter-entrant-navigation" method = "POST" 
        enctype="multipart/form-data">';
      $output .= $navbar;
      $output .= '</form>
 ';
  return $output;
}

/* return a table listing the current entrants 
 * sorted by bib number
 */
 
function lap_counter_entrants_table() {
  global $entrants;
  $base_url = lap_counter_entrant_base_url();
  $options = Array('lapdata'=>TRUE);
  $entrants = lap_counter_entrants_current($options);
  $output = '<div id="lap_counter-listing">
    ';
   $output .= '<table id="lap_counter-entrants">
 ';
  $output .= '<tr class="header-row">
        <th class="aligncenter bib">Bib</th>
        <th class="full_name">Name</th>
        <th class="race_name">Race</th>
        <th class="aligncenter laps">Laps</th>
        </tr>
        ';
  $ctr = 0;
  foreach ($entrants as $entrant) {
    $ctr++;
    $entrant_id = $entrant->ENTRANT_ID;
    $lap = $entrant->lap;
//    $lap = lap_counter_entrant_total($entrant);
//    $lap = 0;
    $race_name = $entrant->race_name;
    $bib = $entrant->bib;
    $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
    $output .= '<td class="aligncenter">' . $bib . '</td>';
    $output .= '<td><a href="'. lap_counter_entrant_base_url($entrant_id);
    $output .= '&nav_last=1#lastlap">
      ';
    $output .= $entrant->full_name . '</a></td>
      ';
    $output .= '<td>' . $race_name . '</td>
      ';
    $output .= '<td class="aligncenter">' . $lap . '</td>
      </tr>';
  }
  $output .= '</table>
  ';
  $output .= '</div>
  ';
  return $output;
}

# Return race data for an entrant
function lap_counter_entrant_info($options) {
  $entrants = lap_counter_entrants_current($options);
  $entrant = NULL;
  foreach ($entrants as $entrant) {
    break;
  }
  return $entrant;
}

### Function: Return the total laps for an entrant
### DEPRECATED: now included in lap_counter_entrants_current

### Function: Return the total number of lap records for this entrant
function lapcounter_record_count($entrant_id = 0) {
  global $wpdb;
  $sql = 'SELECT COUNT(LAP_ID) AS lap_count FROM race_laps ';
  if ($entrant_id) {
    $sql .= ' WHERE entrant_id = ' . $entrant_id;
    $sql .= ' GROUP BY entrant_id';
  }
  else {
    $sql .= 'INNER JOIN race_entrant ON ';
    $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
    $sql .= 'INNER JOIN race_edition ON ';
    $sql .= 'race_entrant.race_id = race_edition.race_id AND ';
    $sql .= 'race_entrant.race_edition = race_edition.RACE_EDITION ';
    $sql .= ' WHERE current = 1';
  }
  $q = $wpdb->prepare($sql);
  $res = $wpdb->get_results($q, ARRAY_A);
  return intval($res[0]['lap_count']);
  return $sql;
}

/* Function: Return the total number of lap records for this entrant
 * 
 * Please note: this number is different from the total laps for the entrant
 * It is the number of records in the db for the purpose of listing all laps,
 * including deleted lap and milestone lap records.
 * 
 * TODO:  add timezone offset to this function
 */

function lap_counter_entrant_record_count($entrant_id = 0) {
  $sql = 'SELECT COUNT(LAP_ID) AS lap_count FROM race_laps ';
  if ($entrant_id) {
    $sql .= ' WHERE entrant_id = ' . $entrant_id;
    $sql .= ' GROUP BY entrant_id';
  }
  else {
    $sql .= 'INNER JOIN race_entrant ON ';
    $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
    $sql .= 'INNER JOIN race_edition ON ';
    $sql .= 'race_entrant.race_id = race_edition.race_id AND ';
    $sql .= 'race_entrant.edition_id = race_edition.EDITION_ID ';
    $sql .= ' WHERE current = 1';
  }
  $res = db_query($sql);
  $return = $res->fetchAssoc();
  return $return['lap_count'];
}

function lap_counter_entrant_total($entrant_id = 0, $offset = FALSE ) {
  global $entrants;
  $c39 = chr(39);
  $race_data = $entrants[$entrant_id];
  $sql = 'SELECT lap FROM race_laps ';
  $sql .= ' WHERE ((deleted = 0) and (entrant_id = ' . $entrant_id . ')';
  if ($offset) {
    $t = intval(lap_counter_get_timezone_offset());
    $t = $_SERVER['REQUEST_TIME'] - $t;
    $tlimit = $t - $race_data->reject_limit ;
    $time = date("Y-m-d H:i:s", $tlimit);
    $sql .= ' and (lap_time < ' . $c39 . $time . $c39 . ')';
  }
  $sql .= ')
      ORDER BY lap DESC LIMIT 1';
  $res = db_query($sql);
  $record = $res->fetchAssoc();

  return intval($record['lap']);
}


### Function: Return an array of the most recent laps for an entrant
function lap_counter_entrant_laps(
        $entrant_id = 0,
        $limit = Array('start'=>0,'size'=>30),
        $key = 'lap_id',
        $sort = "ASC"
        ) {
  $return = Array();
  if ($entrant_id){
    $laps = lap_counter_log($entrant_id, $limit, Array('ORDER'=>$sort));
    $ctr = 0;
    foreach ($laps as $lap) {
      $lapnum = $ctr;
      switch ($key) {
        case 'lap':
          $lapnum = $lap['lap'];
          break;
        case 'time':
          return $laps;
          break;
        case 'lap_id':
          $lapnum = $lap['LAP_ID'];
          break;
        default :
          $lapnum = $lap['LAP_ID'];
          break;
      }
      if ($lapnum < 0) {
        $lapnum = $ctr;
      }
      $return[$lapnum] = $lap;
      $ctr = $ctr + 1;
    }
    asort($return);
  }
  return $return;
}

### Function: load the insert lap form
function lap_counter_insert_lap($form_insert, $entrant_id = 0) {
  global $entrants;
  $entrant = $entrants[$entrant_id];
  $entrant_bib = $entrant->bib;
  $display_name = $entrant_bib . ' - ' . $entrant->full_name;
  $display_name .= ' (' . $entrant->short_name .')';
  $full_name = $entrant->full_name;
  // Provide a backlink to return if in error
  $content = '<div id ="back-link"><a href="' .
          lap_counter_entrant_base_url($entrant_id);
  $content .= '"><-- Back</a></div>';
  $form_insert['insert-lap']['backlink'] = array(
    '#markup' => $content
   );

  // Make sure we return to the subject entrant
  $form_insert['insert-lap']['entrant-id']['#value'] = $entrant_id;
  $base_url = lap_counter_entrant_base_url($entrant_id);
  // Make sure we return to the same location
  if (array_key_exists('nav', $_SESSION)) {
    $navigation = $_SESSION['nav'];
    $form_insert['insert-lap']['nav-start']['#value'] = $navigation['start'];
    $base_url .= '&nav-start=' . $navigation['start'];
  }
  $form_insert['#action'] = $base_url;
  $output = drupal_render($form_insert);
  return $output;
  
  $output .= '<div id="lapcounter-subtitle">
    ';
  $output .= '<a href="'. $base_url . '">';
  $output .= '<-- Back</a></div>';
  $output .= '<div class ="warninwg">';
  $output .= 'Warning! You must enter the split time here and not the time of day!
    </div>';
  $output .= '<form id="lapcounter-form-insert-lap" method = "POST" ';
  $output .= 'enctype="multipart/form-data" ';
  $output .= 'action="' . $base_url .'">';
  $output .= '<table id="lap-counter-insert-lap">';
  $output .= '<tr class="header-row">
      <td>
        Lap time
        (since start of race)
      </td>
      <td>';
  $output .= '<input name="entrant-id" type="hidden" value="';
  $output .= $entrant_id .'">';
  $output .= '<input name="lap-time" id="lap-time" type="text" SIZE = "10">
        (hh:mm:ss)
      ';
  $output .= '</td>
      <td>
        <input type="submit" name="insert-lap" value="Insert Lap">
      </td>';
  $output .= '</tr>
  </table>
  </form>
  </div>
</div>';
  $output .= '
    <script type="text/javascript"> 
    document.getElementById(';
  $output .= chr(39) . 'lap-time' . chr(39) . ').focus()
    </script>';
  return $output;
}

### Function: Assign entrants to counter stations
function lap_counter_station_assign($args) {
  $station_id = $args['station-id'];
  $entrant_list = $args['entrants'];
  $entrants = lap_counter_entrants_current();
  foreach ($entrant_list as $entrant_id) {
    $res = db_update('race_entrant')
          ->fields(Array('ctr_station' => $station_id))
          ->condition('ENTRANT_ID', $entrant_id, '=')
          ->execute();
  }
}
