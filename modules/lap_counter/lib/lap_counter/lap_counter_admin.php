<?php
/*
Name: Lap Counter Admin Functions
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
*/

/*
	Copyright 2013 Medur Wilson  (email : jpmw777@zoho.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

function test124() {
  $entrants = lap_counter_entrants_current();
  $ebibs = Array();
  foreach ($entrants as $entrant) {
    $ebibs[$entrant->bib] = $entrant;
  }
  $res = db_select('race_lapcount', 'r')
          ->fields('r')
          ->execute();
  foreach ($res as $record) {
    $bib = $record->bib;
    $id = $record->ENTRY_ID;
    $entrant_id = $ebibs[$bib];
    $entrant_id = $entrant_id->ENTRANT_ID;
    $res = db_update('race_lapcount')
            ->fields(Array('entrant_id' => $entrant_id))
            ->condition('ENTRY_ID', $id, '=')
            ->execute();
  }
}

function lap_counter_admin() {
  $adminform = drupal_get_form('lap_counter_entrants_admin_form');
  return drupal_render($adminform);
}

  
function lap_counter_admin_submit($form, &$form_state) {
  drupal_set_message('Admin was executed');
  $form_id = $form_state['build_info']['form_id'];
  $button = $form_state['clicked_button']['#id'];
  switch ($form_id) {
    case 'lap_counter_entrants_admin_form' :
      switch ($button) {
        case 'lap-counter-entrants-insert-first-lap-submit':
          $args = Array();
          $args['edition-id'] = $form_state['values']['first-lap-edition-id'];
          $args['average'] = $form_state['values']['average'];
          $res = lap_counter_entrants_insert_first_lap($args);
        break;
        case 'lap-counter-entrants-import-submit':
          $args = Array();
          $args['event-id'] = $form_state['values']['event-id'];
          $args['edition-id'] = $form_state['values']['edition-id'];
          $args['db-id'] = $form_state['values']['db-id'];
          $args['data'] = $form_state['values']['data'];
          $res = lap_counter_entrants_update($args);
        break;
        case 'lap-counter-entrants-station-select-submit':
          $args = Array();
          $args['station-id'] = $form_state['values']['station-id'];
          $args['entrants'] = $form_state['values']['entrants'];
          $res = lap_counter_station_assign($args);
        break;
      }
      break;
    case 'lap_counter_events_admin_form' :
      $editions = lap_counter_events();
      $edition_ids = $form_state['input']['edition_id'];
      foreach ($edition_ids as $key => $id) {
        $update = Array();
        $edition = $editions[$id];
        $value = $form_state['input']['race_edition'][$key];
        if ($value != $edition->race_edition) {
          $update['race_edition'] = $value;
        }
        $value = $form_state['input']['start_time'][$key];
        if ($value != $edition->start_time) {
          $update['start_time'] = $value;
        }
        $value = $form_state['input']['offset'][$key];
        if ($value != $edition->offset) {
          $update['offset'] = intval($value);
        }
        $value = FALSE;
        if (array_key_exists('current-' . $id, $form_state['input'])) {
          $value = TRUE;
        }
        if ($value != $edition->current) {
          $update['current'] = intval($value);
        }
        if (count($update)) {
          $res = db_update('race_edition')
                  ->condition('EDITION_ID', $id, '=')
                  ->fields($update)
                  ->execute();
        }
      }
      break;
  }
  return;
}

### Function: lap_counter_entrants_update
function lap_counter_entrants_update($entrydata=Array()) {
  // Update the entrants list for the chosen event / edition
  // updated 17 June 2012 mcw
  // migrated to drupal 7 Sep 2013 mcw
  // Taken from races_11 Drupal
  $event_id = $entrydata['event-id'];
  $edition_id = $entrydata['edition-id'];
  $db_id = intval($entrydata['db-id']);
  if (count($entrydata) == 0) {
    return;
  }
  global $entrants;
  $c39 = chr(39); // single quote character
  if (!array_key_exists('data', $entrydata)) {
    return;
  }
  $data = $entrydata['data'];
  $datalength = strlen($data);
  $tmp = '';
  $ctr = 0;
  // assemble a list
  $namelist = Array();
  $line_items = explode(chr(13), $data);
  foreach ($line_items as $line_item) {
    $ctr ++;
    // Capture data line by line and load into array
    $entrant_data = lap_counter_parse_entrant_data($line_item, $db_id);
    if (!$entrant_data) {
      continue;
    }
    /* The entrants need to be assigned a unique bib number.
     *    At some point this module might provide a bib assignment 
     *    utility but for now it must be done fully in advance.
     */
    if (!array_key_exists('bib', $entrant_data)) {
      continue;
    }
    $bib = intval($entrant_data['bib']);
    if ($bib <=0) {
      // BIB <= 0 NOT ALLOWED
      continue;
    }
    $namecode = $entrant_data['name_code'];
    $fullname = $entrant_data[ 'full_name'];
    $firstname = $entrant_data[ 'first_name'];
    $lastname = $entrant_data[ 'last_name'];
    $age = $entrant_data[ 'age'];
    $gender = $entrant_data[ 'gender'];
    $city = $entrant_data[ 'city'];
    $state = $entrant_data[ 'state'];
    $country = $entrant_data[ 'country'];
    $pedition_id = $entrant_data['edition_id'];
    if (!$pedition_id) {
      $pedition_id = $edition_id;
    }
    $pid = intval($entrant_data['partcpt_id']);
    if (!$pid) {
      /* Try to find the partcpt_id from the name_code
       * The name_code may have changed (!!!).
       * Check both the partcipant table and the entrant table
       */
      
      $sql = 'SELECT p.*, e.name_code, e.partcpt_id ';
      $sql .= 'FROM race_partcpt as p ';
      $sql .= ' LEFT JOIN race_entrant as e ';
      $sql .= ' ON p.ID = e.partcpt_id ';
      $sql .= ' WHERE ((p.name_code = ' . $c39 . strval($namecode) . $c39 . ') ';
      $sql .= ' OR (e.name_code = ' . $c39 . strval($namecode) . $c39 . ')) ';
      $res = db_query($sql);
      $record = $res->fetchAssoc();
      if ($record) {
        $pid = $record['ID'];
      }
      else {
        // If it is not found then create a new participant
        $insert_data = Array();
        $insert_data['full_name'] = $fullname;
        $insert_data['first_name'] = $firstname;
        $insert_data['last_name'] = $lastname;
        $insert_data['name_code'] = $namecode;
        $insert_data['gender'] = $gender;
        $pid = db_insert('race_partcpt')
                ->fields($insert_data)
                ->execute();
      }        
    }
    // Next check the entrant table
    $sql = 'SELECT * FROM race_entrant ';
    $sql .= 'WHERE ((partcpt_id = '. $pid . ') AND (';
    $sql .= 'edition_id = '. $edition_id . '))';
    
    $res = db_select('race_entrant', 'e')
            ->fields('e')
            ->condition('partcpt_id', $pid, '=')
            ->condition('edition_id', $pedition_id, '=')
            ->execute();
            
    $record = $res->fetchAssoc();
    // Prepare the line item data for insert or update in the entrants table  
    $insert_data = Array();
    $insert_data['partcpt_id'] = $pid;
    $insert_data['name_code'] = $namecode;
    $insert_data['edition_id'] = $pedition_id;
    $insert_data['bib'] = $bib;
    $insert_data['full_name'] = $fullname;
    $insert_data['current_age'] = $age;
    $insert_data['city'] = $city;
    $insert_data['state'] = $state;
    $insert_data['country'] = $country;
    // Here is the test
    if (!$record) {
      // This participant is not registered in the db for this edition.
        $entrant_id = db_insert('race_entrant')
                ->fields($insert_data)
                ->execute();
      }
      else {
        // Do an update query on the existing record
        $entrant_id = $record['ENTRANT_ID'];
        $res = db_update('race_entrant')
                ->fields($insert_data)
                ->condition('ENTRANT_ID', $entrant_id, '=')
                ->execute();
      }
      $namelist[$pid] = $namecode;
    }
}


### Function: lapcounter_parse_entrant_data
// Extract data form data string and return as array
/* 2012/3/31
 * Modular function to handle future input formats
 */

function lap_counter_parse_entrant_data($data, $db_id) {
  $return = Array();

  // tab-delimited data is required
  $dataset = explode(chr(9), $data);
  $t = trim(implode('', $dataset));
  if (strlen($t) > 0) {
    // Capture data line by line and load into array
    /* the partcpt_id is optional - really useful if provided
     *   but may not be available in the case of new registrants etc
     */
    $partcpt_id = 0;
    if ($db_id) {
      $partcpt_id = trim(array_shift($dataset));
    }
    $return['partcpt_id'] = $partcpt_id;
    $bib = intval(trim($dataset[0]));
    $return['bib'] = $bib;
    $lname = $dataset[1];
    $lname = trim($lname, ' ');
    $return['last_name'] = $lname;
    $fname = $dataset[2];
    $fname = trim($fname);
    $return['first_name'] = $fname;
    $return['full_name'] = $fname . ' ' . $lname;
    
    $return['gender'] = trim($dataset[3]);
    $return['age'] = trim($dataset[4]);
    $return['address'] = trim($dataset[5]);
    $edition_id = 0;
    if (count($dataset) > 6) {
      $edition_id = trim($dataset[6]);
    }
    $return['edition_id'] = $edition_id;
    
    // generate name code
    $name_code = '';
    $names = explode(' ', $fname);
    foreach ($names as $name) {
      $name_code .= ucfirst($name);
    }
    $names = explode(' ', $lname);
    foreach ($names as $name) {
      $name_code .= ucfirst($name);
    }
    $return['name_code'] = $name_code;
    
    // extract country / state / city from address
    $addresses = explode(',', $return['address']);
    if (count($addresses) > 0 ) {
      $return['city'] = trim($addresses[0]);
      if (count($addresses) == 2 ) {
        $return['country'] = trim($addresses[1]);
      }
      if (count($addresses) > 2 ) {
        $return['state'] = trim($addresses[1]);
        $return['country'] = trim($addresses[2]);
      }
    }
  }
  return $return;
}


### Function: lap_counter_entrants_insert_first_lap
// Bulk Insert a first lap for all participants 

### Function: lap_counter_entrants_update
function lap_counter_entrants_insert_first_lap($args=Array()) {
  global $entrants;
  $edition_id = $args['edition-id'];
  $options = Array('edition_id' => $edition_id);
  $entrants = lap_counter_entrants_current($options);
  $average = intval($args['average']);
  $state = Array('input'=>Array());
  foreach ($entrants as $entrant) {
    $entrant_id = $entrant->ENTRANT_ID;
    $laps = lap_counter_entrant_total($entrant_id);
    if (intval($laps) == 0) {
      $state['input']['entrant-id'] = $entrant_id;
      $atime = $average + mt_rand(-10, 10);
      $atime = strftime("%M:%S", $average + mt_rand(-10, 10));
      $state['input']['lap-time'] = $atime;
      lap_counter_lap_insert(Array(), $state);
    }
  }
}


