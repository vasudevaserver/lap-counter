<?php
/**
 * The base configurations of the Lapcounter.
 *
 */
  
/** The base path for this module */
define('LAP_COUNTER_BASE_PATH', '/');

# toggle the js bib entry component
function lap_counter_use_js($value = -1) {
  if ($value != -1) {
    $args = Array();
    $args['name'] = 'lapcounter_use_js';
    $args['value'] = $value;
    return lap_counter_option_set(14, $args);
    }
  else {
    $return = lap_counter_option_get(14, 0);
    return $return;
  }
}
# Define a list of js enabled forms
function lap_counter_js_enabled_list() {
  $array_list = Array();
  $array_list['dataentryonly'] = 'lap_counter.js';
  $array_list['dataentryjs'] = 'lap_counter.js';
  $array_list['dataentrychute'] = 'chute.js';
  $array_list['dataentrychuteexit'] = 'chute.js';
  return $array_list;
}

# Return or set the URL of the lap_counter utility
# DEPRECATED - USE $base_url ONLY (v 7.x-0.18)
function lap_counter_base_url($value = '') {
  return $base_url;
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_base_url';
    $args['value'] = $value;
    return lap_counter_option_set(1, $args);
    }
  else {
    return lap_counter_option_get(1, LAP_COUNTER_BASE_PATH);
  }
}


# Return the base url for an entrant
function lap_counter_entrant_base_path($entrant_id=0) {
  $url = LAP_COUNTER_BASE_PATH;
  if ($entrant_id) {
    $url .= 'entrant?entrant-id=';
    $url .= strval($entrant_id);
  }
  return $url;
}

# Return the base url for an entrant
# deprecated (7.x-0.18)
function lap_counter_entrant_base_url($entrant_id=0) {
  return lap_counter_entrant_base_path($entrant_id);
}

/** The number of counter records to process */
define('LAPCOUNTER_CRON_BATCH_SIZE', 20);

# Return or set the lapcounter cron batch size
function lap_counter_cron_batch_size($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lapcounter_cron_batch_size';
    $args['value'] = $value;
    $return = lap_counter_option_set(2, $args);
    return intval($return[1]);
    }
  else {
    $return = lap_counter_option_get(2, LAPCOUNTER_CRON_BATCH_SIZE);
    return intval($return[1]);
  }
}

/** The number of records to show in the lap counter log */
define('LAP_COUNTER_WARNING_THRESHOLD', 55);

# Return or set the warning threshold as a percentage of the lap split time */
function lap_counter_warning_threshold($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_warning_threshold';
    $args['value'] = $value;
    return lap_counter_option_set(3, $args);
    }
  else {
    return lap_counter_option_get(3, LAP_COUNTER_WARNING_THRESHOLD);
  }
}

/** The number of seconds between window reloads */
define('LAP_COUNTER_REFRESH_RATE', 3);

# Return or set the browser refresh rate */
function lap_counter_refresh_rate($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_refresh_rate';
    $args['value'] = $value;
    return lap_counter_option_set(4, $args);
    }
  else {
    return lap_counter_option_get(4, LAP_COUNTER_REFRESH_RATE);
  }
}

/** The number of seconds between cron reloads */
define('LAP_COUNTER_CRON_RATE', 3);

# Return or set the browser refresh rate */
function lap_counter_cron_rate($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_cron_rate';
    $args['value'] = $value;
    return lap_counter_option_set(7, $args);
    }
  else {
    return lap_counter_option_get(7, LAP_COUNTER_CRON_RATE);
  }
}

# Return the list of views requiring a refresh interval
function lap_counter_refresh_rate_list() {
  $return = Array();
  $return[] = 'process-laps';
  $return[] = 'log';
  $return[] = 'stationlog';
  $return[] = 'scoreboard';
  return $return;
}
  /** The number of seconds to allow for rejection of lap count */
define('LAP_COUNTER_REJECT_THRESHOLD', 89);

# Return or set the lap_counter log file name
function lap_counter_reject_threshold($args=Array()) {
  if (isset($args['race_id'])) {
    $race_id = $args['race_id'];
    $res = db_select('race_event', 'r')
         ->fields('r')
         ->condition('RACE_ID', $race_id, '=')
         ->execute();
    $result = $res->fetchAssoc();
    if ($result) {
      $limit = intval($result['reject_limit']);
      return $limit;
    }
    else {
      return lap_counter_option_get(5, LAP_COUNTER_REJECT_THRESHOLD);
    }
  }
  elseif (isset($args['value'])) {
    $value = $args['value'];
    $args = Array();
    $args['option_name'] = 'lapcounter_reject_threshold';
    $args['option_value'] = $value;
    return lap_counter_option_set(5, $args);
    }
  else {
    return lap_counter_option_get(5, LAP_COUNTER_REJECT_THRESHOLD);
  }
}

/** The file name of the lap_counter log */
define('LAP_COUNTER_LOG_NAME', 'lapctr_log');

# Return or set the lap_counter log file name
function lap_counter_log_name($value = '') {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_log_name';
    $args['value'] = $value;
    return lap_counter_option_set(18, $args);
    }
  else {
    return lap_counter_option_get(18, LAP_COUNTER_LOG_NAME);
  }
}

/** The file path of the log directory */
define('LAP_COUNTER_LOG_DIR', '/WinXPShared/log/');

# Return or set the lap_counter log file directory */
function lap_counter_log_dir($value = '') {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_log_dir';
    $args['value'] = $value;
    return lap_counter_option_set(19, $args);
    }
  else {
    return lap_counter_option_get(19, LAP_COUNTER_LOG_DIR);
  }
}

/** The number of laps to display on the entrant detail page */
define('LAP_COUNTER_ENTRANT_SIZE', 600);

# Return or set the lap_counter entrant size
function lap_counter_entrant_size($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_entrant_size';
    $args['value'] = $value;
    return lap_counter_option_set(20, $args);
    }
  else {
    return lap_counter_option_get(20, LAP_COUNTER_ENTRANT_SIZE);
  }
}

/** The number of laps to display on the station report */
define('LAP_COUNTER_STATION_SIZE', 51);

# Return or set the lap_counter station size
function lap_counter_station_size($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_station_size';
    $args['value'] = $value;
    return lap_counter_option_set(21, $args);
    }
  else {
    return lap_counter_option_get(21, LAP_COUNTER_STATION_SIZE);
  }
}

/** The number of records to show in the lap counter log */
define('LAP_COUNTER_LOG_SIZE', 200);

# Return or set the lap_counter station size
function lap_counter_log_size($value = 0) {
  if ($value) {
    $args = Array();
    $args['name'] = 'lap_counter_log_size';
    $args['value'] = $value;
    return lap_counter_option_set(22, $args);
    }
  else {
    return lap_counter_option_get(22, LAP_COUNTER_LOG_SIZE);
  }
}



/** The laps to kilometers conversion */
define('LAP_COUNTER_LAPS_TO_KMS', 2.5);

/** The name of the display vocabulary */
define('LAP_COUNTER_VOCABULARY_NAME', 'display');

// The list of display terms required by this module
function lap_counter_display_vocabulary_terms() {
  $terms = Array();
  $terms[] = 'log';
  $terms[] = 'station-log';
  $terms[] = 'cron-job';
  $terms[] = 'data-entry';
  $terms[] = 'data-entry-only';
  $terms[] = 'scoreboard';
  $terms[] = 'entrants';
  $terms[] = 'entrant';
  $terms[] = 'insert-lap';
  $terms[] = 'auto-refresh';
  return $terms;
}
