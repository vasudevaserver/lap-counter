<?php
/*
Name: lap_counter_results
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.10
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2012  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * generate the standings table
 */

function lap_counter_standings_table() {
  $standings = lap_counter_standings();
  $gender_select = Array('M'=>'Men','F'=>'Women');
  $output = '<div id="lap_counter-listing">
    ';
  foreach ($standings as $edition_id => $race_data) {
    $info = $race_data['info'];
    $race_name = $info['race_name'];
    $output .= '<table id="lap_counter-scoreboard">
      <tbody>
        <tr>
 ';
//      $race_edition = $resultset['race_edition'];
    $output .= '<td class="lap_counter-subtitle"
        colspan = "2" >' . $race_name . '</td>
        </tr>';
      $output .= '<tr>';
      foreach ($gender_select as $gender => $gtitle) {
        $output .= '<td><table class="lap_counter-standings"><tr>
          ';
        $output .= '<td class="lap_counter-subtitle" 
          colspan = "4">' . $gtitle . '</td></tr>
        ';
        $output .= '<tr class="header-row">
          <th class="aligncenter col-data-small">Place</th>
          <th class="col-data-large">Name (bib)</th>
          <th class="aligncenter col-data-small">Laps</th>
          <th class="aligncenter col-data-small">Km</th>
          </tr>
          ';
        $data = $race_data[$gender];
        $ctr = 0;
        foreach ($data as $datum) {
          $ctr++;
          $output .= '
            <tr class="';
          $class = 'oddrow';
          if ($ctr/2 == intval($ctr/2)) {
            $class = 'evenrow';        
          }
          $output .= $class . '">
          ';
          $output .= '<td class="aligncenter">' . $ctr. '</td>
            ';
          $entrant_url = lap_counter_entrant_base_url($datum->entrant_id);
//          $entrant_url = '/scoreboard?entrant_id=' . $datum->entrant_id;
          $output .= '<td class="full-name"><a href="' . $entrant_url
           . '&nav_last=1#lastlap">
          ';
          $output .= $datum->full_name . ' (' . $datum->bib . ')';
          $output .= '</a></td>
          ';
          $output .= '<td class="aligncenter">' . $datum->laps . '</td>
            <td class="aligncenter">' . intval(intval($datum->laps)*0.4)/1 . '</td>
           </tr>';
        }
        $output .= '</table></td>
        ';
      }
      
      $output .= '</tr></table>
        ';
      $output .= '</td></tr>
        ';
    $output .= '</tbody></table>
        ';
  }
  $output .= '</div>';
  return $output;
}

/**
 * Prepare the standings data for the current editions
 */
function lap_counter_standings() {
  $sql = 'SELECT race_edition.EDITION_ID, race_edition.race_id, ';
  $sql .= 'name AS race_name, race_edition ';
  $sql .= 'FROM race_edition INNER JOIN race_event ';
  $sql .= 'ON race_edition.race_id = race_event.RACE_ID ';
  $sql .= ' WHERE (';
  $sql .= 'race_edition.current=1) ';
  $sql .= 'ORDER BY RACE_ID DESC';
  $res = db_query($sql);
  $race_editions = Array();
  $result = Array();
  foreach ($res as $record) {
    $data = Array();
    $edition_id = $record->EDITION_ID;
    $data['race_id'] = $record->race_id;
    $data['race_edition'] = $record->race_edition;
    $data['race_name'] = $record->race_name;
    $race_editions[] = $edition_id;
    $result[$edition_id]['info'] = $data;
  }
  $gender_select = Array('M', 'F');
  foreach ($gender_select as $gender) {
    $setgender = db_update('race_control')
      ->fields(array('current_gender' => $gender))
      ->condition('control_id', 1)
      ->execute();
    foreach ($race_editions as $edition_id) {
      $standings = lap_counter_edition_standings($edition_id, $gender);
      $result[$edition_id][$gender] = $standings;
    }
  }
  return $result;
}



### Function: Return an array of the current standings
function lap_counter_edition_standings($edition_id, $gender) {
  $offset = lap_counter_get_offset($edition_id);
  $sql  = 'SELECT MAX(race_laps.lap) AS laps, race_laps.entrant_id, ';
  $sql .= 'MAX(race_entrant.full_name) AS full_name, ';
  $sql .= 'MAX(race_entrant.bib) as bib ';
  $sql .= 'FROM race_laps INNER JOIN race_entrant ON ';
  $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= 'INNER JOIN race_partcpt ';
  $sql .= 'ON race_entrant.partcpt_id = race_partcpt.ID ';
  $sql .= 'INNER JOIN race_control ';
  $sql .= 'ON race_partcpt.gender = race_control.current_gender ';
  $sql .= 'WHERE ((edition_id = ' . $edition_id . ')';
  $sql .= ' AND (race_laps.lap_time < NOW() - INTERVAL ' . $offset . ' SECOND)';
  $sql .= ' AND (race_laps.deleted = 0)';
  $sql .= ' AND (race_control.control_id = 1)';
  $sql .= ') ';
  $sql .= 'GROUP BY race_laps.entrant_id ';
  $sql .= 'ORDER BY laps DESC, entry_id';
  $res = db_query($sql);
  $return = Array();
  if ($res) {
    $return = $res;
  }
  return $return;
}


/**
 * generate the log table
 */

function lap_counter_log_table_rows($logdata = Array()) {
  $options = Array('lapdata'=>FALSE);
  global $entrants;
  $output = '
    <tr class="header-row">';
  $output .= '
     <th class="time col-time-med">Time</th>
     <th class="split col-time-small">Split</th>
     <th class="bib col-data-small">Bib</th>
     <th class="name col-full-name">Name</th>
     <th class="lap col-data-small">Lap</th>
     <th class="break col-data-small">Brk</th>
     <th class="delete col-data-small">Del</th>
     <th class="break col-data-large">Notes</th>
    </tr>
    ';
  $ctr=0;
  foreach ($logdata as $resultset) {
    $ctr++;
    $output .= '
      <tr class="';
    $class = 'oddrow';
    if ($ctr/2 == intval($ctr/2)) {
      $class = 'evenrow';        
    }
    $output .= $class . '">
    ';
    $entrant_id = $resultset['entrant_id'];
    $bib = $resultset['bib'];
    $entrant_data = $entrants[$entrant_id];
    $start_time = $entrant_data->start_time;
    $sttime = strtotime($start_time);
    $time = $resultset['lap_time'];
    $time1 = strtotime($time);
    $ntime = $time1 - $sttime;
    $ftime = lap_counter_format_timestamp($ntime);
    $output .= '<td class="time text-right">' . $ftime . '</td>
    ';
    $split = $resultset['split'];
    $split = lap_counter_format_timestamp($split);
    $output .= '<td class="time alignright">' . $split . '</td>
    ';
    $output .= '<td class="aligncenter">' . $bib . '</td>
    ';
    $entrant_url = lap_counter_entrant_base_url($entrant_id);
    $output .= '<td class="full-name"><a href="' . $entrant_url
    . '&nav_last=1#lastlap">
    ';
    $output .= $resultset['full_name'] . ' ('. $resultset['short_name'] . ')';
    $output .= '</a></td>
    ';
    $laps = $resultset['lap'];
    $output .= '<td class="aligncenter">' . $laps . '</td>
    ';
    $output .= '<td class="aligncenter">';
    $break = $resultset['break'];
    if ($break) {
      $output .= 'X';
    }
    $output .= '</td>
    ';
    $output .= '<td class="aligncenter">';
    $deleted = $resultset['deleted'];
    if ($deleted) {
      $output .= 'X';
    }
    $output .= '</td>';
    $output .= '<td class="notes">' . $resultset->notes . '</td>
    ';
    $output .= '
      </tr>';
    }
  return $output;
}

/**
 * generate the log table
 */

function lap_counter_log_table() {
  global $entrants;
  $datacount = intval(lap_counter_entrant_record_count());
  $size = lap_counter_log_size();
  $size = intval($size[1]);
  $navigation = Array();
  $navigation['id'] = 'log';
  $navigation['size'] = $size;
  $navigation['total'] = $datacount;
  $navigation = lap_counter_get_navigation($navigation);
  $options = Array('HIDE_DELETED_LAPS'=>TRUE);
  $logdata = lap_counter_log(0, $navigation, $options);
  $output = '<div id="lap_counter-listing">
    ';
  /*
  $output .= '<form id="lap_counter-entrant-navigation" method = "POST" 
        enctype="multipart/form-data">';
  $output .= lap_counter_navigation_bar($navigation);      
  $output .= '</form>
 ';
   * 
   */
  $output .= '<table id="lap_counter-log">
    <tbody>
  ';
  $output .= lap_counter_log_table_rows($logdata);
  $output .= '</tbody>
  ';
  $output .= '</table>
  ';
  $output .= '</div>';
  return $output;
}

//  $offset = lap_counter_option_get(8);
### Function: Return an array of the most recent laps counted
function lap_counter_log($entrant_id = 0,
        $limit = Array('start' => 0, 'size' => 30),
        $options = Array()) {
//  $offset = lap_counter_option_get(8);
  $offset = 0;
  $sql = 'SELECT race_laps.*, race_entrant.full_name, race_entrant.bib, ';
  $sql .= 'race_entrant.ctr_station, race_entrant.race_id, ';
  $sql .= 'race_entrant.race_edition, race_event.short_name, ';
  $sql .= 'race_edition.start_time ';
  $sql .= 'FROM race_laps INNER JOIN race_entrant ON ';
  $sql .= 'race_laps.entrant_id = race_entrant.ENTRANT_ID ';
  $sql .= 'INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= 'INNER JOIN race_event ON ';
  $sql .= 'race_edition.race_id = race_event.RACE_ID ';
  
  
  $wheres = Array();
  $wheres[] = 'race_edition.current = 1';
  $wheres[] = 'race_laps.lap_time < NOW() - INTERVAL race_edition.offset SECOND';
  if ($entrant_id) {
    $wheres[] = 'race_laps.entrant_id = ' . $entrant_id;
  }
  $order = 'DESC';
  if (count($options)) {
    $ak = array_keys($options);
    if (in_array('station_id', $ak)) {
      $where = 'race_entrant.ctr_station = ' . chr(39);
      $where .= $options['station_id'] . chr(39);
      $wheres[] = $where;
    }
    if (in_array('EDITION_ID', $ak)) {
      $wheres[] = 'race_entrant.edition_id = ' . $options['EDITION_ID'];
    }
    if (in_array('HIDE_DELETED_LAPS', $ak)) {
      $wheres[] = 'race_laps.deleted = 0';
    }
    if (in_array('ORDER', $ak)) {
      $order = $options['ORDER'];
    }
  }
  if (count($wheres)) {
    $where = implode(') AND (', $wheres);
    $where = 'WHERE ((' . $where . ')) ';
    $sql .= $where;
  }
  $sql .= ' ORDER BY race_laps.lap_time ';
  $sql .= $order;
  $sql .= ', LAP_ID';
  if ($limit) {
    $start = 0;
    if (is_array($limit)) {
      if (array_key_exists('start', $limit)) {
        $start = $limit['start'];
      }
      $size = intval(LAP_COUNTER_STATION_SIZE);
      if (array_key_exists('size', $limit)) {
        $size = $limit['size'];
      }
      $sql .= ' LIMIT ' . strval($start) . ', ' . strval($size);
    }
  }
  $res = db_query($sql);
  $return = Array();
  while (1) {
    $laprecord = $res->fetchAssoc();
    if (!$laprecord) {
      break;
    }
    $return[] = $laprecord;
  }
  
  return $return;
}
