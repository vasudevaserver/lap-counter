<?php

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 */
function lap_counter_admin_form($form, &$form_state) {
  $base = LAP_COUNTER_BASE_PATH;
//  $base = $base[1];
  $form['lap_counter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Lap Counter Management'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  $form['lap_counter']['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('System Options'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/lap_counter/options';
  $form['lap_counter']['options']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Options</a>'
  );
  $form['lap_counter']['entrants'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Entrants'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/lap_counter/entrants';
  $form['lap_counter']['entrants']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Entrants</a>'
  );
  $form['lap_counter']['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Events'),
      '#collapsible' => FALSE,
  );
  $url = $base . 'admin/config/lap_counter/events';
  $form['lap_counter']['events']['link'] = array(
      '#markup' => '<a href="' . $url . '">
        Events</a>'
  );
  return $form;
}

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 */
function lap_counter_options_admin_form($form, &$form_state) {
  $base = LAP_COUNTER_BASE_PATH;
  $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Lap Counter Options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Count laps in a race.')
  );
  $form['options']['system'] = array(
      '#type' => 'fieldset',
      '#title' => t('System Options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Manage the module\'s settings.')
  );
  $url = $base . 'process-laps';
  $form['options']['system']['process-laps-link'] = array(
      '#title' => t('Process Laps'),
      '#markup' => '<a href="' . $url . '">
      Process Laps</a>',
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#id' => 'lap-counter-options-submit',
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_admin_submit'),
  );
  return $form;
}

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 * Entrants Management
 * 
 */
function lap_counter_entrants_admin_form($form, &$form_state) {
  $entrants = lap_counter_entrant_select_list();
  $editions = lap_counter_edition_select_list();
  $form['entrants'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Entrants'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Import and edit the entrants in the event.')
  );
  $form['entrants']['first-lap'] = array(
      '#type' => 'fieldset',
      '#title' => t('Insert First Lap'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Insert a first lap for all participants.')
  );
  $form['entrants']['first-lap']['first-lap-edition-id'] = array(
      '#type' => 'select',
      '#title' => t('Edition ID'),
      '#options' => $editions,
      '#description' => t('The id of the event edition being inserted.'),
  );
  $form['entrants']['first-lap']['average'] = array(
      '#type' => 'textfield',
      '#title' => t('Average First Lap (seconds)'),
      '#value' => 150,
  );
  $form['entrants']['first-lap']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'lap-counter-entrants-insert-first-lap-submit',
      '#value' => t('Insert'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_admin_submit'),
  );
  $form['entrants']['import'] = array(
      '#type' => 'fieldset',
      '#title' => t('Import Entrants'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['entrants']['import']['edition-id'] = array(
      '#type' => 'select',
      '#title' => t('Edition ID'),
      '#options' => $editions,
      '#description' => t('The id of the event edition being imported.'),
  );
  $form['entrants']['import']['db-id'] = array(
      '#type' => 'select',
      '#title' => t('DB ID column check'),
      '#options' => Array(1 => 'ID is Present',
          0 => 'No ID provided'),
      '#description' => t('Indicates the presence of an db id column in the import list'),
  );
  $form['entrants']['import']['data'] = array(
      '#type' => 'textarea',
      '#title' => t('Enter the data here.'),
      '#description' => t('The raw entrant data.'),
  );
  $form['entrants']['import']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'lap-counter-entrants-import-submit',
      '#value' => t('Import'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_admin_submit'),
  );
  $form['entrants']['stations'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage counting stations'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Add, Delete and Edit counting station settings.')
  );
  $stations = Array();
  $res = db_select('race_counter_stations', 's')
          ->fields('s', Array('ID'))
          ->execute();
  foreach ($res as $sid) {
    $id = $sid->ID;
    $stations[$id] = $id;
  }
  $form['entrants']['stations']['station-id'] = array(
      '#prefix' => '<table><tr><td>',
      '#type' => 'radios',
      '#title' => t('Assign counting stations'),
      '#description' => t('Assign counting stations.'),
      '#options' => $stations,
  );
  $form['entrants']['stations']['entrants'] = array(
      '#title' => t('Select participants to be assigned.'),
      '#prefix' => '</td><td>',
      '#type' => 'select',
      '#multiple' => TRUE,
      '#description' => t('select a participant.'),
      '#options' => $entrants,
      '#size' => 15,
  );
  $form['entrants']['stations']['close'] = array(
      '#suffix' => '</td></tr></table>',
  );
  $form['entrants']['stations']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'lap-counter-entrants-station-select-submit',
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_admin_submit'),
  );

  return $form;
}

/**
 * Admin Settings
 *
 * Forms for administrator to set configuration options.
 * Entrants Management
 * 
 */
function lap_counter_events_admin_form($form, &$form_state) {
  $form['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Events'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#description' => t('Import and edit the entrants in the event.')
  );
  $form['events']['events'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Events'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  $form['events']['editions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manage Editions'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
  );
  $form['events']['editions']['list'] = array(
      '#type' => 'markup',
      '#prefix' => '<table id="lap_counter_editions">
      <tr class="header">
      <th><label for="short_name">Name</label></th>
      <th><label for="EDITION_ID">Edition ID</label></th>
      <th><label for="race_edition">Edition</label></th>
      <th><label for="start_time">Start Time</label></th>
      <th><label for="offset">Offset</label></th>
      <th><label for="current">Active</label></th>
      </tr>',
      '#suffix' => '</table>',
      '#markup' => lap_counter_editions_admin_form(),
  );
  /*   * 
   */
  $form['submit'] = array(
      '#type' => 'submit',
      '#id' => 'lap-counter-events-submit',
      '#value' => t('Save'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_admin_submit'),
  );
  return $form;
}

/**
 * * Implements validation from the Form API.
 *
 * @param $form
 *   A structured array containing the elements and properties of the form.
 * @param $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function lap_counter_form_validate($form, &$form_state) {
  
}

// The edit lap form 
// Modify or delete laps here
function lap_counter_edit_lap_form($form, &$form_state) {
  $form['entrant-bib'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-bib',
      '#name' => 'entrant-bib',
      '#value' => 'placeholder',
  );
  $form['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['lap-id'] = array(
      '#type' => 'hidden',
      '#id' => 'lap-id',
      '#name' => 'lap-id',
      '#value' => 'placeholder',
  );
  $form['nav-last'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-last',
      '#name' => 'nav-last',
      '#value' => 'placeholder',
  );
  $form['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $form['submit_break'] = array(
      '#type' => 'submit',
      '#id' => 'edit-lap-break-submit',
      '#value' => t('BREAK'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_lap_edit'),
  );
  $form['submit_delete'] = array(
      '#type' => 'submit',
      '#id' => 'edit-lap-delete-submit',
      '#value' => t('DELETE'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_lap_edit'),
  );
  return $form;
}

// The bib data entry form with javascript
function lap_counter_bib_data_js_entry_form($form, &$form_state) {
  global $user;
  $uid = 0;
  if ($user) {
    $uid = $user->uid;
  }
  $c39 = chr(39);
  $form['user-id'] = array(
      '#id' => 'userid',
      '#type' => 'hidden',
      '#value' => $uid,
      '#attributes' => array('id' => 'userid'),
  );
  $form['bib-number'] = array(
      '#title' => t('Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
          'onfocus' => 'this.value = ""',
          'onchange' => 'SetBibNumber(this.value)',
          'onblur' => 'SetBibNumber(this.value);
        ',
      ),
  );

//  $log = lap_counter_entry_log();
  $form['log'] = array(
      '#prefix' => '<div id="bib-entry">',
      '#suffix' => '</div>',
      '#type' => 'textarea',
      '#id' => 'entry-items',
      '#rows' => 10,
      '#default_value' => '',
      '#attributes' => array(
          'onchange' => 'document.getElementById("bib-number").focus();return (false);',
          'onfocus' => 'document.getElementById("bib-number").focus();'
          ),
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#attributes' => array(
          'onclick' => 'document.getElementById("bib-number").focus();return (false);'),
//      '#attributes' => array('onclick' => 'return (false);'),
      '#executes_submit_callback' => FALSE,
      '#submit' => array('lap_counter_bib_data_entry'),
  );

  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib-number");';
  $content .= 'ob.focus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}

// The Chute Finish Line entry form
function lap_counter_chute_entry_form($form, &$form_state) {
  $form['bib-number'] = array(
      '#title' => t('Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
//          'onfocus' => 'this.value = ""',
//          'onchange' => 'SetChuteEntry(this.value)',
//          'onkeyup' => 'SetChuteEntry(event)',
//          'onblur' => 'SetChuteEntry(this.value);
//           document.getElementById("bib-number").focus();,
      ),
  );
//  $log = lap_counter_entry_log();
  $form['log'] = array(
      '#prefix' => '<ul id="bib-entry">',
      '#suffix' => '</ul>',
      '#type' => 'markup',
      '#attributes' => array(
          'onchange' => 'document.getElementById("bib-number").focus();return (false);',
          'onfocus' => 'document.getElementById("bib-number").focus();'
          ),
  );

  $form['submit'] = array(
      '#type' => 'textfield',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#attributes' => array(
          'onfocus' => 'document.getElementById("bib-number").focus();return (false);'),
//      '#attributes' => array('onclick' => 'return (false);'),
  );
  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib-number");';
  $content .= 'ob.focus();
    </script>';
  $form['script'] = array(
      '#markup' => $content,
  );
  return $form;
}
  
// The Chute Exit entry form
function lap_counter_chute_exit_form($form, &$form_state) {
  global $user;
    $content = '<a href="' . $url . '">Enter Finisher Bib</a>&nbsp;';
    $form['link1'] = array(
        '#markup' => $content
    );
  return $form;
}
  


// The bib data entry form
function lap_counter_bib_data_entry_form($form, &$form_state) {
  global $user;
  $uid = 0;
  if ($user) {
    $uid = $user->uid;
  }
  $c39 = chr(39);
  $check = $_SERVER['REDIRECT_URL'];
  $url = LAP_COUNTER_BASE_PATH;
  $url_array = Array();
  $url_array[] = $url . 'entry-only';
  $url_array[] = $url . 'entry-js';
  if ($check != $url) {
    $use_js = lap_counter_use_js();
    $use_js = $use_js[1];
    if ($use_js) {
      $url .= 'entry-js';
    } else {
      $url .= 'entry-only';
    }
    $content = '<a href="' . $url . '">Remove Header</a>&nbsp;';
    $form['link1'] = array(
        '#markup' => $content
    );
    $content = '<a href="chute-entry">Chute: Finish Line</a>&nbsp;';
    $form['link2'] = array(
        '#markup' => $content
    );
    $content = '<a href="chute-exit">Chute: Exit</a>';
    $form['link3'] = array(
        '#markup' => $content
    );
  }
  $form['entry'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bib Number entry'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
  );
  $form['entry']['user-id'] = array(
      '#id' => 'userid',
      '#type' => 'hidden',
      '#value' => $uid,
      '#attributes' => array('id' => 'userid'),
  );
  $form['entry']['bib-number'] = array(
      '#title' => t('Enter Bib Number'),
      '#type' => 'textfield',
      '#id' => 'bib-number',
      '#name' => 'bib-number',
      '#default_value' => '',
      '#attributes' => array(
          'onfocus' => 'this.value = ""',
      ),
  );
  $log = lap_counter_entry_log($uid);
  $form['entry']['log'] = array(
      '#prefix' => '<div id="bib-entry">',
      '#suffix' => '</div>',
      '#type' => 'textarea',
      '#id' => 'entry-items',
      '#rows' => 10,
      '#default_value' => $log,
  );
  $form['entry']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'enter-bib-number-submit',
      '#value' => t('Enter Bib Number'),
      '#executes_submit_callback' => FALSE,
      '#submit' => array('lap_counter_bib_data_entry'),
  );

  $content = '
    <script type="text/javascript"> 
     ob = document.getElementById("bib-number");';
  $content .= 'ob.focus();
    
    </script>';
  $form['entry']['script'] = array(
      '#markup' => $content,
  );
  return $form;
}

### The Insert lap form 

function lap_counter_insert_lap_form($form, &$form_state) {
  $c39 = chr(39);
  $form['insert-lap'] = array(
      '#title' => t('Insert Lap'),
      '#type' => 'fieldset',
      '#id' => 'insert-lap-wrapper',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t(
              'Warning! You must enter the elapsed time here and not the time of day!')
  );
  // Put this here to place the item at the top
  $form['insert-lap']['backlink'] = array(
      '#markup' => 'placeholder'
  );
  $form['insert-lap']['split'] = array(
      '#title' => t('Enter lap time here'),
      '#type' => 'textfield',
      '#id' => 'lap-time',
      '#name' => 'lap-time',
      '#description' => t('(HHH:MM:SS) since start of race')
  );
  $form['insert-lap']['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['insert-lap']['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $content = '
    <script type="text/javascript"> 
    document.getElementById(';
  $content .= chr(39) . 'lap-time' . chr(39) . ').focus()
    </script>';
  $form['insert-lap']['script'] = array(
      '#markup' => $content
  );
  $form['insert-lap']['submit'] = array(
      '#type' => 'submit',
      '#id' => 'insert-lap-submit',
      '#value' => t('Insert Lap'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_lap_insert'),
  );
  return $form;
}

function lap_counter_navigation_form($form, &$form_state) {
  $form['entrant-id'] = array(
      '#type' => 'hidden',
      '#id' => 'entrant-id',
      '#name' => 'entrant-id',
      '#value' => 'placeholder',
  );
  $form['nav-start'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-start',
      '#name' => 'nav-start',
      '#value' => 'placeholder',
  );
  $form['nav-size'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-size',
      '#name' => 'nav-size',
      '#value' => 'placeholder',
  );
  $form['nav-total'] = array(
      '#type' => 'hidden',
      '#id' => 'nav-total',
      '#name' => 'nav-total',
      '#value' => 'placeholder',
  );
  $form['submit-first'] = array(
      '#type' => 'submit',
      '#id' => 'submit-first',
      '#name' => 'submit-first',
      '#value' => '|<',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_process_navigation'),
  );
  $form['submit-prev'] = array(
      '#type' => 'submit',
      '#id' => 'submit-prev',
      '#name' => 'submit-prev',
      '#value' => '<',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_process_navigation'),
  );
  $form['submit-next'] = array(
      '#type' => 'submit',
      '#id' => 'submit-next',
      '#name' => 'submit-next',
      '#value' => '>',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_process_navigation'),
  );
  $form['submit-last'] = array(
      '#type' => 'submit',
      '#id' => 'submit-last',
      '#name' => 'submit-last',
      '#value' => '>|',
      '#executes_submit_callback' => TRUE,
      '#submit' => array('lap_counter_process_navigation'),
  );
  return $form;
}

function lap_counter_navigation_bar($nav) {
  $spacer = lap_counter_module_location();
  $spacer .= '/images/spacer.gif';
  $spacer = '<img class = "spacer" src="/' . $spacer . '">';
  $start = 0;
  if (array_key_exists('start', $nav)) {
    $start = $nav['start'];
  }
  $size = 0;
  if (array_key_exists('size', $nav)) {
    $size = $nav['size'];
  }
  $total = 0;
  if (array_key_exists('total', $nav)) {
    $total = $nav['total'];
  }
  $output = '<form id="lap_counter-entrant-navigation" method = "POST" 
        enctype="multipart/form-data" ';
  $target = '';
  if (array_key_exists('target', $nav)) {
    $target = $nav['target'];
    $output .= 'action="' . $target . '"';
  }
  $output .= '>';
  $output .= '<div class="navigation-bar" >';
  $output .= '<input type ="hidden" name="nav-start" value="' . $start . '" />';
  $output .= '<input type ="hidden" name="nav-size" value="' . $size . '" />';
  $output .= '<input type ="hidden" name="nav-total" value="' . $total . '" />';
  $output .= '<input type ="hidden" name="nav-last" value="" />';

  $entrant_id = '';
  if (array_key_exists('entrant-id', $nav)) {
    $entrant_id = $nav['entrant-id'];
    $output .= '
      <input type ="hidden" name="entrant-id" value="' . $entrant_id . '" />';
  }
  $output .= '<table><tr>';
  $output .= '<td>
    ';
  if ($start > 0) {
    $output .= '<div>
      <input type="submit" title="First" value="<<" name="submit-first">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start >= $size) {
    $output .= '<div>
      <input type="submit" title="Prev" value="<" name="submit-prev">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start < $total - $size) {
    $output .= '<div>
      <input type="submit" title="Next" value=">" name="submit-next">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>
    <td>
    ';
  if ($start < $total - $size) {
    $output .= '<div>
      <input type="submit" title="Last" value=">>" name="submit-last">
      </div>';
  } else {
    $output .= $spacer;
  }
  $output .= '</td>';
  $output .= '</tr></table></div>
    ';
  $output .= '</form>';

  return $output;
}

// Prepare the navigation bar data for the entrant page.
function lap_counter_editions_admin_form() {
  $editions = '';
  $events = lap_counter_events();
  foreach ($events as $edition) {
    $editions .= '<tr>
      ';
    $input = '<input type="hidden" name="edition_id[]' . $edition->EDITION_ID . '"';
    $input .= '" value="' . $edition->EDITION_ID . '">';
    $editions .= $input;
    $editions .= '<td>';
    $editions .= $edition->short_name;
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= $edition->EDITION_ID;
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="race_edition[]"
            size="12" value="' . $edition->race_edition . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="start_time[]"
            size="21" value="' . $edition->start_time . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="text"  name="offset[]"
            size="10" value="' . $edition->offset . '" />';
    $editions .= '</td>';
    $editions .= '<td>';
    $editions .= '<input type="checkbox"  name="current-';
    $editions .= $edition->EDITION_ID . '" ';
    if (intval($edition->current) == 1) {
      $editions .= 'checked ';
    }
    $editions .= '/>';
    $editions .= '</td>';
    $editions .= '</tr>';
  }
  return $editions;
}
