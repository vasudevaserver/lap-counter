<?php
/*
Name: Lap Counter
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.01
Author: Medur C Wilson
Author URI: http://medur.ca
*/

/*
	Copyright 2012  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

require_once('lap_counter_admin.php');
require_once('lap_counter_config.php');
require_once('lap_counter_entrant.php');
require_once('lap_counter_forms.php');
require_once('lap_counter_results.php');
require_once('lap_counter_utils.php');

/**
 * Bib data entry listener script
 */

function lap_counter_bib_entry_remote() {
  $bib = 0;
  if (array_key_exists('bibnumber', $_REQUEST)) {
    $bib = $_REQUEST['bibnumber'];
  }
  if (!$bib) {
//    return 'Bib not found';
    $bib = '-';
  }
  $uid = 0;
  if (array_key_exists('userid', $_REQUEST)) {
    $uid = $_REQUEST['userid'];
  }
  $fn = '0';
  if (array_key_exists('fn', $_REQUEST)) {
    $fn = $_REQUEST['fn'];
  }
  switch ($fn) {
    case '0' :
      $res = lap_counter_process_bib_number($bib, $uid);
      $return = '';
      unset($res['sql']);
      $choose = Array('bib', 'name','lap');
      $args = Array();
      $args[] = $res['bib'];
      $args[] = $res['name'];
      $args[] = $res['lap'];
      $return = lap_counter_entry_log_line($args);
      return $return;
      break;
    case 'ChuteEntry' :
      return $bib . ' found';
      break;
  }
}

function lap_counter_page_content($did) {
  global $entrants;
  $entrants = lap_counter_entrants_current();
  $return = Array('did' => $did);
  $return['content'] = '';
  switch ($did) {
    case 'test' :
      return theme('lap_counter_test');
      break;
    case 'cronjob' :
      return theme('lap_counter_cronjob');
      break;
    case 'dataentry' :
      return theme('lap_counter_dataentry');
      break;
    case 'dataentryonly' :
      return theme('lap_counter_dataentryonly');
      break;
    case 'dataentryonly' :
      return theme('lap_counter_dataentry');
      break;
    case 'dataentryjs' :
      return theme('lap_counter_dataentryjs');
      break;
    case 'dataentrychute' :
      return theme('lap_counter_dataentrychute');
      break;
    case 'dataentrychuteexit' :
      return theme('lap_counter_dataentrychuteexit');
      break;
    case 'scoreboard' :
      return theme('lap_counter_scoreboard');
      break;
    case 'entrant' :  // Entrant Page
      return theme('lap_counter_entrant');
      break;
    case 'entrants' :  // List of Entrants
      return theme('lap_counter_entrants');
      break;
    case 'insertlap' :  // Insert Lap
      return theme('lap_counter_insert_lap');
      break;
    case 'log' :
      return theme('lap_counter_log');
      break;
    case 'stationlog' :
      return theme('lap_counter_stationlog');
      break;
    case 'adminlaps' :
      return theme('lap_counter_adminlaps');
      break;
    }
  return $return;
}


/**
 * content for the demo block
 */

function lap_counter_demo_block_content() {
  return 'ffehello?';
}

### Function: Run repeatedly at set interval
function lap_counter_cron_job() {
  return lap_counter_process_laps();
}

/**
 * Process bib entry data
 */

function lap_counter_bib_data_entry($form, &$form_state) {
  $bib = $form_state['values']['bib-number'];
  // $station_id not in use just yet
  $station_id = $form_state['values']['station-id'];
  if (!$station_id) {
    $station_id = '99';
  }
  $res = lap_counter_process_bib_number($bib, $station_id);
  return $res;
}

// Process the bib number and populate the entry log
function lap_counter_bib_number_entry_callback($form, &$form_state) {
  $return  = '';
  $bib = $form_state['values']['bib-number'];
  // $station_id not in use just yet
  $station_id = $form_state['values']['station-id'];
  if (!$station_id) {
    $station_id = '99';
  }
  $c = chr(39);
  unset($form_state['input']['bib-number']);
  unset($form_state['values']['bib-number']);
  $form['entry']['log']['#attributes'] = array(
    'onfocus' => 'document.getElementById('
     . $c . 'bib-number' . $c . ').focus()',
  );
//  $res = lap_counter_process_bib_number($bib, $station_id);
//  $return .= $bib . ' ' . $res['name'] . ' ' . $res['lap'];
  $return .= $bib . ' entered';
  return $form['entry']['log'];
  return $return;
}

/**
 * Enter the bib into the lap_count table
 */

function lap_counter_process_bib_number($bib, $station_id) {
  
  # Determine the Entrant id  
  $sql = 'SELECT race_entrant.ENTRANT_ID, race_entrant.full_name, race_entrant.bib ';
  $sql .= 'FROM race_entrant INNER JOIN race_edition ';
  $sql .= ' ON race_entrant.edition_id = race_edition.EDITION_ID ';
  $sql .= ' WHERE ((';
  $sql .= 'race_edition.current=1' . ') AND (';
  $sql .= 'race_entrant.bib= :bib))';
  
  $result = db_query($sql, array(':bib' => $bib));
  $record = $result->fetchAssoc();
  
  $entrant_id = "";
  $name = 'no bib number';
  
  if ($record) {
    $entrant_id = $record['ENTRANT_ID'];
    $name = $record['full_name'];
  }

  $data = Array();
  $data['entrant_id'] = $entrant_id;
  $data['bib'] = $bib;
  $data['station_id'] = $station_id;
  $lap = 1;

  if ($entrant_id) {
    $lap = lap_counter_entrant_total($entrant_id, TRUE);
    $data['lap'] = $lap + 1;
    $lap_id = db_insert('race_lapcount')
      ->fields($data)
      ->execute();
  }
  $msg = 'Bib #' . $bib;
  
  if ($entrant_id) {
    $msg .= ' - ' . $name . ' has ' . strval($lap + 1) . ' laps';
  }
  else {
    $msg .= ' Not Found';
  }
//  drupal_set_message($msg);
  $data['lap'] = $lap;
  $data['name'] = $name;
  $data['sql'] = $sql;
  return $data;
}

/**
 * Process the latest batch of counter records
 */

function lap_counter_process_laps() {
  global $entrants;
  $c39 = chr(39);
  // Move all the processed records out of the way
  $update = db_update('race_lapcount')
          ->fields(array('status' => 2))
          ->condition('status', 1, '=')
          ->execute();
  // Mark all unprocessed records
  $batch_size = lap_counter_cron_batch_size();
  $batch = db_select('race_lapcount', 'r')
          ->fields('r')
          ->condition('status', 0, '=')
          ->range(0,$batch_size)
          ->execute();
  
  foreach ($batch as $record) {
    $entry_id = $record->ENTRY_ID;
    $update = db_update('race_lapcount')
          ->fields(array('status' => 1))
          ->condition('ENTRY_ID', $entry_id, '=')
          ->execute();
  }

    $sqlsrc = 'SELECT entrant_id, ENTRY_ID FROM ';
    $sqlsrc .= 'race_lapcount WHERE status = 1';
    $sql = 'SELECT entrant_id, MAX(ENTRY_ID) AS MaxEntryId FROM (' . $sqlsrc;
    $sql .= ') AS T1 GROUP BY entrant_id';
    $sql1 = 'SELECT * FROM race_lapcount';
    $sql1 = $sql1 . ' INNER JOIN (' . $sql . ') AS T2';
    $sql1 = $sql1 . ' ON ENTRY_ID = MaxEntryId ;';
    
    $res = db_query($sql1);
    $return = Array();
    
    foreach ($res as $record) {
      $ret1 = Array();
      $entrant_id = $record->entrant_id;
      $options = Array('entrant_id' => $entrant_id);
      $bib = $record->bib;
      $entrant_data = $entrants[$entrant_id];
      $race_id = $entrant_data->race_id;
      $ret1['bib'] = $bib;
      $ret1['name'] = $entrant_data->full_name;
      $start_time = $entrant_data->start_time;
      $time = $record->lap_time;
      $ttime = strtotime($time);
      $st_time = strtotime($start_time);
      if (($st_time - $ttime) > 0) {
        continue; // This race hasn't started yet
      }
      $ret1['time'] = $time;
      $newentry_id = $record->ENTRY_ID;
      # find last lap record for this entrant
      $sql = 'SELECT LAP_ID FROM race_laps ';
      $sql .= 'WHERE ((deleted = 0) AND (milestone_id = 0) ';
      $sql .= 'AND (entrant_id = ' . $entrant_id .') ';
      $sql .= 'AND (lap_time < ' . $c39 . $time . $c39 .'))';
      $sql .= 'ORDER BY lap_time DESC LIMIT 1';
      $res1 = db_query($sql);
      $res1 = $res1->fetchAssoc();
      $lap_id = 0;
      $last_lap = 0;
      $lap = 1;
      $result = Array();
      $last_time = $start_time;
      $result['id'] = $entrant_id;
      $result['time'] = $time;
      if ($res1) {
        $lap_id = intval($res1['LAP_ID']);
        $res1 = db_select('race_laps', 'r')
                ->fields('r')
                ->condition('LAP_ID', $lap_id, '=')
                ->execute();
        $res1 = $res1->fetchAssoc();
        $last_time = $res1['lap_time'];
        $last_lap = $res1['lap'];
      }
      $result['ltime'] = $last_time;
      $result['ttime'] = $ttime;
      $tlast_time = strtotime($last_time);
      $result['tltime'] = $tlast_time;
      $result['neid'] = $newentry_id;
      $result[] = 'hello?';
      $diff = $ttime - $tlast_time;
      $args = Array('race_id' => $race_id);
      $reject_threshold = lap_counter_reject_threshold($args);
    //  $reject_threshold = intval($reject_threshold[1]);
      if ($diff > $reject_threshold) {
        $lap = $last_lap + 1;
      }
      else {
        $lap = 0;
      }
      $ret1['lap'] = $lap;
      if ($lap) {
        // Need to reject items recorded witon the reject threshold
        $result = db_select('race_laps', 'r')
          ->fields('r')
          ->condition('entrant_id', $entrant_id, '=')
          ->condition('lap', $lap, '=')
          ->execute();
        $res = $result->fetchAssoc();
        if (!$res) {
          $lapdata = Array(
           'entrant_id'=> $entrant_id,
           'bib'=> $bib,
           'lap'=> $lap,
           'entry_id'=> $newentry_id,
           'last_lap_id'=> $lap_id,
           'lap_time'=> $time,
           'split'=> $diff
          );
          $result = lap_counter_process_lap_entry($lapdata);
        }
      }
      $return[] = $ret1;
    }
    
    // Move all the processed records out of the way
    $update = db_update('race_lapcount')
          ->fields(array('status' => 2))
          ->condition('status', 1, '=')
          ->execute();
    $output = '';
    foreach ($return as $record) {
      $output .= $record['bib'] . '<br />';
      $output .= $record['name'] . '<br />';
      $output .= $record['time'] . '<br />';
      $output .= $record['lap'] . '<br /><br /><br />';
    }
    return $output;
  }

  // Create  a lap record in the race_lap table for the provided lap
function lap_counter_process_lap_entry($args) {
  $result = db_insert('race_laps')
     ->fields($args)
     ->execute();
}

  // assemble a list of the most recent data entry records
function lap_counter_entry_log($station_id = 99) {
  global $entrants;
  $options = Array('lapdata'=>TRUE);
  $entrants = lap_counter_entrants_current($options);
  $log_size = lap_counter_option_get(17, 5);
  $log_size = intval($log_size[1]);
  $result = db_select('race_lapcount', 'r')
     ->fields('r')
     ->condition('station_id', $station_id, '=')
     ->orderBy('ENTRY_ID', 'DESC')
     ->range(0, $log_size)
     ->execute();
  $return = '';
  foreach ($result as $record) {
    $entrant_id = $record->entrant_id;
    $entrant = $entrants[$entrant_id];
    $args = Array();
    $args[0] = $record->bib;
    $args[1] = $entrant->full_name;
    $args[2] = $entrant->lap;
    $return .= lap_counter_entry_log_line($args);
    $return .= chr(10);
  }
  return $return;
}


  // create a log line for the data entry log table
function lap_counter_entry_log_line($args = array()) {
  $bib = $args[0];
  $return = str_pad($bib, 4);
  $name = str_pad($args[1], 25);
  $name = substr($name, 0, 25);
  
  $return .= $name;
  $return .= str_pad($args[2], 6, ' ', STR_PAD_LEFT);
  return trim($return);
}


 /**
 * Enter splits into the database
 */
function lapcounter_enter_split($entrant_id, $milestone, $time) {
  global $wpdb;
  if (!$entrant_id) return 'nothing';
  if (!$milestone) return 'nothing';
  if (!$time) return 'nothing';
  $lap_time = $time;
#  $lap_time = lapcounter_convert_string_to_race_time($time, '%X');
  $milestones = explode('+', $milestone);
  $milestone_id = intval($milestones[0]);
  $lap = intval($milestones[1]);
  $lap_id = lapcounter_get_lap_id_from_lap($entrant_id, $lap);
  $notes = lapcounter_generate_milestone_description($milestone_id);
  $args = Array();
  $args['milestone_id'] = $milestone_id;
  $args['notes'] = $notes;
  $return = lapcounter_insert_lap($entrant_id, $lap_time, $args);
  return $return;
}

// Mark or delete the chosen lap
function lap_counter_lap_edit($form, &$form_state) {
  $lap_id = 0;
  $entrant_id = $_REQUEST['entrant-id'];
  if (array_key_exists('lap-id', $_REQUEST)) {
    $lap_id = $_REQUEST['lap-id'];
  }
  if (!$lap_id) {
    return;
  }
  if (array_key_exists('op', $_REQUEST)) {
    $op = $_REQUEST['op'];
  }
  switch ($op) {
    case 'DELETE':
      # delete the chosen lap
      $res = db_update('race_laps')
        ->fields(array('deleted' => 1))
        ->condition('LAP_ID', $lap_id, '=')
        ->execute();
      break;
    case 'UNDELETE':
      # restore the chosen lap
      $res = db_update('race_laps')
        ->fields(array('deleted' => 0))
        ->condition('LAP_ID', $lap_id, '=')
        ->execute();
      break;
    case 'BREAK':
      # mark the chosen lap
      $res = db_update('race_laps')
        ->fields(array('break' => 1))
        ->condition('LAP_ID', $lap_id, '=')
        ->condition('deleted', 0, '=')
        ->execute();
      break;
    case 'REMOVE':
      # unmark the chosen lap
      $res = db_update('race_laps')
        ->fields(array('BREAK' => 0))
        ->condition('LAP_ID', $lap_id, '=')
        ->execute();
      break;
  }
  return lap_counter_reorder_laps($entrant_id);
}


### Sort and renumber the laps of the entrant
function lap_counter_reorder_laps($entrant_id) {
  global $entrants;
  $lapdata = lap_counter_entrant_laps($entrant_id, 0, 'time');
  $lap = 0;
  $last_lap_id = 0;
  $race_data = $entrants[$entrant_id];
  $start_time = $race_data->start_time;
  $prev_time = $start_time;
  $count = count($lapdata);
  for ($i = 0; $i < $count; $i++) {
    if ($i == 370) {
      $t = $i;
    }
    $laprecord = $lapdata[$i];
    $lap_id = $laprecord['LAP_ID'];
    $deleted = $laprecord['deleted'];
    $time = $laprecord['lap_time'];
    $split = lap_counter_calculate_split($prev_time, $time);
    $milestone = $laprecord['milestone_id'];
    if (!$deleted and !$milestone) {
      $lap++;
      }
    if (!$deleted) {
      $prev_time = $time;
      }
    $res = db_update('race_laps')
        ->fields(array('split'=>$split, 'lap'=>$lap))
        ->condition('LAP_ID', $lap_id, '=')
        ->execute();
  }
}


### Insert a new lap at the chosen lap
function lap_counter_lap_insert($form, &$form_state) {
  global $entrants;
  $entrant_id = $form_state['input']['entrant-id'];
  $time = $form_state['input']['lap-time'];
  if (!$entrant_id) {
    return;
  }
  $entrant_info = $entrants[$entrant_id];
  $notes = 'inserted lap';
//  $milestone = $args['milestone_id'];
  $res = db_select('race_laps', 'r')
          ->fields('r')
          ->orderBy('lap_time', 'DESC')
          ->range(0, 1)
          ->condition('entrant_id', $entrant_id, '=')
          ->execute();
  
  $laprecord = $res->fetchAssoc();
  $start_time = $entrant_info->start_time;
  $lap = 0;
  $prevtime = 0;
  if ($laprecord) {
    $lap = $laprecord['lap'];
    $prevtime = $laprecord['lap_time'];
  }
  $test = explode(':', $time);
  $ct = count($test);
  if ($ct < 2 )  {
    $time = $prevtime;
  }
  if ($ct == 2 )  {
    $time = 60 * intval($test[0]) + intval($test[1]);
    $time += strtotime($start_time);
    $time = strftime("%Y-%m-%d %X", $time);
  }  
  if ($ct == 3 )  {
    $time = 3600 * intval($test[0]) + 60 * intval($test[1]) + intval($test[2]);
    $time += strtotime($start_time);
    $time = strftime("%Y-%m-%d %X", $time);
  }
  $res = db_insert('race_laps')
          ->fields(array(
              'entrant_id' => $entrant_id,
              'lap_time'   => $time,
              'notes'      => $notes,
              'bib'        => $entrant_info->bib
              ))
          ->execute();
  drupal_set_message('Lap Inserted');
/*  if ($milestone) {
//    deal with this later
//    $sql .= ', milestone_id';
  }
 * 
 */
  return lap_counter_reorder_laps($entrant_id);
  return $sql;
}
        

