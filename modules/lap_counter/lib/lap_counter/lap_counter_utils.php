<?php
/*
Plugin Name: Lap Counter
Description: Manage the web presence of a race, with daily updates, split tables etc.
Version: 0.06
Author: Medur C Wilson
Author URL: http://medur.ca
*/

/*  
	Copyright 2007  Medur Wilson  (email : medurw@yahoo.ca)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// The current request's query string as an array.
function lap_counter_get_query_string_array() {
  $qs = $_SERVER['QUERY_STRING'];
  $qs_array = Array();
  parse_str($qs, $qs_array);
  return $qs_array;
}

// A list of all events as an array for use in a select list.
function lap_counter_event_select_list() {
  $res = db_select('race_event', 'r')
          ->fields('r', array('RACE_ID', 'short_name'))
          ->orderBy('RACE_ID')
          ->execute();
  $return = Array();
  while (1) {
    $record = $res->fetchAssoc();
    if (!$record) {
      break;
    }
    $return[$record['RACE_ID']] = $record['short_name'];
  }
  return $return;
}


// A list of all current entrants as an array for use in a select list.
function lap_counter_entrant_select_list() {
  $entrants = lap_counter_entrants_current();
  $list = Array();
  foreach ($entrants as $entrant) {
    $entrant_id = $entrant->ENTRANT_ID;
    $name = $entrant->bib . ' ' . $entrant->full_name;
    $list[$entrant_id] = $name;
  }
  return $list;
}

/* A list of all events as an array for use in a select list.
 * This needs a dynamic update to 
 * restrict the list to editions of current event
 */

function lap_counter_edition_select_list($event = 0) {
  $sql = 'SELECT race_edition.EDITION_ID , race_edition.race_edition , ';
  $sql .= 'race_event.RACE_ID, race_event.short_name ';
  $sql .= 'FROM race_edition ';
  $sql .= 'INNER JOIN race_event ON race_edition.RACE_ID = race_event.RACE_ID ';
  $sql .= 'ORDER BY race_event.RACE_ID, race_edition.EDITION_ID';
  $res = db_query($sql);
  $return = Array();
  foreach ($res as $record) {
    $value = $record->short_name . ' ' . $record->race_edition;
    $return[$record->EDITION_ID] = $value;
  }
  return $return;
}

// Prepare the navigation bar data for the entrant page.
function lap_counter_get_navigation($nav = Array()) {
  $entrant_id = NULL;
  if (array_key_exists('entrant-id', $nav)) {
    $entrant_id = intval($nav['entrant-id']);
  }
  $return = Array('entrant-id'=>$entrant_id);
  $target = NULL;
  if (array_key_exists('target', $nav)) {
    $target = $nav['target'];
  }
  $return['target'] = $target;
  $session_nav = $_SESSION['nav'];
  $session_nav = Array();
  $id = $nav['id'];
  $return['id'] = $id;
  $nav_size = $nav['size'];
  if (array_key_exists('nav-size', $_REQUEST)) {
    $nav_size = intval($_REQUEST['nav-size']);
  }
  elseif (array_key_exists('size', $session_nav)) {
//    $nav_size = intval($session_nav['size']);
  }
  $return['size'] = $nav_size;
  $nav_start = 0;
  $nav_total = $nav['total'];
  if (array_key_exists('nav-total', $_REQUEST)) {
    $nav_total = intval($_REQUEST['nav-total']);
  }
  elseif (array_key_exists('total', $session_nav)) {
//    $nav_total = intval($session_nav['total']);
  }
  $return['total'] = $nav_total;
  if ($id == 'entrant') {
    $nav_start = max(Array($nav_total - $nav_size, 0));
  }
  if (array_key_exists('nav-start', $_REQUEST)) {
    $nav_start = intval($_REQUEST['nav-start']);
  }
  elseif (array_key_exists('start', $session_nav)) {
//    $nav_start = intval($session_nav['start']);
  }
  if (array_key_exists('start', $session_nav)) {
    $nav_start = intval($session_nav['start']);
  }
  if (array_key_exists('submit-first', $_REQUEST)) {
    $nav_start = 0;
  }
  if (array_key_exists('submit-prev', $_REQUEST)) {
    $nav_start = max(Array($nav_start - $nav_size, 0));
  }
  if (array_key_exists('submit-next', $_REQUEST)) {
    $min = min(Array($nav_start + $nav_size, $nav_total - $nav_size));
    $nav_start = max(Array($min, 0));
  }
  if (array_key_exists('submit-last', $_REQUEST)) {
    $nav_start = max(Array($nav_total - $nav_size, 0));
  }
  $return['start'] = $nav_start;
  $_SESSION['nav'] = $return;
  return $return;
}


function lap_counter_get_request_value($key, $default='') {
  global $_REQUEST;
  $rk = array_keys($_REQUEST);
  $return = $default;
  if (in_array($key, $rk) ) {
    if ($_REQUEST[$key]) {
      $return = $_REQUEST[$key];
    }
  }
  return $return;
}


// Format a php timestmp time as HH:MM:SS
function lap_counter_format_timestamp($time, $ftype = 0) {
  $ftime = '';
  switch ($ftype) {
    case 0:
      $hours = intval($time / 3600);
      if ($hours) {
        $ftime = strval($hours) . ':';
      }
      else {
        $ftime = '0:';
      }
      $ltime = localtime($time);
      if ($ltime[1]) {
        $ftime .= str_pad($ltime[1], 2, "0", STR_PAD_LEFT) . ':';
      }
      else {
        $ftime .= '00:';
      }
      if ($ltime[0]) {
        $ftime .= str_pad($ltime[0], 2, "0", STR_PAD_LEFT);
      }
      else {
        $ftime .= '00';
      }
    break;
    case 1:
      $ltime = localtime($time);
      if ($ltime[2]) {
        $ftime = str_pad($ltime[2], 2, "0", STR_PAD_LEFT) . ":";
      }
      $ftime .= str_pad($ltime[1], 2, "0", STR_PAD_LEFT) . ":";
      $ftime .= str_pad($ltime[0], 2, "0", STR_PAD_LEFT);
    break;
  }
  return $ftime;
}



// Get the offset value for the race edition
function lap_counter_get_offset($edition_id) {
  $sql = 'SELECT offset FROM race_edition WHERE edition_id = ';
  $sql .= $edition_id;
  $res = db_query($sql);
  if (count($res)) {
    foreach ($res AS $record) {
      return intval($record->offset);
    }
  }
  else {
    return 0;
  }
}


// Shortcut function to return the system operating mode.
function lap_counter_get_mode() {
  return lap_counter_option_get(9);
}

// Shortcut function to return the current timezone offset.
function lap_counter_get_timezone_offset() {
  $return = lap_counter_option_get(6, 0);
  return intval($return[1]);
}



// Sets an option value in the options table
function lap_counter_option_set($option_id, $args = Array()) {
  $result = db_select('race_options', 'ro')
          ->fields('ro')
          ->condition('option_id', $option_id, '=')
          ->fetchAssoc()
          ->execute();
  if ($result) {
    $values = Array();
    if (isset($args['option_name'])) {
      $values['option_name'] = $args['option_name'];
    }
    if (isset($args['option_value'])) {
      $values['option_value'] = $args['option_value'];
    }
    if (count($values)) {
      $result = db_update('race_options')
          ->fields($values)
          ->condition('option_id', $option_id, '=')
          ->execute();
    }
  }
  else {
    array_merge($args, array('option_id' => $option_id));
    $result = db_insert('race_options')
          ->fields($args)
          ->execute();
  }
  return lap_counter_option_get($option_id);
}

// Get an option value from the options table
function lap_counter_option_get($option_id, $default = '') {
  $res = db_select('race_options', 'r')
          ->fields('r')
          ->condition('option_id', $option_id, '=')
          ->execute()
          ->fetchAssoc();
  if (!$res) {
    $return = Array(1 => (int)$default);
  }
  elseif (count($res)) {
    $return = Array(1 => $res['option_value']);
  }
  else {
    $return = Array(1 => $default);
  }
  return $return;
}

/**
 * Return a list of all current editions
 */
function lap_counter_current_events() {
  return lap_counter_events(1);
}

/**
 * Return a list of all events
 */
function lap_counter_events($current = FALSE) {
  $sql = 'SELECT race_event.short_name, race_event.name, ';
  $sql .= 'race_event.lap_distance, race_event.reject_limit, ';
  $sql .= 'race_edition.start_time, race_edition.offset, ';
  $sql .= 'race_edition.finish_time, race_edition.EDITION_ID, ';
  $sql .= 'race_edition.race_id, race_edition.race_edition, ';
  $sql .= 'race_edition.description, race_edition.current ';
  $sql .= 'FROM race_edition INNER JOIN race_event ON ';
  $sql .= ' race_edition.race_id = race_event.RACE_ID ';
  if ($current) {
    $sql .= ' WHERE race_edition.current = 1';
  }
  $events = db_query($sql);
  $return = Array();
  foreach ($events as $event) {
    $return[$event->EDITION_ID] = $event;
  }
  return $return;
}


  /**
 * Return a list of counter stations
 */
function lap_counter_counter_stations() {
  $sql = 'SELECT * FROM race_counter_stations';
  $stations = db_query($sql);
  $return = Array();
  foreach ($stations as $station) {
    $return[$station->ID] = $station;
  }
  return $return;
}



function lap_counter_calculate_split($stime, $ftime) {
  #find the split time between the 2 times and return as number of seconds
  $tftime = strtotime($ftime);
  $tstime = strtotime($stime);
  $diff = $tftime - $tstime;
  return $diff;
}