  <?php
/**
 * @file
 * Provides support to lap counting team in a long-distance race on a loop.
 */

/**
* Implements hook_help.
*
* Displays help and module information.
*
* @param path
*   Which path of the site we're using to display help
* @param arg
*   Array that holds the current path as returned from arg() function

$basename = explode('.php', basename( __FILE__));
$basename = $basename[0];
$file = join(DIRECTORY_SEPARATOR, array(dirname(__FILE__)));
$file .= DIRECTORY_SEPARATOR . 'lib';
$file .= DIRECTORY_SEPARATOR . $basename;
$file .= DIRECTORY_SEPARATOR . $basename;
$tmp = $file . '.php';
if (file_exists($tmp)) {
   include_once $tmp;
 }
$tmp = $file . '.inc';
if (file_exists($tmp)) {
   include_once $tmp;
 }
*/

require_once 'lib/lap_counter/lap_counter.inc';

// Return the file system location of this module
function lap_counter_module_location() {
  
  $file = drupal_get_path('module', 'lap_counter');
  return $file;
}

/**
 * Implements hook_permission().
 *
 * Since the access to our new custom pages will be granted based on
 * special permissions, we need to define what those permissions are here.
 * This ensures that they are available to enable on the user role
 * administration pages.
 */
function lap_counter_permission() {
  return array(
    'add edit delete laps' => array(
      'title' => t('Add Edit Delete Laps'),
      'description' => t('Allow user to edit the laps of an entrant'),
    ),
    'administer lap counter' => array(
      'title' => t('Administer Lap Counter'),
      'description' => t('Allow user to administer lap counter settings'),
    ),
  );
}

function lap_counter_help($path, $arg) {
  switch ($path) {
  case 'admin/help#lap_counter':
    $output = '<p>'. t('Main admin menu entry for Lap Counter.') .'</p>';
    return $output;
}
}

/**
 * Implementation of hook_menu().
 *
 * Called when Drupal is building menus.  Cache parameter lets module know
 * if Drupal intends to cache menu or not - different results may be
 * returned for either case.
 *
 * @param may_cache true when Drupal is building menus it will cache
 *
 * @return An array with the menu path, callback, and parameters.
 */

function lap_counter_menu() {
  $items = array();
  /* Admin menus */
  $items['admin/config/lap_counter'] = array(
    'title' => 'Lap Counter',
    'description' => 'Count laps in a race.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lap_counter_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/lap_counter/options'] = array(
    'title' => 'Lap Counter Options',
    'description' => 'Manage Options.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lap_counter_options_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/lap_counter/entrants'] = array(
    'title' => 'Lap Counter Entrants',
    'description' => 'Manage Entrants.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lap_counter_entrants_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['admin/config/lap_counter/events'] = array(
    'title' => 'Lap Counter Events',
    'description' => 'Manage Events.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lap_counter_events_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['lap_counter/events'] = array(
    'title' => 'Lap Counter Events',
    'description' => 'Manage Events.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lap_counter_events_admin_form'),
    'access arguments' => array('add edit delete laps'),
  );
  /* User interface */
  $items['scoreboard'] = array(
    'title' => 'Scoreboard',
    'description' => 'Show the current standings.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('scoreboard'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['entrant'] = array(
    'title' => 'Entrant',
    'description' => 'Show the selected entrant\'s info.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('entrant'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['insert-lap'] = array(
    'title' => 'Insert Lap',
    'description' => 'Insert a lap for the entrant.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('insertlap'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['log'] = array(
    'title' => 'Log',
    'description' => 'Show the most recent activity.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('log'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['test'] = array(
    'title' => 'Test Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('test'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['entry'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('dataentry'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['entry-only'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter activity.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('dataentryonly'),
    'access arguments' => array('add edit delete laps'),
  );
  $items['entry-js'] = array(
    'title' => 'Data Entry',
    'description' => 'Enter bib data.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('dataentryjs'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['chute-entry'] = array(
    'title' => 'Chute Finish Line',
    'description' => 'Enter participants at finish line.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('dataentrychute'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['chute-exit'] = array(
    'title' => 'Chute Exit',
    'description' => 'Enter bib numbers for participants exiting chute.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('dataentrychuteexit'),
    'access arguments' => array('add edit delete laps'), 
  );
  $items['counter'] = array(
    'title' => 'Station Log',
    'description' => 'Log of Station.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('stationlog'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['entrants'] = array(
    'title' => 'Runners',
    'description' => 'List of all Runners.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('entrants'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['process-laps'] = array(
    'title' => 'Process Laps',
    'description' => 'Process the lap data.',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('cronjob'),
    'access arguments' => array('access content'), /* can do better */
  );
  $items['admin-laps'] = array(
    'title' => 'Admin',
    'description' => 'Administer Lap Counter Settings',
    'page callback' => 'lap_counter_page_content',
    'page arguments' => array('adminlaps'),
    'access arguments' => array('administer lap counter'), /* can do better */
  );

  return $items;
}

function lap_counter_theme() {

  return array(
    'lap_counter_page_content' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_test' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_scoreboard' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_log' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_stationlog' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_entrant' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_entrants' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_insert_lap' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_dataentry' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_dataentryonly' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_dataentryjs' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_dataentrychute' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_dataentrychuteexit' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_cronjob' => array(
      'template' => 'lap_counter-content',
    ),
    'lap_counter_adminlaps' => array(
      'template' => 'lap_counter-content',
    ),
  );
}

function lap_counter_preprocess_lap_counter_test(&$variables) {
  $variables['page_title'] = 'TEst';
  $variables['display_id'] = 'test';
  $variables['messages'] = '';
  }

/**
 * Implementation of template_preprocess_lap_counter_scoreboard().
 */
function lap_counter_preprocess_lap_counter_scoreboard(&$variables) {
  $variables['page_content'] = lap_counter_standings_table();
  $variables['page_title'] = 'Scoreboard';
  $variables['display_id'] = 'scoreboard';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Entrant view.
 */
function lap_counter_preprocess_lap_counter_entrant(&$variables) {
  global $entrants;
  $entrant_id = lap_counter_get_request_value( 'entrant-id' ,'');
  $entrant = $entrants[$entrant_id];
// Load the insert form here to process any inserts
  $form_insert = drupal_get_form('lap_counter_insert_lap_form');
  $variables['page_content'] = lap_counter_entrant_table($entrant);
  $variables['page_title'] = $entrant->full_name . ' (' . $entrant->bib . ') - ' .
          $entrant->race_name;
  $variables['display_id'] = 'entrant';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Entrants view.
 */
function lap_counter_preprocess_lap_counter_entrants(&$variables) {
  
  $variables['page_content'] = lap_counter_entrants_table();
  $variables['page_title'] = 'Runners';
  $variables['display_id'] = 'entrants';
  $variables['messages'] = '';
  }

  
/**
 * Set up variables for Lap Insert form.
 */
function lap_counter_preprocess_lap_counter_insert_lap(&$variables) {
  global $entrants;
  $entrant_id = lap_counter_get_request_value( 'entrant-id' ,'');
  $entrant = $entrants[$entrant_id];
  $title = 'Insert Lap for: ' . $entrant->full_name . ' (' . $entrant->bib . ')';
  // Load the form here so we can return to the entrant page
  $form_insert = drupal_get_form('lap_counter_insert_lap_form');
  $content = lap_counter_insert_lap($form_insert, $entrant_id);
  $variables['page_content'] = $content;
  $variables['page_title'] = $title;
  $variables['display_id'] = 'insertlap';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Data Entry.
 */
function lap_counter_preprocess_lap_counter_dataentry(&$variables) {  
  $dataform = drupal_get_form('lap_counter_bib_data_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentry';
  $variables['messages'] = '';
  }
/**
 * Set up variables for Data Entry.
 */
function lap_counter_preprocess_lap_counter_dataentryonly(&$variables) {  
  $dataform = drupal_get_form('lap_counter_bib_data_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentryonly';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Javascript-based Data Entry.
 */
function lap_counter_preprocess_lap_counter_dataentryjs(&$variables) {  
  $dataform = drupal_get_form('lap_counter_bib_data_js_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Data Entry';
  $variables['display_id'] = 'dataentryjs';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Chute - Finish Line.
 */
function lap_counter_preprocess_lap_counter_dataentrychute(&$variables) {  
  $dataform = drupal_get_form('lap_counter_chute_entry_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Chute: Finish Line';
  $variables['display_id'] = 'dataentrychute';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Chute - Finish Line.
 */
function lap_counter_preprocess_lap_counter_dataentrychuteexit(&$variables) {  
  $dataform = drupal_get_form('lap_counter_chute_exit_form');
  $variables['page_content'] = drupal_render($dataform);
  $variables['page_title'] = 'Chute: Exit';
  $variables['display_id'] = 'dataentrychuteexit';
  $variables['messages'] = '';
  }

/**
 * Set up variables for Station Log view.
 */
function lap_counter_preprocess_lap_counter_stationlog(&$variables) {
  $filter = Array();
  $edition_id = lap_counter_get_request_value( 'edition-id' ,'');
  $current_station_id = lap_counter_get_request_value( 'station-id' ,'');
  $races = lap_counter_current_events();
  $text = 'All Runners';
  if ($current_station_id) {
    $filter['station_id'] = $current_station_id;
    $text = 'Station: ' . $current_station_id;
  }
  if ($edition_id) {
    $filter['EDITION_ID'] = $edition_id;
    foreach ($races as $race) {
      if ($race->EDITION_ID == $edition_id) {
            $text = $race->short_name . ' Race';
            break;
          }
        }
        unset($filter['station_id']);
      }
  $variables['page_content'] = lap_counter_entrant_station_log_table($filter);
  $variables['page_title'] = $text;
  $variables['display_id'] = 'stationlog';
  $variables['messages'] = '';
  }
/**
 * Set up variables to process lap data.
 */

function lap_counter_preprocess_lap_counter_cronjob(&$variables) {
  $variables['page_content'] = lap_counter_cron_job();
  $variables['page_title'] = 'Process Laps';
  $variables['display_id'] = 'cronjob';
  $variables['messages'] = '';
  $refresh_rate = lap_counter_cron_rate();
  $args = Array();
  $args['#tag'] = 'meta';
  $args['#attributes'] = Array(
      'http-equiv' => 'Refresh',
      'content' => $refresh_rate
  );
    drupal_add_html_head($args, 'refresh_rate');
  }
  
/**
 * Set up variables Lap Counter Admin page.
 */

function lap_counter_preprocess_lap_counter_adminlaps(&$variables) {
  $variables['page_content'] = lap_counter_admin();
  $variables['page_title'] = 'Administer Lap Counter';
  $variables['display_id'] = 'adminlaps';
  $variables['messages'] = '';
}

/**
 * Set up variables for Log view.
 */
  
function lap_counter_preprocess_lap_counter_log(&$variables) {
  $variables['page_content'] = lap_counter_log_table();
  $variables['page_title'] = 'Log';
  $variables['display_id'] = 'log';
  $variables['messages'] = '';
}