<?php 
/**
 * @file
 * Get Skeleton theme implementation to display a single Drupal page.
 */
  $refresh_list = lap_counter_refresh_rate_list();
  if (in_array($display_id, $refresh_list)) {
    $refresh_rate = lap_counter_refresh_rate();
    $args = Array();
    $args['#tag'] = 'meta';
    $args['#attributes'] = Array(
      'http-equiv' => 'Refresh',
      'content' => $refresh_rate
    );
    drupal_add_html_head($args, 'refresh_rate');
}
$module_location = lap_counter_module_location();
$cssfile = $module_location . '/css/lap_counter.css';
drupal_add_css($cssfile);
$array_list = lap_counter_js_enabled_list();
if (in_array($display_id, array_keys($array_list))) {
  $cssfile = $module_location . '/css/data_entry.css';
  drupal_add_css($cssfile);
  $jsfile = $array_list[$display_id];
  $jsfile = $module_location . '/js/' . $jsfile;
  drupal_add_js($jsfile);
}
//$menu = menu_load('menu-lap-counter');
$menu_links = menu_navigation_links('menu-lap-counter');
$menu_html = theme('links__menu-lap-counter', array('links' => $menu_links));

?>
  <div class="container"
       onkeyup = "SetChuteEntry(event)">
    <div id="header">
      
      <?php if ($display_id != 'dataentryonly'): ?>
        <div id="lap-counter-menu">
          <?php print $menu_html; ?>
        </div> <!-- /menu -->
      <?php endif; ?>
    </div> <!-- /header -->
    <?php if ($messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div></div> <!-- /.section, /#messages -->
    <?php endif; ?>      
    <div id="content" class="column">
          <?php 
            $output = '<div id="lap_counter-title">';
            $output .= $page_title;
            $output .= '</div>';
            print $output;
          ?>
      <?php print ( $page_content ); ?>
      
    </div>
      
      
  </div><!-- container -->
