function SetChuteEntry(event) {
  var response = '';
  var textfield = document.getElementById("bib-number");
  var itemlog = document.getElementById("bib-entry");
//  var userid = document.getElementById("userid");
//  var uid = userid.value;
  var keypress = event.keyCode? event.keyCode : event.charCode
  var bvalue = textfield.value;
//  alert(bvalue);
  if (bvalue.length == 0) {
    bvalue = '-';
  }
  
  var xmlhttp;
  if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }
  else
    {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      response = UpdateLog(itemlog, xmlhttp.responseText);
      itemlog.innerHTML=response;
    }
  }
  if (keypress == 13 || keypress == 9) {
    textfield.value = '';
    var url = '/bib-entry.php';
    var args = 'bibnumber=' + bvalue;
//    args += '&userid=' + uid;
    args += '&fn=ChuteEntry';
    xmlhttp.open("POST",url,true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(args);
    return false;
  }
  return;
}

function UpdateLog(itemlog, text) {
  var log = itemlog.innerHTML;
  data = log.split('\n');
  var len = data.length;
  if (len > 10) {
    len = 10;
  }
  var result = '<li>' + text + '</li>\n';
  for (var i=0;i<len-1;i++)
  {
    result += data[i];
    result += '\n';
  }
  return result;
}